﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="grafico.aspx.cs" Inherits="webequilibrio.app.residuo.grafico" %>

<%@ Register Assembly="DevExpress.XtraCharts.v15.2.Web, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../Content/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <h2>Controle de Qualidade das Operações Florestais</h2>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3>Controle de Qualidade da Colheita - Florestal ARA</h3>
                </div>
                <div class="panel-body">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                        <div class="col-xs-5 ">
                                <dx:ASPxComboBox ID="cbxMes" runat="server" NullText="Mês" DataSourceID="ds_vMes" TextField="mes">
                                </dx:ASPxComboBox>
                                <asp:SqlDataSource ID="ds_vMes" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                                    ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" 
                                    SelectCommand="SELECT mes FROM v_mes">

                                </asp:SqlDataSource>
                        </div>

                        <div class="col-xs-5 ">
                                <dx:ASPxComboBox ID="cbxSemana" runat="server" NullText="Semana">
                                    <Items>
                                        <dx:ListEditItem Text="1 Semana" Value="1" />
                                        <dx:ListEditItem Text="2 Semana" Value="2" />
                                        <dx:ListEditItem Text="3 Semana" Value="3" />
                                        <dx:ListEditItem Text="4 Semana" Value="4" />
                                        <dx:ListEditItem Text="5 Semana" Value="5" />
                                    </Items>
                                </dx:ASPxComboBox>
                        </div>

                        <div class="col-xs-2">
                            <asp:Button ID="btnFiltrar" runat="server" Text="Filtrar" CssClass="btn btn-default" OnClick="btnFiltrar_Click" />
                        </div>
                            </div>
                    </div>
                            </div>
                        </div>

                    <div class="row">
                        <div class="col-md-6">
                            <dxchartsui:WebChartControl ID="GrafProjResiduo" runat="server" CrosshairEnabled="True" Height="400px" Width="450px">
                                <DiagramSerializable>
                                    <cc1:XYDiagram>
                                        <axisx visibility="True" visibleinpanesserializable="-1">
                                </axisx>
                                        <axisy visibleinpanesserializable="-1" visibility="True">
                                </axisy>
                                    </cc1:XYDiagram>
                                </DiagramSerializable>
                                <Legend Visibility="False"></Legend>
                                <SeriesSerializable>
                                    <cc1:Series Name="Series 1" ArgumentDataMember="projeto" ValueDataMembersSerializable="residuo" LabelsVisibility="True">
                                        <labelserializable>
                                <cc1:SideBySideBarSeriesLabel Position="Top">
                                </cc1:SideBySideBarSeriesLabel>
                            </labelserializable>
                                    </cc1:Series>
                                </SeriesSerializable>
                                <Titles>
                                    <cc1:ChartTitle Text="Percentual de Resíduo por Projeto" Visibility="True" />
                                </Titles>
                            </dxchartsui:WebChartControl>
                        </div>
                        <div class="col-md-6">
                            <dxchartsui:WebChartControl ID="GrafPorTipoResiduo" runat="server" CrosshairEnabled="True" Height="400px" Width="450px">
                                <Legend Visibility="True"></Legend>
                                <SeriesSerializable>
                                    <cc1:Series ArgumentDataMember="tipo" Name="Series 1" ValueDataMembersSerializable="valor" LegendTextPattern="{A} {V:0.00%}" ToolTipSeriesPattern="" ArgumentScaleType="Qualitative" LabelsVisibility="True">
                                        <viewserializable>
                                    <cc1:PieSeriesView MinAllowedSizePercentage="60">
                                    </cc1:PieSeriesView>
                                </viewserializable>
                                    </cc1:Series>
                                </SeriesSerializable>
                                <Titles>
                                    <cc1:ChartTitle Text="Estratificação por Tipo de Resíduo Semanal" Font="Tahoma, 12pt" />
                                </Titles>
                            </dxchartsui:WebChartControl>
                        </div>
                        <div class="col-md-6">
                            <dxchartsui:WebChartControl ID="GrafPorTipoResiduoMes" runat="server" CrosshairEnabled="True" Height="400px" Width="450px">
                                <Legend Visibility="True"></Legend>
                                <SeriesSerializable>
                                    <cc1:Series ArgumentDataMember="tipo" Name="Series 1" ValueDataMembersSerializable="valor" LegendTextPattern="{A} - {VP:0.00%}" ToolTipSeriesPattern="" ArgumentScaleType="Qualitative" LabelsVisibility="True">
                                        <viewserializable>
                                    <cc1:PieSeriesView MinAllowedSizePercentage="60">
                                    </cc1:PieSeriesView>
                                </viewserializable>
                                    </cc1:Series>
                                </SeriesSerializable>
                                <Titles>
                                    <cc1:ChartTitle Text="Estratificação por Tipo de Resíduo Mensal" Font="Tahoma, 12pt" />
                                </Titles>
                            </dxchartsui:WebChartControl>
                        </div>
                        <div class="col-md-6">
                            <dxchartsui:WebChartControl ID="GrafAcumMesSemana" runat="server" CrosshairEnabled="True" Height="400px" Width="450px">
                                <DiagramSerializable>
                                    <cc1:XYDiagram>
                                    <axisx visibleinpanesserializable="-1">
                                    </axisx>
                                    <axisy visibleinpanesserializable="-1">
                                    </axisy>
                                    </cc1:XYDiagram>
                                </DiagramSerializable>
                                <Legend Visibility="False"></Legend>
                                <SeriesSerializable>
                                    <cc1:Series ArgumentDataMember="tipo" Name="Series 1" ValueDataMembersSerializable="valor" LabelsVisibility="True" ToolTipSeriesPattern="{S:0.00%}" >
                                    <labelserializable>
                                        <cc1:SideBySideBarSeriesLabel Position="Top">
                                        </cc1:SideBySideBarSeriesLabel>
                                    </labelserializable>
                                    </cc1:Series>
                                </SeriesSerializable>
                                <Titles>
                                    <cc1:ChartTitle Text="Percentual de Resíduos Mês" />
                                </Titles>
                            </dxchartsui:WebChartControl>
                        </div>
                        <div class="col-md-12">
                            <dxchartsui:WebChartControl ID="GrafMensal" runat="server" CrosshairEnabled="True" Height="400px" Width="450px">
                                <DiagramSerializable>
                                    <cc1:XYDiagram>
                                    <axisx visibleinpanesserializable="-1">
                                    </axisx>
                                    <axisy visibleinpanesserializable="-1">
                                    </axisy>
                                    </cc1:XYDiagram>
                                </DiagramSerializable>
                                <Legend Visibility="False"></Legend>
                                <SeriesSerializable>
                                    <cc1:Series ArgumentDataMember="tipo" Name="Series 1" ValueDataMembersSerializable="valor" LabelsVisibility="True" ToolTipSeriesPattern="{S:0.00%}" >
                                    <labelserializable>
                                        <cc1:SideBySideBarSeriesLabel Position="Top">
                                        </cc1:SideBySideBarSeriesLabel>
                                    </labelserializable>
                                    </cc1:Series>
                                </SeriesSerializable>
                                <Titles>
                                    <cc1:ChartTitle Text="Percentual de Resíduos Mês" />
                                </Titles>
                            </dxchartsui:WebChartControl>
                        </div>
                        <div class="col-md-12">
                            <h3><asp:Label ID="lblPercAracruz" runat="server" Text="Label"></asp:Label></h3>
                        </div>
                    </div>



                </div>
            </div>
        </div>

    </form>
</body>
</html>
