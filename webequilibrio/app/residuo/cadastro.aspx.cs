﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using DevExpress.Web;
using DevExpress.Export;
using DevExpress.XtraPrinting;


namespace webequilibrio.app.residuo
{
    public partial class cadastro : System.Web.UI.Page
    {
        app.residuo.Calcular calcular = new Calcular();
        app.classe.bdados dados = new app.classe.bdados();
        string url = HttpContext.Current.Request.Url.AbsolutePath;
        protected void Page_Load(object sender, EventArgs e)
        {
            url = HttpContext.Current.Request.Url.AbsolutePath;
            bool acesso = dados.temAcesso(Convert.ToInt32(Session["_userId"]), "~" + url);

            //if (!acesso)
            //{
            //    Response.Redirect("~/default.aspx", true);
            //}

        }

        protected void gvResiduo_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            try
            {
                //for (int i = 0; i < e.NewValues.Count; i++)
                //{
                //    if (e.NewValues[i].ToString().Trim() == "")
                //        throw new Exception("valor em branco");
                //}

                int id = (int)e.NewValues["id"];
                float peso_bruto = (float)e.NewValues["peso_bruto"];
                float casca_amostra = (float)e.NewValues["casca_amostra"];
                float peso_liquido = (float)e.NewValues["peso_liquido"];
                float casca_presa = (float)e.NewValues["casca_presa"];
                float casca_solta = (float)e.NewValues["casca_solta"];
                float madeira_inapta = (float)e.NewValues["madeira_inapto"];
                float areia = (float)e.NewValues["areia"];
                float outro = (float)e.NewValues["outro"];
                DateTime dt_monitoramento = (DateTime)e.NewValues["dt_monitoramento"];
                string unidade = (string)e.NewValues["unidade"];
                calcular.tblAuxiliar(peso_bruto, casca_amostra, peso_liquido, casca_presa, casca_solta, madeira_inapta, areia, outro, unidade, dt_monitoramento, id);
            }
            catch (Exception ex)
            {
                e.Cancel = true;
            }


        }

        protected void gvResiduo_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
        {

            //int id = GetLastKey();
            //float peso_bruto = (float)e.NewValues["peso_bruto"];
            //float casca_amostra = (float)e.NewValues["casca_amostra"];
            //float peso_liquido = (float)e.NewValues["peso_liquido"];
            //float casca_presa = (float)e.NewValues["casca_presa"];
            //float casca_solta = (float)e.NewValues["casca_solta"];
            //float madeira_inapta = (float)e.NewValues["madeira_inapto"];
            //float areia = (float)e.NewValues["areia"];
            //float outro = (float)e.NewValues["outro"];
            //DateTime dt_monitoramento = (DateTime)e.NewValues["dt_monitoramento"];
            //string unidade = (string)e.NewValues["unidade"];
            //calcular.tblAuxiliar(peso_bruto, casca_amostra, peso_liquido, casca_presa, casca_solta, madeira_inapta, areia, outro, unidade, dt_monitoramento, id);


        }

        //protected int GetLastKey()
        //{
        //    string connectionString = ConfigurationManager.ConnectionStrings["equilibrioConnectionString"].ConnectionString;
            //using (MySql.Data.MySqlClient.MySqlConnection connection = new MySql.Data.MySqlClient.MySqlConnection(connectionString))
            //{
            //    MySql.Data.MySqlClient.MySqlCommand command = new MySql.Data.MySqlClient.MySqlCommand("SELECT LAST_INSERT_ID()");
            //    command.Connection = connection;
            //    connection.Open();
            //    Object result = command.ExecuteScalar();
            //    return Convert.ToInt32(result);
            //}
        //}

        protected void gvResiduo_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            foreach (GridViewColumn column in gvResiduo.Columns)
            {
                //GridViewDataColumn dataColumn = column as GridViewDataColumn;
                //if (dataColumn == null) continue;

                //if (
                //    dataColumn.FieldName != "id" &&
                //    dataColumn.FieldName != "dt_criacao" &&
                //    dataColumn.FieldName != "dt_atualizacao"
                //    )
                //{
                //    if (e.NewValues[dataColumn.FieldName] == null)
                //    {
                //        e.Errors[dataColumn] = "Não pode ser em branco";
                //    }
                //}

                //if ((float)e.NewValues["peso_bruto"] <= (float)e.NewValues["peso_liquido"])
                //{
                //    AddError(e.Errors, gvResiduo.Columns["peso_bruto"], "Peso bruto não pode ser menor ou igual a peso_liquido");
                //}

                //if (e.NewValues["hr_inicio"].ToString() == "00:00:00")
                //{
                //    AddError(e.Errors, gvResiduo.Columns["hr_inicio"], "Insira o horário");
                //}
                //if (e.NewValues["hr_fim"].ToString() == "00:00:00")
                //{
                //    AddError(e.Errors, gvResiduo.Columns["hr_fim"], "Insira o horário");
                //}
            }

            if (string.IsNullOrEmpty(e.RowError) && e.Errors.Count > 0) e.RowError = "Corriga todos os erros.";
        }

        void AddError(Dictionary<GridViewColumn, string> errors, GridViewColumn column, string errorText)
        {
            if (errors.ContainsKey(column)) return;
            errors[column] = errorText;
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {

            gvExporter.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }
    }
}