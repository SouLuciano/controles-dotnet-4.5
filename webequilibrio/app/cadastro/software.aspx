﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="software.aspx.cs" Inherits="webequilibrio.app.cadastro.software" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <dx:ASPxGridView ID="gvSoftware" runat="server" DataSourceID="dsSoftware" EnableTheming="True" Theme="Metropolis" AutoGenerateColumns="False" KeyFieldName="id">
            <Settings ShowFilterRow="True" />
            <SettingsSearchPanel Visible="True" />
            <Columns>
                <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="0">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="nome" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="versao" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="sistema" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="requer" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="fabricante" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="changelog" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ativo" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="dsSoftware" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
            DeleteCommand="DELETE FROM software WHERE id = @id" 
            InsertCommand="INSERT INTO software (id, nome, versao, sistema, requer, fabricante, changelog, ativo) VALUES (@id, @nome, @versao, @sistema, @requer, @fabricante, @changelog, @ativo)" 
            ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" 
            SelectCommand="SELECT id, nome, versao, sistema, requer, fabricante, changelog, ativo FROM software ORDER BY nome, versao" 
            UpdateCommand="UPDATE software SET nome = @nome, versao = @versao, sistema = @sistema, requer = @requer, fabricante = @fabricante, changelog = @changelog, ativo = @ativo WHERE id = @id">
            <DeleteParameters>
                <asp:Parameter Name="id" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="id" Type="Int32" />
                <asp:Parameter Name="nome" Type="String" />
                <asp:Parameter Name="versao" Type="String" />
                <asp:Parameter Name="sistema" Type="String" />
                <asp:Parameter Name="requer" Type="String" />
                <asp:Parameter Name="fabricante" Type="String" />
                <asp:Parameter Name="changelog" Type="String" />
                <asp:Parameter Name="ativo" Type="Boolean" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="nome" Type="String" />
                <asp:Parameter Name="versao" Type="String" />
                <asp:Parameter Name="sistema" Type="String" />
                <asp:Parameter Name="requer" Type="String" />
                <asp:Parameter Name="fabricante" Type="String" />
                <asp:Parameter Name="changelog" Type="String" />
                <asp:Parameter Name="ativo" Type="Boolean" />
                <asp:Parameter Name="id" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
    
    </div>
    </form>
</body>
</html>
