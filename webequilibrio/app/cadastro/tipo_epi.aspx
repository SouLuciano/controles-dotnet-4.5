﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="tipo_epi.aspx.cs" Inherits="webequilibrio.app.cadastro.eqp_tipo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxGridView runat="server" AutoGenerateColumns="False" DataSourceID="dsTipoEquipamento" Theme="Metropolis" ID="gvEpi" KeyFieldName="nome" OnRowInserting="gvEpi_RowInserting" OnRowUpdating="gvEpi_RowUpdating">
        <SettingsDetail AllowOnlyOneMasterRowExpanded="True">
        </SettingsDetail>
        <SettingsEditing Mode="PopupEditForm">
        </SettingsEditing>
        <Settings ShowFilterRow="True" ShowFilterRowMenu="True">
        </Settings>
        <SettingsBehavior AutoFilterRowInputDelay="1800">
        </SettingsBehavior>
        <SettingsPopup>
            <EditForm HorizontalAlign="Center">
            </EditForm>
        </SettingsPopup>
        <SettingsSearchPanel Delay="2000">
        </SettingsSearchPanel>
        <Columns>
            <dx:GridViewCommandColumn ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="nome" VisibleIndex="1" Caption="Relação de EPI">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="tipo" VisibleIndex="2" Visible="False">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="dsTipoEquipamento" runat="server"
        ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>"
        DeleteCommand="DELETE FROM tipo_equipamento WHERE nome = @nome AND tipo = @tipo"
        InsertCommand="INSERT INTO tipo_equipamento (nome, tipo) VALUES (@nome, @tipo)"
        ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>"
        SelectCommand="SELECT nome, tipo FROM tipo_equipamento WHERE (tipo = @tipo) ORDER BY nome"
        UpdateCommand="UPDATE tipo_equipamento SET nome = @nome, tipo = @tipo  WHERE nome = @nome AND tipo = @tipo"
        OnUpdating="dsTipoEquipamento_Updating">
        <DeleteParameters>
            <asp:Parameter Name="nome" Type="String" />
            <asp:Parameter Name="tipo" Type="String" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="nome" Type="String" />
            <asp:Parameter Name="tipo" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="nome" Type="String" />
            <asp:Parameter DefaultValue="epi" Name="tipo" Type="String" />
        </UpdateParameters>
        <SelectParameters>
            <asp:Parameter DefaultValue="epi" Name="tipo" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
