﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="logsistema.aspx.cs" Inherits="webequilibrio.app.consultas.logsistema" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxGridView ID="gvLog" runat="server" DataSourceID="dsLog" EnableTheming="True" Theme="Metropolis" AutoGenerateColumns="False" KeyFieldName="id">
        <SettingsPager PageSize="15">
        </SettingsPager>
        <Settings ShowFilterRow="True" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="0">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="tipo" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="acao" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="descricao" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="url" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ip" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="os" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="browser" VisibleIndex="8">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="browser_ver" VisibleIndex="9">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="dt_criacao" VisibleIndex="10">
                <PropertiesDateEdit DisplayFormatString="G">
                </PropertiesDateEdit>
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataComboBoxColumn FieldName="id_user" VisibleIndex="1">
                <PropertiesComboBox DataSourceID="dsUser" TextField="email" ValueField="id">
                    <Columns>
                        <dx:ListBoxColumn Caption="Nome" FieldName="nome" />
                        <dx:ListBoxColumn Caption="e-mail" FieldName="email" />
                    </Columns>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="dsLog" runat="server" 
        ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
        SelectCommand="SELECT [id], [id_user], [tipo], [acao], [descricao], [url], [ip], [os], [browser], [browser_ver], [dt_criacao] FROM [log] ORDER BY [id] DESC"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsUser" runat="server" 
        ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
        SelectCommand="SELECT [user].id, [user].email, funcionario.nome FROM [user] INNER JOIN funcionario ON [user].idFuncionario = funcionario.id"></asp:SqlDataSource>
</asp:Content>
