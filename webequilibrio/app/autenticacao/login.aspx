﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="webequilibrio.app.autenticacao.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="login.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-3.1.1.min.js"></script>
    <link href="../../Content/awesome-bootstrap-checkbox.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="container form-signin">
            <h3 class="form-signin-heading">Entre com e-mail e senha</h3>

            <div class="form-group">
                <div class="col-sm-12">
                    <asp:TextBox ID="txtEmail" runat="server" class="form-control" placeholder="Email" type="email"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12" style="padding-top: 5px">
                    <asp:TextBox ID="txtSenha" runat="server" type="password" class="form-control" placeholder="Senha"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <div class=" col-sm-12">
                    <asp:Button ID="btnEntrar" runat="server" Text="Entrar" class="btn btn-large btn-primary" type="submit" OnClick="btnEntrar_Click" Width="100%" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-7" style="padding-top: 10px">
                    <ASP:CheckBox id="chkPersistCookie" runat="server" autopostback="false" Visible="False" />
                </div>
                <div class="col-sm-5 text-right" style="padding-top: 10px">
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn-link" PostBackUrl="~/App/url/autenticacao/recuperarsenha.aspx" Visible="False">Esqueci a senha</asp:LinkButton>
                </div>
            </div>
                     
         
            <div class="form-group">
                <div id="mensErr" class="col-sm-12 alert alert-warning">
                    <p><asp:Label ID="lblErr" runat="server" Text="Label"></asp:Label></p>
                </div>
            </div>


        </div>
    </form>

    <script>
        var uri = 'api/default';
        $(document).ready(function () {
            // We send an AJAX request.
            $.getJSON(uri).done(function (data) { });
        });
    </script>
</body>
</html>
