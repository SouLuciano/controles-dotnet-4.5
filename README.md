# Controle Diversos #

### Qual o objetivo deste repositório? 

Este repositório tem como objetivo a avalização técnica de recrutadores. 

### Para que serve este repositório? 

Este programa tem como finalidade o controle de equipamentos e acompanhamento de processos, com indicadores. 

### O que foi usado neste projeto? 

Visual Studio 2015, Linguagem C#,
WebForms, 
Devexpress versão 15.2.7.0, 
dotNet Framework 4.5.2,
MSSQL Server.

### Onde este projeto está sendo usado? ####

Este portal está sendo usado autualmente por uma empresa real, posso demonstrar o seu funcionamento caso solicitado.