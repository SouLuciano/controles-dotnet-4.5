﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace webequilibrio.app.residuo
{
    public partial class grafico : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            

            

        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            if(cbxMes.Text != "" && cbxSemana.Text != "")
            {
                data.dsEquilibrio.residuo_auxDataTable dtResiduoAux = new data.dsEquilibrio.residuo_auxDataTable();
                data.dsEquilibrioTableAdapters.residuo_auxTableAdapter taResiduoAux = new data.dsEquilibrioTableAdapters.residuo_auxTableAdapter();

                int temRegistro = Convert.ToInt32(taResiduoAux.ScalarQueryMES_SEMANA(cbxMes.Text, cbxSemana.Text));
                if (temRegistro == 0)
                {
                    lblPercAracruz.Text = "";
                    return;
                }


                decimal CascaSolta;
                decimal AreiaTerra;
                decimal MadeiraInapta;
                decimal CascaPresa;
                decimal Outros;

                data.dsEquilibrio.GraficoDataTable dtGrafico = new data.dsEquilibrio.GraficoDataTable();
                data.dsEquilibrioTableAdapters.GraficoTableAdapter taGrafico = new data.dsEquilibrioTableAdapters.GraficoTableAdapter();
                dtGrafico = taGrafico.GetDataProjResiduo(cbxSemana.Text, "2017");

                if (dtGrafico.Rows.Count == 0)
                    return;

                GrafProjResiduo.DataSource = dtGrafico;
                GrafProjResiduo.DataBind();

                //*

                try
                {
                    data.dsEquilibrio.GraficoDataTable dtGraficoTipoRes = new data.dsEquilibrio.GraficoDataTable();
                    data.dsEquilibrioTableAdapters.GraficoTableAdapter taGraficoTipoRes = new data.dsEquilibrioTableAdapters.GraficoTableAdapter();
                    dtGraficoTipoRes = taGraficoTipoRes.GetDataByPorTipoResiduo(cbxSemana.Text, "2017", cbxMes.Text);

                    data.dsEquilibrio.PorTipoResiduoDataTable dt_tipoRes = new data.dsEquilibrio.PorTipoResiduoDataTable();
                    CascaSolta = Math.Round(Convert.ToDecimal(dtGraficoTipoRes.Rows[0]["CascaSolta"]), 2);
                    AreiaTerra = Math.Round(Convert.ToDecimal(dtGraficoTipoRes.Rows[0]["AreiaTerra"]), 2);
                    MadeiraInapta = Math.Round(Convert.ToDecimal(dtGraficoTipoRes.Rows[0]["MadeiraInapta"]), 2);
                    CascaPresa = Math.Round(Convert.ToDecimal(dtGraficoTipoRes.Rows[0]["CascaPresa"]), 2);
                    Outros = Math.Round(Convert.ToDecimal(dtGraficoTipoRes.Rows[0]["Outros"]), 2);

                    dt_tipoRes.Rows.Add("Casca Solta", CascaSolta);
                    dt_tipoRes.Rows.Add("Areia/Terra", AreiaTerra);
                    dt_tipoRes.Rows.Add("Madeira Inadequada", MadeiraInapta);
                    dt_tipoRes.Rows.Add("Casca Presa", CascaPresa);
                    dt_tipoRes.Rows.Add("Outros", Outros);

                    GrafPorTipoResiduo.DataSource = dt_tipoRes;
                    GrafPorTipoResiduo.DataBind();
                }
                catch
                {
                    return;
                }
                //*

                data.dsEquilibrio.GraficoDataTable dtGraficoTipoResMes = new data.dsEquilibrio.GraficoDataTable();
                data.dsEquilibrioTableAdapters.GraficoTableAdapter taGraficoTipoResMes = new data.dsEquilibrioTableAdapters.GraficoTableAdapter();
                dtGraficoTipoResMes = taGraficoTipoResMes.GetDataByTipoResMes("2017", cbxMes.Text);

                data.dsEquilibrio.PorTipoResiduoDataTable dt_tipoResMes = new data.dsEquilibrio.PorTipoResiduoDataTable();

                CascaSolta = Math.Round(Convert.ToDecimal(dtGraficoTipoResMes.Rows[0]["CascaSolta"]), 2);
                AreiaTerra = Math.Round(Convert.ToDecimal(dtGraficoTipoResMes.Rows[0]["AreiaTerra"]), 2);
                MadeiraInapta = Math.Round(Convert.ToDecimal(dtGraficoTipoResMes.Rows[0]["MadeiraInapta"]), 2);
                CascaPresa = Math.Round(Convert.ToDecimal(dtGraficoTipoResMes.Rows[0]["CascaPresa"]), 2);
                Outros = Math.Round(Convert.ToDecimal(dtGraficoTipoResMes.Rows[0]["Outros"]), 2);

                dt_tipoResMes.Rows.Add("Casca Solta", CascaSolta);
                dt_tipoResMes.Rows.Add("Areia/Terra", AreiaTerra);
                dt_tipoResMes.Rows.Add("Madeira Inadequada", MadeiraInapta);
                dt_tipoResMes.Rows.Add("Casca Presa", CascaPresa);
                dt_tipoResMes.Rows.Add("Outros", Outros);

                GrafPorTipoResiduoMes.DataSource = dt_tipoResMes;
                GrafPorTipoResiduoMes.DataBind();

                //*

                data.dsEquilibrio.GraficoDataTable dtGraficoAcumMesSemana = new data.dsEquilibrio.GraficoDataTable();
                data.dsEquilibrioTableAdapters.GraficoTableAdapter taGraficoAcumMesSemana = new data.dsEquilibrioTableAdapters.GraficoTableAdapter();
                dtGraficoTipoResMes = taGraficoTipoResMes.GetDataByResiduoMesPorSemana("2017", cbxMes.Text);
                data.dsEquilibrio.PorTipoResiduoDataTable dt_tipoAcumMesSemana = new data.dsEquilibrio.PorTipoResiduoDataTable();

                for (int i = 0; i < dtGraficoTipoResMes.Rows.Count; i++)
                {
                    decimal valor = Math.Round(Convert.ToDecimal(dtGraficoTipoResMes.Rows[i]["Soma"]), 2);
                    dt_tipoAcumMesSemana.Rows.Add(dtGraficoTipoResMes.Rows[i]["semanaInspec"], valor);
                }

                GrafAcumMesSemana.DataSource = dt_tipoAcumMesSemana;
                GrafAcumMesSemana.DataBind();

                //*

                data.dsEquilibrio.GraficoDataTable dtGraficoAno = new data.dsEquilibrio.GraficoDataTable();
                data.dsEquilibrioTableAdapters.GraficoTableAdapter taGraficoAno = new data.dsEquilibrioTableAdapters.GraficoTableAdapter();
                dtGraficoAno = taGraficoAno.GetDataByANO("2017");
                data.dsEquilibrio.PorTipoResiduoDataTable dt_tipoAno = new data.dsEquilibrio.PorTipoResiduoDataTable();

                for (int i = 0; i < dtGraficoAno.Rows.Count; i++)
                {
                    decimal valor = Math.Round(Convert.ToDecimal(dtGraficoAno.Rows[i]["valor"]), 2);
                    dt_tipoAno.Rows.Add(dtGraficoAno.Rows[i]["tipo"], valor);
                }

                GrafMensal.DataSource = dt_tipoAno;
                GrafMensal.DataBind();

                //*

                data.dsEquilibrio.GraficoDataTable dtGraficoMesUnidade = new data.dsEquilibrio.GraficoDataTable();
                data.dsEquilibrioTableAdapters.GraficoTableAdapter taGraficoMesUnidade = new data.dsEquilibrioTableAdapters.GraficoTableAdapter();
                dtGraficoMesUnidade = taGraficoTipoResMes.GetDataByPercMesUnidade("2017");

                lblPercAracruz.Text = "Percentual de Resíduos Aracruz: " + Math.Round(Convert.ToDecimal(dtGraficoMesUnidade.Rows[0]["Soma"]), 2).ToString() + "%";


            }
        }
    }
}