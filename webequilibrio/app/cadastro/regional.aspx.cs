﻿using System;
using System.Collections.Generic;
using System.Web;
using DevExpress.Web;
using System.Data;

namespace webequilibrio.app.cadastro
{
    public partial class regional : System.Web.UI.Page
    {
        app.classe.bdados dados = new app.classe.bdados();
        string url;
        string userId;
        string email;

        protected void Page_Load(object sender, EventArgs e)
        {
            userId = Session["_userId"].ToString();
            url = HttpContext.Current.Request.Url.AbsolutePath;
            bool acesso = dados.temAcesso(Convert.ToInt32(userId), "~" + url);

            if (!acesso)
            {
                Response.Redirect("~/default.aspx", true);
            }

            using (DataTable dtUser = (DataTable)Session["_user"])
            {
                email = dtUser.Rows[0]["email"].ToString();
            }
        }

        protected void gvRegional_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            e.NewValues["user_email"] = email;
            e.NewValues["dt_criacao"] = DateTime.Now;
            e.NewValues["dt_atualizacao"] = DateTime.Now;
        }

        protected void gvRegional_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "criou", "regional", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }

        protected void gvRegional_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "atualizou", "regional", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }

        protected void gvRegional_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            e.NewValues["user_email"] = email;
            e.NewValues["dt_atualizacao"] = DateTime.Now;
        }

        protected void gvRegional_CustomErrorText(object sender, DevExpress.Web.ASPxGridViewCustomErrorTextEventArgs e)
        {
            if (e.ErrorText.Contains("duplicate"))
            {
                if (e.ErrorText.Contains("IX_regional") || e.ErrorText.Contains("UNIQUE KEY"))
                    e.ErrorText = "Esta regional já está cadastrada";
            }
        }

        void AddError(Dictionary<GridViewColumn, string> errors, GridViewColumn column, string errorText)
        {
            if (errors.ContainsKey(column)) return;
            errors[column] = errorText;
        }

        protected void gvRegional_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            foreach (GridViewColumn column in gvRegional.Columns)
            {
                GridViewDataColumn dataColumn = column as GridViewDataColumn;
                if (dataColumn == null) continue;

                if (e.NewValues["regional"] == null)
                {
                    AddError(e.Errors, gvRegional.Columns["regional"], "Não pode ser em branco");
                }

                if (e.NewValues["ativo"] == null)
                {
                    AddError(e.Errors, gvRegional.Columns["ativo"], "Ativo não pode ser em branco");
                }

                if (dataColumn.FieldName == "regional")
                    if (e.NewValues["regional"] != null)
                    {
                        if (e.Keys["id"] != null)
                        {
                            if (dados.checkDadoRepetido("regional", e.NewValues["regional"].ToString(), "regional", userId, e.Keys["id"].ToString()))
                                AddError(e.Errors, gvRegional.Columns["regional"], "Esta regional já está cadastrada");
                        }
                        else
                        {
                            if (dados.checkDadoRepetido("regional", e.NewValues["regional"].ToString(), "regional", userId, ""))
                                AddError(e.Errors, gvRegional.Columns["regional"], "Esta regional já está cadastrada");
                        }
                    }

            }
        }
    }
}