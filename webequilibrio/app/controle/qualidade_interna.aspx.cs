﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web;
using DevExpress.XtraPrinting;
using DevExpress.Export;
using System.Web.UI;

namespace webequilibrio.app.controle
{
    public partial class qualidade_interna : System.Web.UI.Page
    {
        string url;
        string userId;
        app.classe.bdados dados = new classe.bdados();
        string email;
        string TipoDeacessoTblNC;

        protected void Page_Load(object sender, EventArgs e)
        {
            userId = Session["_userId"].ToString();
            url = HttpContext.Current.Request.Url.AbsolutePath;
            bool acesso = dados.temAcesso(Convert.ToInt32(userId), "~" + url);

            if (!acesso)
            {
                Response.Redirect("~/default.aspx", true);
            }
            
            TipoDeacessoTblNC = dados.getTipoDeAcessoQualidadeInterna(Session["_userId"].ToString());

            buscarNumDias(spinEditPrazo.Value.ToString());

            if (!IsPostBack)
            {
                ASPxPageControl1.ActiveTabIndex = 0;
                
            }

            if (TipoDeacessoTblNC == "criarEditarQualquerNc")
                ASPxPageControl1.TabPages[1].Visible = true;
            else
                ASPxPageControl1.TabPages[1].Visible = false;

            using (DataTable dtUser = (DataTable)Session["_user"])
            {
                email = dtUser.Rows[0]["email"].ToString();
            }
        }

        protected void gv_ncFilho_BeforePerformDataSelect(object sender, EventArgs e)
        {
            Session["id_qual_verif_monitora"] = (sender as DevExpress.Web.ASPxGridView).GetMasterRowKeyValue();
        }

        protected void gv_NC_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            e.NewValues["user_email"] = email;
            e.NewValues["ativo"] = true;
            e.NewValues["dt_atualizacao"] = DateTime.Now;
            dados.setLog(Convert.ToInt32(userId), "inf", "criou", "não conformidade", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }

        protected void gv_NC_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            e.NewValues["user_email"] = email;
            e.NewValues["dt_atualizacao"] = DateTime.Now;
            dados.setLog(Convert.ToInt32(userId), "inf", "atualizou", "não conformidade", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }

        protected void gv_ncFilho_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            e.NewValues["user_email"] = email;
            e.NewValues["ativo"] = true;
            e.NewValues["dt_atualizacao"] = DateTime.Now;
            e.NewValues["ativo"] = true;
            e.NewValues["id_qual_verif_monitora"] = (sender as DevExpress.Web.ASPxGridView).GetMasterRowKeyValue();
            dados.setLog(Convert.ToInt32(userId), "inf", "criou", "não conformidade filho", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }

        protected void gv_ncFilho_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            e.NewValues["user_email"] = email;
            e.NewValues["dt_atualizacao"] = DateTime.Now;
            dados.setLog(Convert.ToInt32(userId), "inf", "atualizou", "não conformidade filho", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }

        protected void gv_NC_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName != "dt_amostra") return;

            DateTime dt_amostra = Convert.ToDateTime(e.CellValue);
            DateTime dt_limite = AddBusinessDays(dt_amostra, 7);

            if (dt_limite < DateTime.Now)
                e.Cell.ForeColor = System.Drawing.Color.Red;
        }

        public static DateTime AddBusinessDays(DateTime date, int days)
        {
            if (days < 0)
            {
                throw new ArgumentException("days cannot be negative", "days");
            }
            if (days == 0) return date;
            if (date.DayOfWeek == DayOfWeek.Saturday)
            {
                date = date.AddDays(2);
                days -= 1;
            }
            else if (date.DayOfWeek == DayOfWeek.Sunday)
            {
                date = date.AddDays(1);
                days -= 1;
            }
            date = date.AddDays(days / 5 * 7);
            int extraDays = days % 5;

            if ((int)date.DayOfWeek + extraDays > 5)
            {
                extraDays += 2;
            }
            return date.AddDays(extraDays);
        }

        protected void dsNC_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            // VISUALIZA POR REGIONAL OU TODAS.
            string sql = @"
            SELECT id, id_projeto, id_regional, dt_amostra, id_filtro, qtd_talhao, qtd_nc, verificado, em_aberto, dt_atualizacao, user_email, ativo 
            FROM qual_verif_monitora ";

            switch (TipoDeacessoTblNC)
            {
                case "tratarNcPorRegional":
                    sql = sql + $"WHERE id_regional = {Session["_userIdRegional"].ToString()} ";
                    break;
                case "criarEditarQualquerNc":
                    break;
                default: // visualizarNcPorRegional
                    sql = sql + $"WHERE id_regional = {Session["_userIdRegional"].ToString()} ";
                    break;
            }

            sql = sql + " ORDER BY id DESC";

            e.Command.CommandText = sql;
        }

        protected void gv_NC_CommandButtonInitialize(object sender, DevExpress.Web.ASPxGridViewCommandButtonEventArgs e)
        {
            if (TipoDeacessoTblNC != "criarEditarQualquerNc")
            {
                // ocultar botões
                if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Edit)
                    e.Visible = false;

                if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.New)
                    e.Visible = false;
            }
        }

        protected void gv_ncFilho_CommandButtonInitialize(object sender, DevExpress.Web.ASPxGridViewCommandButtonEventArgs e)
        {
            if (TipoDeacessoTblNC == "visualizarNcPorRegional" || TipoDeacessoTblNC == "")
            {
                // ocultar botões
                if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Edit)
                    e.Visible = false;

                if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.New)
                    e.Visible = false;
            }
            else if (TipoDeacessoTblNC == "tratarNcPorRegional")
            {
                if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.New)
                    e.Visible = false;
            }
        }

        protected void gv_ncFilho_CellEditorInitialize(object sender, DevExpress.Web.ASPxGridViewEditorEventArgs e)
        {
            if (TipoDeacessoTblNC == "tratarNcPorRegional")
            {
                ASPxGridView grid = sender as ASPxGridView;
                if (e.Column.FieldName == "area" || e.Column.FieldName == "talhao" || e.Column.FieldName == "ocorrencia")
                    e.Editor.ReadOnly = true;
            }
        }

        protected void gv_ncFilho_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.VisibleIndex >= 0)
            {
                string s_id = e.GetValue("id").ToString();
                e.Row.Cells[6].Attributes.Add("onclick", "javascript:window.open('qualidade_interna_anexo.aspx?id_foto=" + s_id + "')");
            }
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            if (cbExportar.Text == "Expandido")
            {
                gv_NC.SettingsDetail.ExportMode = GridViewDetailExportMode.All;
            }

            dados.setLog(Convert.ToInt32(userId), "inf", "exportou", "Não conformidade interna", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            gvExporterNc.WriteXlsxToResponse(new XlsxExportOptionsEx() { ExportType = ExportType.WYSIWYG });

            gv_NC.SettingsDetail.ExportMode = GridViewDetailExportMode.None;
        }

        protected void btnGravarDias_Click(object sender, EventArgs e)
        {
            dados.setNumDiasAuditoriaInterna(spinEditPrazo.Value.ToString());
        }

        protected void buscarNumDias(string numAtual)
        {
            DataView dv = (DataView)dsDias.Select(DataSourceSelectArguments.Empty);
            string numDias = Convert.ToString(dv.Table.Rows[0][2]);

            if (numDias == numAtual || numAtual == "0")
            {
                spinEditPrazo.Value = numDias;
            }
            else
            {
                spinEditPrazo.Value = numAtual;
            }
        }

        protected void gvProcesso_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            e.NewValues["tipo"] = "qual_verif_monitora_processo";
        }
    }
}