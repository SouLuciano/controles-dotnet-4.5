﻿using System;
using System.Web;
using System.Data;
using DevExpress.Export;
using DevExpress.Web;
using DevExpress.XtraPrinting;

namespace webequilibrio.app.controle
{
    public partial class veiculo_km_rodado : System.Web.UI.Page
    {
        app.classe.bdados dados = new app.classe.bdados();
        string url;
        string userId;
        string email;

        protected void Page_Load(object sender, EventArgs e)
        {
            userId = Session["_userId"].ToString();
            url = HttpContext.Current.Request.Url.AbsolutePath;
            bool acesso = dados.temAcesso(Convert.ToInt32(userId), "~" + url);

            if (!acesso)
            {
                Response.Redirect("~/default.aspx", true);
            }

            using (DataTable dtUser = (DataTable)Session["_user"])
            {
                email = dtUser.Rows[0]["email"].ToString();
            }
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "exportou", "veiculo_km_rodado", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            gvExporter.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }

        protected void gvVeiculoKmRodado_Init(object sender, EventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;
            (grid.Columns["ano"] as GridViewDataSpinEditColumn).PropertiesSpinEdit.MaxValue = DateTime.Now.Year;
            (grid.Columns["km_rodado"] as GridViewDataSpinEditColumn).PropertiesSpinEdit.MaxValue = 9999;
        }

        protected void gvVeiculoKmRodado_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            string id_regional = "";
            try
            {
                e.NewValues["user_email"] = email;
                e.NewValues["dt_atualizacao"] = DateTime.Now;

                if (e.NewValues["id_equipe"] != null)
                {
                    int id_equipe = Convert.ToInt32(e.NewValues["id_equipe"]);

                    using (DataTable regional = dados.getRegional(id_equipe, userId))
                    {
                        id_regional = regional.Rows[0]["id"].ToString();
                        e.NewValues["id_regional"] = id_regional;
                    }
                }

                dados.setLog(Convert.ToInt32(userId), "inf", "Atualizou", "veiculo_km_rodado", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            }

            catch (Exception ex)
            {
                e.Cancel = true;
                dados.setLog(Convert.ToInt32(userId), "err", "gvKmRodado_RowUpdating()", ex.Message, url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            }
        }

        protected void gvVeiculoKmRodado_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            e.NewValues["ano"] = DateTime.Now.Year;
        }

        protected void gvVeiculoKmRodado_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            string id_regional = "";
            e.NewValues["dt_criacao"] = DateTime.Now;
            e.NewValues["dt_atualizacao"] = DateTime.Now;
            e.NewValues["user_email"] = email;

            if (e.NewValues["id_equipe"] != null)
            {
                int id_equipe = Convert.ToInt32(e.NewValues["id_equipe"]);

                using (DataTable regional = dados.getRegional(id_equipe, userId))
                {
                    id_regional = regional.Rows[0]["id"].ToString();
                    e.NewValues["id_regional"] = regional.Rows[0]["id"];
                }
            }

            dados.setLog(Convert.ToInt32(userId), "inf", "Criou", "veiculo_km_rodado", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }

        protected void gvVeiculoKmRodado_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "Deletou", "veiculo_km_rodado", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }
    }
}