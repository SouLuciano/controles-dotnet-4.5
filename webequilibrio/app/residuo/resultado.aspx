﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="resultado.aspx.cs" Inherits="webequilibrio.app.residuo.resultado" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" ShowCollapseButton="True" Width="100%" Theme="Office2010Blue" HeaderText="Controle de Qualidade da Colheita - Florestal Aracruz">
        <PanelCollection>
            <dx:PanelContent runat="server">
                <table class="nav-justified">
                    <tr>
                        <td>
                            <table style="width: 500px; margin-bottom: 10px;">
                                <tr>
                                    <td>
                                        <dx:ASPxComboBox ID="cbxMes" runat="server" DataSourceID="ds_vMes" NullText="Mês" TextField="mes">
                                        </dx:ASPxComboBox>
                                        <asp:SqlDataSource ID="ds_vMes" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                                        ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" 
                                        SelectCommand="select distinct
                                            `residuo_aux`.`mesInspec` AS `mes`
                                            from `residuo_aux`
                                            order by field(`residuo_aux`.`mesInspec`,'Janeiro','Fevereiro','Março','abril','maio','junho','julho','agosto','setembro','outubro','novembro','Dezembro')"></asp:SqlDataSource>
                                    </td>
                                    <td>
                                        <dx:ASPxComboBox ID="cbxSemana" runat="server" NullText="Semana">
                                            <Items>
                                                <dx:ListEditItem Text="1 Semana" Value="1 Semana" />
                                                <dx:ListEditItem Text="2 Semana" Value="2 Semana" />
                                                <dx:ListEditItem Text="3 Semana" Value="3 Semana" />
                                                <dx:ListEditItem Text="4 Semana" Value="4 Semana" />
                                                <dx:ListEditItem Text="5 Semana" Value="5 Semana" />
                                            </Items>
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnFiltrar" runat="server" CssClass="btn btn-default" Text="Filtrar" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvResultado" runat="server" AutoGenerateColumns="False" DataSourceID="dsResultado" KeyFieldName="id">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="0" Visible="False">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn Caption="Data monitoramento" FieldName="dt_monitoramento" ShowInCustomizationForm="True" VisibleIndex="1">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn Caption="Placa caminhão" FieldName="placa" ShowInCustomizationForm="True" VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Projeto" FieldName="projeto" ShowInCustomizationForm="True" VisibleIndex="3">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Talhão" FieldName="talhao" ShowInCustomizationForm="True" VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Mat. genético" FieldName="genetico" ShowInCustomizationForm="True" VisibleIndex="5">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Semana corte" FieldName="semana_corte" ShowInCustomizationForm="True" VisibleIndex="6">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Módulo" FieldName="modulo" ShowInCustomizationForm="True" VisibleIndex="7">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Peso líquido" FieldName="pesoliquido" ShowInCustomizationForm="True" VisibleIndex="8">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Resíduo Kg" FieldName="residuoKg" ShowInCustomizationForm="True" VisibleIndex="9">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Resíduo %" FieldName="residuoPorc" ShowInCustomizationForm="True" VisibleIndex="10">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Casca solta %" FieldName="cascaSoltaPorc" ShowInCustomizationForm="True" VisibleIndex="11">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Casca presa %" FieldName="cascaPresaPorc" ShowInCustomizationForm="True" VisibleIndex="12">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Madeira inapta" FieldName="madeira_inapto" ShowInCustomizationForm="True" VisibleIndex="13">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Areia %" FieldName="areiaPorc" ShowInCustomizationForm="True" VisibleIndex="14">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Outros %" FieldName="outroPorc" ShowInCustomizationForm="True" VisibleIndex="15">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="dsResultado" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                                ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" 
                                SelectCommand="SELECT residuo.id, residuo_aux.semanaInspec, residuo_aux.mesInspec, residuo.dt_monitoramento, residuo.placa, residuo.projeto, residuo.talhao, residuo.genetico, residuo.semana_corte, residuo.modulo, residuo_aux.pesoliquido, residuo_aux.residuoKg, residuo_aux.residuoPorc, residuo_aux.cascaSoltaPorc, residuo_aux.cascaPresaPorc, residuo.madeira_inapto, residuo_aux.areiaPorc, residuo_aux.outroPorc FROM residuo INNER JOIN residuo_aux ON residuo.id = residuo_aux.id_residuo WHERE (residuo_aux.mesInspec = @mesInspec) AND (residuo_aux.semanaInspec = @semanaInspec) ORDER BY residuo.id">

                                <SelectParameters>
                                    <asp:ControlParameter ControlID="cbxMes" Name="mesInspec" PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="cbxSemana" Name="semanaInspec" PropertyName="Value" Type="String" />
                                </SelectParameters>

                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>
</asp:Content>
