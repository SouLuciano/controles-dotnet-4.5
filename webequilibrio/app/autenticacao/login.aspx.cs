﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;

using System.Web.Script.Serialization;

namespace webequilibrio.app.autenticacao
{
    public partial class login : System.Web.UI.Page
    {
        string url = HttpContext.Current.Request.Url.AbsolutePath;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Mensagem", "$('#mensErr').hide();", true);

            btnEntrar.Focus();    
        }

        protected void btnEntrar_Click(object sender, EventArgs e)
        {
            
            app.classe.bdados dados = new classe.bdados();
            DataTable user = new DataTable();

            
            if (dados.checkLogin(txtEmail.Text, txtSenha.Text))
            {
                try
                {
                    user = dados.getDadosUserByIdOrEmail(txtEmail.Text);

                    if (dados.checkLogin(txtEmail.Text, txtSenha.Text))
                    {
                        if ((bool)user.Rows[0]["ativo"])
                        {
                            Session["_userId"] = user.Rows[0]["id"];
                            Session["_user"] = user;
                            Session["_userIdRegional"] = user.Rows[0]["id_regional"];

                            FormsAuthenticationTicket tkt = new FormsAuthenticationTicket(
                                1,
                                user.Rows[0]["nome"].ToString(),
                                DateTime.Now,
                                DateTime.Now.AddYears(50),
                                chkPersistCookie.Checked,
                                user.Rows[0]["id"].ToString());

                            string cookiestr = FormsAuthentication.Encrypt(tkt);

                            HttpCookie ck = new HttpCookie(FormsAuthentication.FormsCookieName, cookiestr);

                            if (chkPersistCookie.Checked)
                                ck.Expires = tkt.Expiration;

                            ck.Path = FormsAuthentication.FormsCookiePath;


                            Response.Cookies.Add(ck);

                            string strRedirect;
                            strRedirect = Request["ReturnUrl"];

                            if (strRedirect == null)
                                    strRedirect = "~/default.aspx";
                            
                            Response.Redirect(strRedirect, false);
                            dados.setLog(Convert.ToInt32(user.Rows[0]["id"]), "inf", "logou", null, url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
                            FormsAuthentication.RedirectFromLoginPage(user.Rows[0]["nome"].ToString(), chkPersistCookie.Checked);
                        }
                        else
                        {
                            lblErr.Text = "Esta conta está desativada";
                            ScriptManager.RegisterStartupScript(Page, this.GetType(), "DatePickerScript", "$('#mensErr').show();", true);
                        }
                    }
                    else
                    {
                        lblErr.Text = "A conta está desativada";
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "DatePickerScript", "$('#mensErr').show();", true);
                    }


                }
                catch (Exception ex)
                {
                    dados.setLog(Convert.ToInt32(user.Rows[0]["id"]), "err", "btnEntrar_Click", ex.Message, url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
                }
                finally
                {
                    user.Dispose();
                    dados = null;
                }
            }
            else
            {
                if ((txtEmail.Text.Trim() == "") || (txtSenha.Text.Trim() == ""))
                    lblErr.Text = "Digite o email e a senha.";
                else
                    lblErr.Text = "Email ou senha errado";

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Mensagem", "$('#mensErr').show();", true);
            }
            
        }
    }
}