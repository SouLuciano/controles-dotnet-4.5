﻿using System;

namespace webequilibrio.app.classe
{
    public class veiculo
    {
        public int id { get; set; }
        public int id_veiculo { get; set; }
        public int? id_projeto { get; set; }
        public string placa { get; set; }
        public string modelo { get; set; }
        public string cartao { get; set; }
        public Boolean ativo { get; set; }
        public int? id_equipe { get; set; }
        public int? id_celular { get; set; }
        public string user_email { get; set; }
        public DateTime dt_criacao { get; set; }
        public string observacao { get; set; }
        public int id_regional { get; set; }
        public DateTime? dt_contrato_ini { get; set; }
        public DateTime? dt_contrato_fim { get; set; }
        public string combustivel { get; set; }
        public string alugado { get; set; }
        public string status { get; set; }

    }
}