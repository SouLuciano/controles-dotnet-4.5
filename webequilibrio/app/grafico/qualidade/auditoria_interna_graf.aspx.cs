﻿using System;
using System.Web;
using System.Collections;

namespace webequilibrio.app.grafico.qualidade
{
    public partial class auditoria_interna_graf : System.Web.UI.Page
    {
        app.classe.bdados dados = new app.classe.bdados();
        string url;
        string userId;

        protected void Page_Load(object sender, EventArgs e)
        {
            userId = Session["_userId"].ToString();
            url = HttpContext.Current.Request.Url.AbsolutePath;
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "exportou", "Dados cruzados auditoria interna", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            ASPxPivotGridExporter1.ExportXlsxToResponse("Auditoria interna"); // .WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }

        protected void gvAuditoria_CustomFieldSort(object sender, DevExpress.Web.ASPxPivotGrid.PivotGridCustomFieldSortEventArgs e)
        {
            object orderValue1 = e.GetListSourceColumnValue(e.ListSourceRowIndex1, "numMes"),
            orderValue2 = e.GetListSourceColumnValue(e.ListSourceRowIndex2, "numMes");
            e.Result = Comparer.Default.Compare(orderValue1, orderValue2);
            e.Handled = true;
        }
    }
}