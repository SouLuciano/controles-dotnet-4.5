﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Grafico.master" AutoEventWireup="true" CodeBehind="veiculo_km_rodado.aspx.cs" Inherits="webequilibrio.app.grafico.veiculo.veiculo_km_rodado" %>

<%@ Register Assembly="DevExpress.XtraCharts.v15.2.Web, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
<%@ Register assembly="DevExpress.Web.ASPxPivotGrid.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPivotGrid" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="GraficoContent" runat="server">
	<p>
        <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="1">
            <TabPages>
                <dx:TabPage Text="Gráficos">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <table cellpadding="2" cellspacing="3" style="width: 100%">
                                <tr>
                                    <td>
                                        <table cellpadding="2" cellspacing="3" style="width: 100%">
                                            <tr>
                                                <td>
                                                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Ano" Theme="PlasticBlue">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td>
                                                    <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Mês" Theme="PlasticBlue">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td>
                                                    <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Regional" Theme="PlasticBlue">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <dx:ASPxComboBox ID="cbxAno" runat="server" DataSourceID="dsAno" TextField="ano" Theme="PlasticBlue" ValueField="ano" ValueType="System.Int16" Width="100px">
                                                    </dx:ASPxComboBox>
                                                    <asp:SqlDataSource ID="dsAno" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT DISTINCT ano FROM veiculo_km_rodado ORDER BY ano"></asp:SqlDataSource>
                                                </td>
                                                <td>
                                                    <dx:ASPxComboBox ID="cbxMes" runat="server" EnableTheming="True" Theme="PlasticBlue">
                                                        <Items>
                                                            <dx:ListEditItem Text="Janeiro" Value="Jan" />
                                                            <dx:ListEditItem Text="Fevereiro" Value="Fev" />
                                                            <dx:ListEditItem Text="Março" Value="Mar" />
                                                            <dx:ListEditItem Text="Abril" Value="Abr" />
                                                            <dx:ListEditItem Text="Maio" Value="Mai" />
                                                            <dx:ListEditItem Text="Junho" Value="Jun" />
                                                            <dx:ListEditItem Text="Julho" Value="Jul" />
                                                            <dx:ListEditItem Text="Agosto" Value="Ago" />
                                                            <dx:ListEditItem Text="Setembro" Value="Set" />
                                                            <dx:ListEditItem Text="Outubro" Value="Out" />
                                                            <dx:ListEditItem Text="Novembro" Value="Nov" />
                                                            <dx:ListEditItem Text="Dezembro" Value="Dez" />
                                                        </Items>
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td>
                                                    <dx:ASPxComboBox ID="cbxRegional" runat="server" DataSourceID="dsCbxRegional" EnableTheming="True" TextField="regional" Theme="PlasticBlue" ValueField="id" ValueType="System.Int32" Width="100px">
                                                    </dx:ASPxComboBox>
                                                    <asp:SqlDataSource ID="dsCbxRegional" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" OnSelecting="dsCbxRegional_Selecting" SelectCommand="SELECT [id], [regional], [ativo] FROM [regional] WHERE ([ativo] = @ativo) ORDER BY [regional]">
                                                        <SelectParameters>
                                                            <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                                                        </SelectParameters>
                                                    </asp:SqlDataSource>
                                                </td>
                                                <td>
                                                    <dx:ASPxCheckBox ID="chkQuadriciclo" runat="server" CheckState="Unchecked" Text="Quadriciclo" Theme="PlasticBlue">
                                                    </dx:ASPxCheckBox>
                                                </td>
                                                <td>
                                                    <dx:ASPxButton ID="btnFiltrar" runat="server" OnClick="btnFiltrar_Click" Text="Filtrar" Theme="PlasticBlue">
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="height: 19px">&nbsp;</td>
                                                <td style="height: 19px">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <dxchartsui:WebChartControl ID="wcPorProjeto0" runat="server" CrosshairEnabled="True" DataSourceID="dsPorAno" Height="150px" Width="1100px">
                                            <DiagramSerializable>
                                                <cc1:XYDiagram>
                                                <axisx visibleinpanesserializable="-1">
                                                    <numericscaleoptions scalemode="Manual" />
                                                </axisx>
                                                <axisy title-visibility="Default" visibleinpanesserializable="-1">
                                                    <wholerange alwaysshowzerolevel="False" />
                                                </axisy>
                                                </cc1:XYDiagram>
                                            </DiagramSerializable>
                                            <Legend Visibility="False"></Legend>
                                            <SeriesSerializable>
                                                <cc1:Series ArgumentDataMember="ano" LabelsVisibility="True" Name="Series 2" SeriesPointsSortingKey="Value_1" ToolTipHintDataMember="kmRodado" ValueDataMembersSerializable="kmRodado">
                                                <viewserializable>
                                                    <cc1:LineSeriesView>
                                                    </cc1:LineSeriesView>
                                                </viewserializable>
                                                </cc1:Series>
                                            </SeriesSerializable>
                                            <Titles>
                                                <cc1:ChartTitle Font="Segoe UI, 15.75pt" Text="Média km rodado por ano" />
                                            </Titles>
                                        </dxchartsui:WebChartControl>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <dxchartsui:WebChartControl ID="wcMediaKmPorMes" runat="server" CrosshairEnabled="True" DataSourceID="dsPorMes" Height="150px" Width="1100px">
                                            <DiagramSerializable>
                                                <cc1:XYDiagram>
                                                <axisx visibleinpanesserializable="-1">
                                                </axisx>
                                                <axisy visibleinpanesserializable="-1">
                                                    <wholerange alwaysshowzerolevel="False" />
                                                </axisy>
                                                </cc1:XYDiagram>
                                            </DiagramSerializable>
                                            <Legend Visibility="False"></Legend>
                                            <SeriesSerializable>
                                                <cc1:Series ArgumentDataMember="mes" LabelsVisibility="True" Name="Series 1" ValueDataMembersSerializable="kmRodado">
                                                <viewserializable>
                                                    <cc1:LineSeriesView>
                                                    </cc1:LineSeriesView>
                                                </viewserializable>
                                                </cc1:Series>
                                            </SeriesSerializable>
                                            <Titles>
                                                <cc1:ChartTitle Font="Segoe UI, 15.75pt" Text="Média km rodado por mês" />
                                            </Titles>
                                        </dxchartsui:WebChartControl>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <dxchartsui:WebChartControl ID="wcSomaKmPorMes" runat="server" CrosshairEnabled="True" DataSourceID="dsPorMesSoma" Height="150px" Width="1100px">
                                            <DiagramSerializable>
                                                <cc1:XYDiagram>
                                                <axisx visibleinpanesserializable="-1">
                                                </axisx>
                                                <axisy visibleinpanesserializable="-1">
                                                    <wholerange alwaysshowzerolevel="False" />
                                                </axisy>
                                                </cc1:XYDiagram>
                                            </DiagramSerializable>
                                            <Legend Visibility="False"></Legend>
                                            <SeriesSerializable>
                                                <cc1:Series ArgumentDataMember="mes" LabelsVisibility="True" Name="Series 1" ValueDataMembersSerializable="kmRodado">
                                                <viewserializable>
                                                    <cc1:LineSeriesView>
                                                    </cc1:LineSeriesView>
                                                </viewserializable>
                                                </cc1:Series>
                                            </SeriesSerializable>
                                            <Titles>
                                                <cc1:ChartTitle Font="Segoe UI, 15.75pt" Text="Soma km rodado por mês" />
                                            </Titles>
                                        </dxchartsui:WebChartControl>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <dxchartsui:WebChartControl ID="wcPorProjeto" runat="server" CrosshairEnabled="True" DataSourceID="dsPorProjeto" Height="350px" Width="1100px">
                                            <DiagramSerializable>
                                                <cc1:XYDiagram>
                                                <axisx visibleinpanesserializable="-1">
                                                </axisx>
                                                <axisy visibleinpanesserializable="-1">
                                                </axisy>
                                                </cc1:XYDiagram>
                                            </DiagramSerializable>
                                            <Legend Visibility="False"></Legend>
                                            <SeriesSerializable>
                                                <cc1:Series ArgumentDataMember="projeto" LabelsVisibility="True" Name="Series 1" ValueDataMembersSerializable="kmRodado" />
                                            </SeriesSerializable>
                                            <Titles>
                                                <cc1:ChartTitle Font="Segoe UI, 15.75pt" Text="Média rodado por projeto" />
                                            </Titles>
                                        </dxchartsui:WebChartControl>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <dxchartsui:WebChartControl ID="wcSomaPorEquipeNoMes" runat="server" CrosshairEnabled="True" DataSourceID="dsPorEquipe" Height="350px" SeriesDataMember="projeto" Width="1100px">
                                            <DiagramSerializable>
                                                <cc1:XYDiagram>
                                                <axisx title-visibility="Default" visibility="True" visibleinpanesserializable="-1">
                                                </axisx>
                                                <axisy visibleinpanesserializable="-1">
                                                </axisy>
                                                </cc1:XYDiagram>
                                            </DiagramSerializable>
                                            <Legend AlignmentHorizontal="Right" Visibility="True"></Legend>
                                            <SeriesTemplate ArgumentDataMember="equipe" LabelsVisibility="True" ValueDataMembersSerializable="kmRodado">
                                            <viewserializable>
                                                <cc1:StackedBarSeriesView>
                                                </cc1:StackedBarSeriesView>
                                            </viewserializable>
                                            <labelserializable>
                                                <cc1:StackedBarSeriesLabel Position="Auto" TextOrientation="TopToBottom">
                                                </cc1:StackedBarSeriesLabel>
                                            </labelserializable>
                                            </SeriesTemplate>
                                            <Titles>
                                                <cc1:ChartTitle Font="Segoe UI, 15.75pt" Text="Soma rodado por equipe" />
                                            </Titles>
                                            <CrosshairOptions ShowArgumentLabels="True" ShowValueLabels="True" ShowValueLine="True" />
                                        </dxchartsui:WebChartControl>
                                        <asp:SqlDataSource ID="dsPorEquipe" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" OnSelecting="dsPorEquipe_Selecting" SelectCommand="SELECT equipe.projeto_equipe AS projeto, equipe.codigo AS equipe, SUM(veiculo_km_rodado.km_rodado) AS kmRodado
FROM veiculo, veiculo_km_rodado, equipe
WHERE
	veiculo_km_rodado.id_equipe = equipe.id AND 
	veiculo_km_rodado.ano = 2017 AND 
	veiculo_km_rodado.id_veiculo = veiculo.id AND
	veiculo_km_rodado.mes = 'out' AND
	veiculo.modelo &lt;&gt; 'Quadriciclo'
GROUP BY equipe.codigo, equipe.projeto_equipe

"></asp:SqlDataSource>
                                        <asp:SqlDataSource ID="dsPorMes" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" OnSelecting="dsPorMes_Selecting" SelectCommand="SELECT veiculo_km_rodado.mes, AVG(veiculo_km_rodado.km_rodado)AS kmRodado 
FROM equipe, veiculo_km_rodado 
WHERE veiculo_km_rodado.id_equipe = equipe.id AND mes = 'out' AND ano = 2016
GROUP BY ano, mes"></asp:SqlDataSource>
                                        <asp:SqlDataSource ID="dsPorMesSoma" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" OnSelecting="dsPorMesSoma_Selecting" SelectCommand="SELECT veiculo_km_rodado.mes, AVG(veiculo_km_rodado.km_rodado)AS kmRodado 
FROM equipe, veiculo_km_rodado 
WHERE veiculo_km_rodado.id_equipe = equipe.id AND mes = 'out' AND ano = 2016
GROUP BY ano, mes"></asp:SqlDataSource>
                                        <asp:SqlDataSource ID="dsPorAno" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" OnSelecting="dsPorAno_Selecting" SelectCommand="SELECT ano, AVG(veiculo_km_rodado.km_rodado)AS kmRodado 
                            FROM equipe, veiculo_km_rodado 
                            WHERE veiculo_km_rodado.id_equipe = equipe.id 
                            GROUP BY ano"></asp:SqlDataSource>
                                        <asp:SqlDataSource ID="dsPorProjeto" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" OnSelecting="dsPorProjeto_Selecting" SelectCommand="SELECT equipe.projeto_equipe AS projeto,  AVG(veiculo_km_rodado.km_rodado)AS kmRodado
						FROM equipe, veiculo_km_rodado
						WHERE veiculo_km_rodado.id_equipe = equipe.id AND veiculo_km_rodado.ano = 2017
						GROUP BY projeto_equipe"></asp:SqlDataSource>
                                    </td>
                                </tr>
                            </table>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Text="Dados cruzados">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <dx:ASPxPivotGrid ID="gvSomaKm" runat="server" ClientIDMode="AutoID" DataSourceID="dsGvSomaKm" EnableTheming="True" OnCustomFieldSort="gvSomaKm_CustomFieldSort" Theme="PlasticBlue">
                                <Fields>
                                    <dx:PivotGridField ID="fieldmes" Area="ColumnArea" AreaIndex="1" Caption="Mês" FieldName="mes" SortMode="Custom" UnboundFieldName="fieldMs">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldProjeto" AreaIndex="0" FieldName="Projeto">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldRegional" AreaIndex="1" FieldName="Regional">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldEquipe" Area="RowArea" AreaIndex="0" FieldName="Equipe">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldkmRodado" Area="DataArea" AreaIndex="0" Caption="Km Rodado" CellFormat-FormatString="{0:00.00}" CellFormat-FormatType="Numeric" FieldName="kmRodado">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldAno" Area="ColumnArea" AreaIndex="0" FieldName="Ano">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldModelo" AreaIndex="2" FieldName="Modelo">
                                    </dx:PivotGridField>
                                </Fields>
                                <OptionsPager RowsPerPage="18">
                                </OptionsPager>
                            </dx:ASPxPivotGrid>
                            <dx:ASPxButton ID="btn_ExpPGridSomaKm_Click" runat="server" OnClick="btnExpPGridSomaKm_Click" Text="Exportar para Excel" Theme="PlasticBlue">
                            </dx:ASPxButton>
                            <asp:SqlDataSource ID="dsGvSomaKm" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT projeto.nome AS Projeto, regional.regional as Regional, Ano, veiculo_km_rodado.mes,
( CASE mes
            WHEN 'jan' THEN 1
            WHEN 'fev' THEN 2
            WHEN 'mar' THEN 3
            WHEN 'abr' THEN 4
            WHEN 'mai' THEN 5
            WHEN 'jun' THEN 6
            WHEN 'jul' THEN 7
            WHEN 'ago' THEN 8
            WHEN 'set' THEN 9
            WHEN 'out' THEN 10
            WHEN 'nov' THEN 11
            WHEN 'dez' THEN 12
          END ) as [numMes],
(CASE modelo WHEN 'Quadriciclo' THEN 'Quadriciclo' ELSE 'Veiculo' END) AS [Modelo],
equipe.codigo AS Equipe, ROUND(SUM(CAST(veiculo_km_rodado.km_rodado AS FLOAT)), 2) AS kmRodado 
FROM equipe, veiculo_km_rodado, veiculo, regional, projeto 
WHERE 
	veiculo_km_rodado.id_equipe = equipe.id 
AND veiculo_km_rodado.id_veiculo = veiculo.id
AND veiculo_km_rodado.id_regional = regional.id 
AND	veiculo_km_rodado.id_projeto = projeto.id
GROUP BY projeto.nome, ano, mes, regional, codigo, veiculo.modelo"></asp:SqlDataSource>
                            <dx:ASPxPivotGridExporter ID="ASPxPivotGridExporter1" runat="server" ASPxPivotGridID="gvSomaKm">
                                <OptionsPrint>
                                    <PageSettings PaperKind="A4" />
                                </OptionsPrint>
                            </dx:ASPxPivotGridExporter>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
            </TabPages>
        </dx:ASPxPageControl>
    </p>
</asp:Content>
