﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="qualidade_interna.aspx.cs" Inherits="webequilibrio.app.controle.qualidade_interna" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" EnableCallBacks="True" EnableTheming="True" Theme="Metropolis">
        <TabPages>
            <dx:TabPage Text="Não conformidade">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table class="dxflInternalEditorTable_Moderno">
                            <tr>
                                <td>
                                    <dx:ASPxGridView ID="gv_NC" runat="server" AutoGenerateColumns="False" DataSourceID="dsNC" EnableTheming="True" KeyFieldName="id" OnCommandButtonInitialize="gv_NC_CommandButtonInitialize" OnHtmlDataCellPrepared="gv_NC_HtmlDataCellPrepared" OnRowInserting="gv_NC_RowInserting" OnRowUpdating="gv_NC_RowUpdating" Theme="Metropolis">
                                        <SettingsDetail ShowDetailRow="True" />
                                        <Templates>
                                            <DetailRow>
                                                <dx:ASPxGridView ID="gv_ncFilho" runat="server" AutoGenerateColumns="False" DataSourceID="dsNcFilho" EnableTheming="True" KeyFieldName="id" OnBeforePerformDataSelect="gv_ncFilho_BeforePerformDataSelect" OnCellEditorInitialize="gv_ncFilho_CellEditorInitialize" OnCommandButtonInitialize="gv_ncFilho_CommandButtonInitialize" OnHtmlRowCreated="gv_ncFilho_HtmlRowCreated" OnRowInserting="gv_ncFilho_RowInserting" OnRowUpdating="gv_ncFilho_RowUpdating" Theme="Metropolis">
                                                    <SettingsEditing Mode="PopupEditForm">
                                                    </SettingsEditing>
                                                    <Columns>
                                                        <dx:GridViewCommandColumn ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                                        </dx:GridViewCommandColumn>
                                                        <dx:GridViewDataTextColumn Caption="Foto" FieldName="id" ReadOnly="True" VisibleIndex="8">
                                                            <EditFormSettings Visible="False" />
                                                            <CellStyle Cursor="pointer" Font-Underline="True" ForeColor="Blue">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="id_qual_verif_monitora" Visible="False" VisibleIndex="1">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Area" FieldName="area" VisibleIndex="2">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Talhão" FieldName="talhao" VisibleIndex="3">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataDateColumn Caption="Editado em" FieldName="dt_atualizacao" VisibleIndex="9">
                                                            <PropertiesDateEdit DisplayFormatString="g">
                                                            </PropertiesDateEdit>
                                                            <EditFormSettings Visible="False" />
                                                        </dx:GridViewDataDateColumn>
                                                        <dx:GridViewDataTextColumn Caption="Usuário" FieldName="user_email" VisibleIndex="10">
                                                            <EditFormSettings Visible="False" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataComboBoxColumn Caption="Procede" FieldName="procede" VisibleIndex="5">
                                                            <PropertiesComboBox AllowNull="True">
                                                                <Items>
                                                                    <dx:ListEditItem Text="Sim" Value="Sim" />
                                                                    <dx:ListEditItem Text="Não" Value="Não" />
                                                                </Items>
                                                            </PropertiesComboBox>
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataTextColumn FieldName="ativo" Visible="False" VisibleIndex="11">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataMemoColumn Caption="Descrição da ocorrência" FieldName="ocorrencia" VisibleIndex="4">
                                                        </dx:GridViewDataMemoColumn>
                                                        <dx:GridViewDataMemoColumn Caption="Tratativa e Nº do effetivo" FieldName="tratativa" VisibleIndex="6">
                                                        </dx:GridViewDataMemoColumn>
                                                    </Columns>
                                                </dx:ASPxGridView>
                                                <asp:SqlDataSource ID="dsNcFilho" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" DeleteCommand="DELETE FROM [qual_verif_monitora_filho] WHERE [id] = @id" InsertCommand="INSERT INTO [qual_verif_monitora_filho] ([id_qual_verif_monitora], [area], [talhao], [ocorrencia], [procede], [tratativa], [dt_atualizacao], [user_email], [ativo]) VALUES (@id_qual_verif_monitora, @area, @talhao, @ocorrencia, @procede, @tratativa, @dt_atualizacao, @user_email, @ativo)" SelectCommand="SELECT [id], [id_qual_verif_monitora], [area], [talhao], [ocorrencia], [procede], [tratativa], [dt_atualizacao], [user_email], [ativo] FROM [qual_verif_monitora_filho] WHERE ([id_qual_verif_monitora] = @id_qual_verif_monitora)" UpdateCommand="UPDATE [qual_verif_monitora_filho] SET [id_qual_verif_monitora] = @id_qual_verif_monitora, [area] = @area, [talhao] = @talhao, [ocorrencia] = @ocorrencia, [procede] = @procede, [tratativa] = @tratativa, [dt_atualizacao] = @dt_atualizacao, [user_email] = @user_email, [ativo] = @ativo WHERE [id] = @id">
                                                    <DeleteParameters>
                                                        <asp:Parameter Name="id" Type="Int32" />
                                                    </DeleteParameters>
                                                    <InsertParameters>
                                                        <asp:Parameter Name="id_qual_verif_monitora" Type="Int32" />
                                                        <asp:Parameter Name="area" Type="String" />
                                                        <asp:Parameter Name="talhao" Type="String" />
                                                        <asp:Parameter Name="ocorrencia" Type="String" />
                                                        <asp:Parameter Name="procede" Type="String" />
                                                        <asp:Parameter Name="tratativa" Type="String" />
                                                        <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                                                        <asp:Parameter Name="user_email" Type="String" />
                                                        <asp:Parameter Name="ativo" Type="Boolean" />
                                                    </InsertParameters>
                                                    <SelectParameters>
                                                        <asp:SessionParameter Name="id_qual_verif_monitora" SessionField="id_qual_verif_monitora" Type="Int32" />
                                                    </SelectParameters>
                                                    <UpdateParameters>
                                                        <asp:Parameter Name="id_qual_verif_monitora" Type="Int32" />
                                                        <asp:Parameter Name="area" Type="String" />
                                                        <asp:Parameter Name="talhao" Type="String" />
                                                        <asp:Parameter Name="ocorrencia" Type="String" />
                                                        <asp:Parameter Name="procede" Type="String" />
                                                        <asp:Parameter Name="tratativa" Type="String" />
                                                        <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                                                        <asp:Parameter Name="user_email" Type="String" />
                                                        <asp:Parameter Name="ativo" Type="Boolean" />
                                                        <asp:Parameter Name="id" Type="Int32" />
                                                    </UpdateParameters>
                                                </asp:SqlDataSource>
                                            </DetailRow>
                                        </Templates>
                                        <SettingsEditing Mode="PopupEditForm">
                                        </SettingsEditing>
                                        <Settings ShowFilterRow="True" ShowFilterRowMenu="True" />
                                        <SettingsBehavior AutoFilterRowInputDelay="2000" />
                                        <SettingsDataSecurity AllowDelete="False" />
                                        <SettingsSearchPanel Delay="2000" Visible="True" />
                                        <Columns>
                                            <dx:GridViewCommandColumn ShowClearFilterButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                            </dx:GridViewCommandColumn>
                                            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="12">
                                                <EditFormSettings Visible="False" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataDateColumn Caption="Dt. Amostra" FieldName="dt_amostra" ShowInCustomizationForm="True" VisibleIndex="3">
                                            </dx:GridViewDataDateColumn>
                                            <dx:GridViewDataDateColumn Caption="Atualização" FieldName="dt_atualizacao" ShowInCustomizationForm="True" VisibleIndex="9">
                                                <PropertiesDateEdit DisplayFormatString="g">
                                                </PropertiesDateEdit>
                                                <EditFormSettings Visible="False" />
                                            </dx:GridViewDataDateColumn>
                                            <dx:GridViewDataTextColumn Caption="Usuáiro" FieldName="user_email" ShowInCustomizationForm="True" VisibleIndex="11">
                                                <EditFormSettings Visible="False" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="Projeto" FieldName="id_projeto" ShowInCustomizationForm="True" VisibleIndex="1">
                                                <PropertiesComboBox DataSourceID="dsProjeto" TextField="nome" ValueField="id">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="Regional" FieldName="id_regional" ShowInCustomizationForm="True" VisibleIndex="2">
                                                <PropertiesComboBox DataSourceID="dsRegional" TextField="regional" ValueField="id">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="Processo" FieldName="id_filtro" ShowInCustomizationForm="True" VisibleIndex="4">
                                                <PropertiesComboBox DataSourceID="dsFiltroProcesso" TextField="nome_filtro" ValueField="id">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="Verificado" FieldName="verificado" ShowInCustomizationForm="True" VisibleIndex="7">
                                                <PropertiesComboBox>
                                                    <Items>
                                                        <dx:ListEditItem Text="Sim" Value="Sim" />
                                                        <dx:ListEditItem Text="Não" Value="Não" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="Em aberto" FieldName="em_aberto" ShowInCustomizationForm="True" VisibleIndex="8">
                                                <PropertiesComboBox>
                                                    <Items>
                                                        <dx:ListEditItem Text="Sim" Value="Sim" />
                                                        <dx:ListEditItem Text="Não" Value="Não" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataCheckColumn FieldName="ativo" ShowInCustomizationForm="True" VisibleIndex="10">
                                            </dx:GridViewDataCheckColumn>
                                            <dx:GridViewDataSpinEditColumn Caption="Qtd. talhão" FieldName="qtd_talhao" ShowInCustomizationForm="True" VisibleIndex="5">
                                                <PropertiesSpinEdit DisplayFormatString="g" MaxValue="200" MinValue="1" NumberType="Integer">
                                                </PropertiesSpinEdit>
                                            </dx:GridViewDataSpinEditColumn>
                                            <dx:GridViewDataSpinEditColumn Caption="Qtd. NC" FieldName="qtd_nc" ShowInCustomizationForm="True" VisibleIndex="6">
                                                <PropertiesSpinEdit DisplayFormatString="g" MaxValue="200" MinValue="0" NumberType="Integer">
                                                </PropertiesSpinEdit>
                                            </dx:GridViewDataSpinEditColumn>
                                        </Columns>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="dxflInternalEditorTable_Moderno" style="width: 400px">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label1" runat="server" Text="Exportar, modo:"></asp:Label>
                                            </td>
                                            <td>
                                                <dx:ASPxComboBox ID="cbExportar" runat="server" DropDownStyle="DropDown" EnableTheming="True" NullText="Exportar para excel" SelectedIndex="0" Theme="Metropolis">
                                                    <Items>
                                                        <dx:ListEditItem Selected="True" Text="Agrupado" Value="0" />
                                                        <dx:ListEditItem Text="Expandido" Value="1" />
                                                    </Items>
                                                </dx:ASPxComboBox>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnExportar" runat="server" OnClick="btnExportar_Click" Text="Exportar" Theme="Metropolis">
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <asp:SqlDataSource ID="dsNC" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                            DeleteCommand="DELETE FROM [qual_verif_monitora] WHERE [id] = @id" 
                            InsertCommand="INSERT INTO [qual_verif_monitora] ([id_projeto], [id_regional], [dt_amostra], [id_filtro], [qtd_talhao], [qtd_nc], [verificado], [em_aberto], [dt_atualizacao], [user_email], [ativo]) VALUES (@id_projeto, @id_regional, @dt_amostra, @id_filtro, @qtd_talhao, @qtd_nc, @verificado, @em_aberto, @dt_atualizacao, @user_email, @ativo)" 
                            SelectCommand="SELECT [id], [id_projeto], [id_regional], [dt_amostra], [id_filtro], [qtd_talhao], [qtd_nc], [verificado], [em_aberto], [dt_atualizacao], [user_email], [ativo] FROM [qual_verif_monitora] ORDER BY [id] DESC" 
                            UpdateCommand="UPDATE [qual_verif_monitora] SET [id_projeto] = @id_projeto, [id_regional] = @id_regional, [dt_amostra] = @dt_amostra, [id_filtro] = @id_filtro, [qtd_talhao] = @qtd_talhao, [qtd_nc] = @qtd_nc, [verificado] = @verificado, [em_aberto] = @em_aberto, [dt_atualizacao] = @dt_atualizacao, [user_email] = @user_email, [ativo] = @ativo WHERE [id] = @id" OnSelecting="dsNC_Selecting">
                            <DeleteParameters>
                                <asp:Parameter Name="id" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="id_projeto" Type="Int32" />
                                <asp:Parameter Name="id_regional" Type="Int32" />
                                <asp:Parameter Name="dt_amostra" Type="DateTime" />
                                <asp:Parameter Name="id_filtro" Type="Int32" />
                                <asp:Parameter Name="qtd_talhao" Type="Int16" />
                                <asp:Parameter Name="qtd_nc" Type="Int16" />
                                <asp:Parameter Name="verificado" Type="String" />
                                <asp:Parameter Name="em_aberto" Type="String" />
                                <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                                <asp:Parameter Name="user_email" Type="String" />
                                <asp:Parameter Name="ativo" Type="Boolean" />
                            </InsertParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="id_projeto" Type="Int32" />
                                <asp:Parameter Name="id_regional" Type="Int32" />
                                <asp:Parameter Name="dt_amostra" Type="DateTime" />
                                <asp:Parameter Name="id_filtro" Type="Int32" />
                                <asp:Parameter Name="qtd_talhao" Type="Int16" />
                                <asp:Parameter Name="qtd_nc" Type="Int16" />
                                <asp:Parameter Name="verificado" Type="String" />
                                <asp:Parameter Name="em_aberto" Type="String" />
                                <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                                <asp:Parameter Name="user_email" Type="String" />
                                <asp:Parameter Name="ativo" Type="Boolean" />
                                <asp:Parameter Name="id" Type="Int32" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="dsProjeto" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [nome] FROM [projeto] ORDER BY [nome]"></asp:SqlDataSource>
                        <asp:SqlDataSource ID="dsRegional" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [regional] FROM [regional] ORDER BY [regional]"></asp:SqlDataSource>
                        <asp:SqlDataSource ID="dsFiltroProcesso" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [nome_filtro] FROM [filtro_regra] WHERE ([tipo] = @tipo) ORDER BY [nome_filtro]">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="qual_verif_monitora_processo" Name="tipo" Type="String" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <dx:ASPxGridViewExporter ID="gvExporterNc" runat="server" ExportedRowType="All" FileName="NC_Interna" GridViewID="gv_NC" PaperKind="A4">
                        </dx:ASPxGridViewExporter>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="Regras">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table class="dxflInternalEditorTable_Moderno">
                            <tr>
                                <td>
                                    <dx:ASPxGridView ID="gvRegraAcesso" runat="server" AutoGenerateColumns="False" DataSourceID="dsVerifMonitoraAcesso" EnableTheming="True" KeyFieldName="id" Theme="Metropolis" Width="600px">
                                        <SettingsEditing Mode="PopupEditForm">
                                        </SettingsEditing>
                                        <Settings ShowTitlePanel="True" />
                                        <SettingsSearchPanel Visible="True" />
                                        <SettingsText Title="Regras de acesso" />
                                        <Columns>
                                            <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                            </dx:GridViewCommandColumn>
                                            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="1" Visible="False">
                                                <EditFormSettings Visible="False" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataComboBoxColumn FieldName="id_user" ShowInCustomizationForm="True" VisibleIndex="2" Caption="Nome">
                                                <PropertiesComboBox DataSourceID="dsUser" TextField="nome" ValueField="id">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataComboBoxColumn FieldName="nome_filtro" ShowInCustomizationForm="True" VisibleIndex="3" Caption="Tipo de pemissão">
                                                <PropertiesComboBox DataSourceID="dsFiltroRegra" TextField="descricao" ValueField="nome_filtro">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                        </Columns>
                                    </dx:ASPxGridView>
                                    <asp:SqlDataSource ID="dsVerifMonitoraAcesso" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" DeleteCommand="DELETE FROM [qual_verif_monitora_acesso] WHERE [id] = @id" InsertCommand="INSERT INTO [qual_verif_monitora_acesso] ([id_user], [nome_filtro]) VALUES (@id_user, @nome_filtro)" SelectCommand="SELECT [id], [id_user], [nome_filtro] FROM [qual_verif_monitora_acesso]" UpdateCommand="UPDATE [qual_verif_monitora_acesso] SET [id_user] = @id_user, [nome_filtro] = @nome_filtro WHERE [id] = @id">
                                        <DeleteParameters>
                                            <asp:Parameter Name="id" Type="Int32" />
                                        </DeleteParameters>
                                        <InsertParameters>
                                            <asp:Parameter Name="id_user" Type="Int32" />
                                            <asp:Parameter Name="nome_filtro" Type="String" />
                                        </InsertParameters>
                                        <UpdateParameters>
                                            <asp:Parameter Name="id_user" Type="Int32" />
                                            <asp:Parameter Name="nome_filtro" Type="String" />
                                            <asp:Parameter Name="id" Type="Int32" />
                                        </UpdateParameters>
                                    </asp:SqlDataSource>
                                    <asp:SqlDataSource ID="dsUser" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [user].id, funcionario.nome 
FROM funcionario, grupo, grupo_user, [user]
WHERE	grupo.nome = 'Qualidade' AND 
		grupo_user.id_grupo = grupo.id AND
	 	grupo_user.id_user = [user].id AND
		funcionario.id = [user].idFuncionario"></asp:SqlDataSource>
                                    <asp:SqlDataSource ID="dsFiltroRegra" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [descricao], [nome_filtro] FROM [filtro_regra] WHERE ([tipo] = @tipo)">
                                        <SelectParameters>
                                            <asp:Parameter DefaultValue="qual_verif_monitora_regra_nc" Name="tipo" Type="String" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxGridView ID="gvProcesso" runat="server" AutoGenerateColumns="False" DataSourceID="dsProcesso" EnableTheming="True" KeyFieldName="id" Theme="Metropolis" OnRowInserting="gvProcesso_RowInserting" Width="600px">
                                        <SettingsEditing Mode="PopupEditForm">
                                        </SettingsEditing>
                                        <Settings ShowTitlePanel="True" />
                                        <SettingsSearchPanel Visible="True" />
                                        <SettingsText Title="Nome dos processos" />
                                        <Columns>
                                            <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                            </dx:GridViewCommandColumn>
                                            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="1" Visible="False">
                                                <EditFormSettings Visible="False" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="tipo" ShowInCustomizationForm="True" VisibleIndex="2" Visible="False">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="nome_filtro" ShowInCustomizationForm="True" VisibleIndex="3" Caption="Tipos de processo">
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:ASPxGridView>
                                    <asp:SqlDataSource ID="dsProcesso" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" DeleteCommand="DELETE FROM [filtro_regra] WHERE [id] = @id" InsertCommand="INSERT INTO [filtro_regra] ([nome_filtro], [tipo]) VALUES (@nome_filtro, @tipo)" SelectCommand="SELECT [id], [nome_filtro], [tipo] FROM [filtro_regra] WHERE ([tipo] = @tipo) ORDER BY [nome_filtro]" UpdateCommand="UPDATE [filtro_regra] SET [nome_filtro] = @nome_filtro, [tipo] = @tipo WHERE [id] = @id">
                                        <DeleteParameters>
                                            <asp:Parameter Name="id" Type="Int32" />
                                        </DeleteParameters>
                                        <InsertParameters>
                                            <asp:Parameter Name="nome_filtro" Type="String" />
                                            <asp:Parameter Name="tipo" Type="String" />
                                        </InsertParameters>
                                        <SelectParameters>
                                            <asp:Parameter DefaultValue="qual_verif_monitora_processo" Name="tipo" Type="String" />
                                        </SelectParameters>
                                        <UpdateParameters>
                                            <asp:Parameter Name="nome_filtro" Type="String" />
                                            <asp:Parameter Name="tipo" Type="String" />
                                            <asp:Parameter Name="id" Type="Int32" />
                                        </UpdateParameters>
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="modal-sm" style="width: 600px">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label2" runat="server" Text="Qtd. de dias úteis para responder a NC"></asp:Label>
                                            </td>
                                            <td>
                                                <dx:ASPxSpinEdit ID="spinEditPrazo" runat="server" EnableTheming="True" MaxValue="30" MinValue="1" Number="0" NumberType="Integer" Theme="Metropolis">
                                                </dx:ASPxSpinEdit>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnGravarDias" runat="server" OnClick="btnGravarDias_Click" Text="Gravar dias" Theme="Metropolis">
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:SqlDataSource ID="dsDias" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" DeleteCommand="DELETE FROM [filtro_regra] WHERE [id] = @id" InsertCommand="INSERT INTO [filtro_regra] ([tipo], [nome_filtro]) VALUES (@tipo, @nome_filtro)" SelectCommand="SELECT [id], [tipo], [nome_filtro] FROM [filtro_regra] WHERE ([tipo] = @tipo)" UpdateCommand="UPDATE filtro_regra SET tipo = @tipo, nome_filtro = @nome_filtro WHERE (id = @id) AND (tipo = @tipo) AND (nome_filtro = @nome_filtro)">
                                        <DeleteParameters>
                                            <asp:Parameter Name="id" Type="Int32" />
                                        </DeleteParameters>
                                        <InsertParameters>
                                            <asp:Parameter Name="tipo" Type="String" />
                                            <asp:Parameter Name="nome_filtro" Type="String" />
                                        </InsertParameters>
                                        <SelectParameters>
                                            <asp:Parameter DefaultValue="qual_verif_monitora_limite_prazo" Name="tipo" Type="String" />
                                        </SelectParameters>
                                        <UpdateParameters>
                                            <asp:Parameter Name="tipo" Type="String" DefaultValue="qual_verif_monitora_limite_prazo" />
                                            <asp:ControlParameter ControlID="spinEditPrazo" Name="nome_filtro" PropertyName="Number" Type="String" />
                                            <asp:Parameter Name="id" Type="Int32" />
                                        </UpdateParameters>
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
    </dx:ASPxPageControl>
</asp:Content>
