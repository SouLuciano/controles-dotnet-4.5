﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Globalization;

namespace webequilibrio.app.classe
{
    public class auxiliar
    {
        public string redusString(string stringAtual, int maxString)
        {
            if (stringAtual.Length > maxString)
            {
                string novaString = stringAtual.Substring(0, maxString);
                return novaString;
            }
            else
            {
                return stringAtual;
            }
        }

        public string somenteNumeros(string stringAtual)
        {
            string resultString = string.Empty;
            Regex regexObj = new Regex(@"[^\d]");
            resultString = regexObj.Replace(stringAtual, "");
            return resultString;
        }

        public bool ehEmail(string email)
        {
            bool isEmail = Regex.IsMatch(email, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.IgnoreCase);
            return isEmail;
        }

        public static bool ehNumero32(string valor)
        {
            bool resposta = true;
            try
            {
                Int32 inteiro = Convert.ToInt32(valor);
            }
            catch
            {
                resposta = false;
            }
            return resposta;
        }

        public static bool ehNumero64(string valor)
        {
            bool resposta = true;
            try
            {
                Int64 inteiro = Convert.ToInt64(valor);
            }
            catch
            {
                resposta = false;
            }
            return resposta;
        }

        public static int GetWeekInMonth(DateTime date)
        {
            DateTime tempdate = date.AddDays(-date.Day + 1);
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNumStart = ciCurr.Calendar.GetWeekOfYear(tempdate, CalendarWeekRule.FirstFourDayWeek, ciCurr.DateTimeFormat.FirstDayOfWeek);
            int weekNum = ciCurr.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstFourDayWeek, ciCurr.DateTimeFormat.FirstDayOfWeek);
            return weekNum - weekNumStart + 1;
        }
    }
}