﻿using System;
using System.Data;
using System.Web;
using DevExpress.XtraPrinting;
using DevExpress.Export;
using DevExpress.Web;
using System.Collections.Generic;


namespace webequilibrio.app.cadastro
{
    public partial class mobile : System.Web.UI.Page
    {

        string url;
        string userId;
        app.classe.bdados dados = new classe.bdados();
        app.classe.celular celulares = new classe.celular();
        string email;
        protected void Page_Load(object sender, EventArgs e)
        {
            userId = Session["_userId"].ToString();
            url = HttpContext.Current.Request.Url.AbsolutePath;
            bool acesso = dados.temAcesso(Convert.ToInt32(userId), "~" + url);

            if (!acesso)
            {
                Response.Redirect("~/default.aspx", true);
            }

            using (DataTable dtUser = (DataTable)Session["_user"])
            {
                email = dtUser.Rows[0]["email"].ToString();
            }

            //pcMobile.ActiveTabIndex = 0;
        }

        #region CELULAR

        protected void gvCelular_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            #region classe Celulares
            celulares.id_celular = Convert.ToInt32(e.Keys["id"]);
            celulares.id_projeto = Convert.ToInt32(e.OldValues["id_projeto"]);
            celulares.modelo = (e.OldValues["modelo"].ToString().Trim() == "") ? null : e.OldValues["modelo"].ToString();
            celulares.imei1 = (e.OldValues["imei1"].ToString().Trim() == "") ? null : e.OldValues["imei1"].ToString();
            celulares.imei2 = (e.OldValues["imei2"].ToString().Trim() == "") ? null : e.OldValues["imei2"].ToString();
            celulares.ativo = Convert.ToBoolean(e.OldValues["ativo"]);

            celulares.observacao = (e.OldValues["observacao"].ToString().Trim() == "") ? null : e.OldValues["observacao"].ToString();

            if (e.NewValues["observacao"] != null && e.NewValues["observacao"].ToString().Contains("(mensagem do sistema)"))
                e.NewValues["observacao"] = null;


            if (e.OldValues["id_simcard1"].ToString().Trim() != "") celulares.id_simcard1 = Convert.ToInt32(e.OldValues["id_simcard1"]);
            if (e.OldValues["id_simcard2"].ToString().Trim() != "") celulares.id_simcard2 = Convert.ToInt32(e.OldValues["id_simcard2"]);
            celulares.sistema_op = (e.OldValues["sistema_op"].ToString().Trim() == "") ? null : e.OldValues["sistema_op"].ToString();
            celulares.senha = (e.OldValues["senha"].ToString().Trim() == "") ? null : e.OldValues["senha"].ToString();
            celulares.status = (e.OldValues["status"].ToString().Trim() == "") ? null : e.OldValues["status"].ToString();
            celulares.dt_criacao = DateTime.Now;
            e.NewValues["dt_atualizacao"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            if (e.OldValues["id_equipe"].ToString().Trim() != "") celulares.id_equipe = Convert.ToInt32(e.OldValues["id_equipe"]);
            celulares.user_email = (e.OldValues["user_email"].ToString().Trim() == "") ? null : e.OldValues["user_email"].ToString();
            celulares.funcao = (e.OldValues["funcao"].ToString().Trim() == "") ? null : e.OldValues["funcao"].ToString();
            if (e.OldValues["id_regional"].ToString().Trim() != "") celulares.id_regional = Convert.ToInt32(e.OldValues["id_regional"]);
            #endregion

            e.NewValues["user_email"] = email;
            e.NewValues["dt_atualizacao"] = DateTime.Now;

            #region ATUALIZAÇÕES DA REGIONAL E STATUS

            if (e.NewValues["status"].ToString().Contains("Reserva") || e.NewValues["status"].ToString() == "Manutenção")
            {
                e.NewValues["id_equipe"] = null;

                if (e.NewValues["status"].ToString().Contains("Reserva"))
                {
                    int idRegional = dados.getIdRegionalPeloSatatus(e.NewValues["status"].ToString(), userId);
                    if (idRegional != 0)
                    {
                        e.NewValues["id_regional"] = idRegional;
                    }
                }
            }

            if (e.NewValues["id_equipe"] != null)
            {
                int id_equipe = Convert.ToInt32(e.NewValues["id_equipe"]);

                using (DataTable regional = dados.getRegional(id_equipe, userId))
                {
                    e.NewValues["id_regional"] = regional.Rows[0]["id"].ToString();
                    e.NewValues["status"] = "Em uso";
                }
            }

            #endregion

        }

        protected void gvCelular_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
        {
            using (data.dsCadastrosTableAdapters.celular_histTableAdapter ta = new data.dsCadastrosTableAdapters.celular_histTableAdapter())
            {
                ta.Insert(
                    celulares.id_celular,
                    celulares.id_projeto,
                    celulares.modelo,
                    celulares.imei1,
                    celulares.imei2,
                    celulares.ativo,
                    celulares.observacao,
                    celulares.id_simcard1,
                    celulares.id_simcard2,
                    celulares.sistema_op,
                    celulares.senha,
                    celulares.status,
                    celulares.dt_criacao,
                    celulares.user_email,
                    celulares.id_equipe,
                    celulares.funcao,
                    celulares.id_regional);
            }

            dados.setLog(Convert.ToInt32(userId), "inf", "atualizou", "celular", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }

        protected void gvCelularHist_BeforePerformDataSelect(object sender, EventArgs e)
        {
            Session["id_celular"] = (sender as DevExpress.Web.ASPxGridView).GetMasterRowKeyValue();
        }
        
        protected void gvCelular_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            e.NewValues["user_email"] = email;

            if (e.NewValues["id_equipe"] != null)
            {
                int id_equipe = Convert.ToInt32(e.NewValues["id_equipe"]);

                using (DataTable regional = dados.getRegional(id_equipe, userId))
                {
                    e.NewValues["id_regional"] = regional.Rows[0]["id"];
                }
            }

            e.NewValues["dt_criacao"] = DateTime.Now;
            e.NewValues["dt_atualizacao"] = DateTime.Now;

            dados.setLog(Convert.ToInt32(userId), "inf", "criou", "celular", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }

        protected void gvCelular_CustomErrorText(object sender, DevExpress.Web.ASPxGridViewCustomErrorTextEventArgs e)
        {
            if (e.ErrorText.Contains("duplicate"))
            {
                if (e.ErrorText.Contains("IX_celular_imei1") || e.ErrorText.Contains("UNIQUE KEY"))
                    e.ErrorText = "Este imei já está cadastrado";

                if (e.ErrorText.Contains("id_simcard"))
                    e.ErrorText = "Este SimCard já está associado a outro imei";

                if (e.ErrorText == "Este imei já está cadastrado")
                    e.ErrorText.ToString();

                if (e.ErrorText == "Este SimCard (chip) está associado a outro imei")
                    e.ErrorText.ToString();
            }

            if (e.ErrorText.Contains("NULL"))
            {
                if (e.ErrorText.Contains("ativo"))
                    e.ErrorText = "Marque a opção Ativo";
            }
        }

        protected void gvCelular_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            foreach (GridViewColumn column in gvCelular.Columns)
            {
                GridViewDataColumn dataColumn = column as GridViewDataColumn;
                if (dataColumn == null) continue;

                if (e.NewValues["imei1"] == null)
                {
                    AddError(e.Errors, gvCelular.Columns["imei1"], "Não pode ser em branco");
                }

                if (e.NewValues["id_projeto"] == null)
                {
                    AddError(e.Errors, gvCelular.Columns["id_projeto"], "Não pode ser em branco");
                }

                if (e.NewValues["ativo"] == null)
                {
                    AddError(e.Errors, gvCelular.Columns["ativo"], "Ativo não pode ser em branco");
                }


                #region CHECK REPETIDO SIMCARD1
                if (dataColumn.FieldName == "id_simcard1")
                    if (e.NewValues["id_simcard1"] != null)
                    {
                        if (e.Keys["id"] != null)
                        {
                            if (dados.checkDadoRepetido("id_simcard1", e.NewValues["id_simcard1"].ToString(), "celular", userId, e.Keys["id"].ToString()))
                                AddError(e.Errors, gvCelular.Columns["id_simcard1"], "Este SimCard (chip) está associado a outro imei");

                            if (dados.checkDadoRepetido("id_simcard2", e.NewValues["id_simcard1"].ToString(), "celular", userId, e.Keys["id"].ToString()))
                                AddError(e.Errors, gvCelular.Columns["id_simcard1"], "Este SimCard (chip) está associado a outro imei");
                        }
                        else
                        {
                            if (dados.checkDadoRepetido("id_simcard1", e.NewValues["id_simcard1"].ToString(), "celular", userId, ""))
                                AddError(e.Errors, gvCelular.Columns["id_simcard1"], "Este SimCard (chip) está associado a outro imei");

                            if (dados.checkDadoRepetido("id_simcard2", e.NewValues["id_simcard1"].ToString(), "celular", userId, ""))
                                AddError(e.Errors, gvCelular.Columns["id_simcard1"], "Este SimCard (chip) está associado a outro imei");
                        }
                    }
                #endregion

                #region CHECK REPETIDO SIMCARD2
                if (dataColumn.FieldName == "id_simcard2")
                    if (e.NewValues["id_simcard2"] != null)
                    {
                        if (e.Keys["id"] != null)
                        {
                            if (dados.checkDadoRepetido("id_simcard1", e.NewValues["id_simcard2"].ToString(), "celular", userId, e.Keys["id"].ToString()))
                                AddError(e.Errors, gvCelular.Columns["id_simcard2"], "Este SimCard (chip) está associado a outro imei");

                            if (dados.checkDadoRepetido("id_simcard2", e.NewValues["id_simcard2"].ToString(), "celular", userId, e.Keys["id"].ToString()))
                                AddError(e.Errors, gvCelular.Columns["id_simcard2"], "Este SimCard (chip) está associado a outro imei");
                        }
                        else
                        {
                            if (dados.checkDadoRepetido("id_simcard1", e.NewValues["id_simcard2"].ToString(), "celular", userId, ""))
                                AddError(e.Errors, gvCelular.Columns["id_simcard2"], "Este SimCard (chip) está associado a outro imei");

                            if (dados.checkDadoRepetido("id_simcard2", e.NewValues["id_simcard2"].ToString(), "celular", userId, ""))
                                AddError(e.Errors, gvCelular.Columns["id_simcard2"], "Este SimCard (chip) está associado a outro imei");
                        }
                    }
                #endregion
                if (dataColumn.FieldName == "imei1")
                    if (e.NewValues["imei1"] != null)
                    {
                        if (e.Keys["id"] != null)
                        {
                            if (dados.checkDadoRepetido("imei1", e.NewValues["imei1"].ToString(), "celular", userId, e.Keys["id"].ToString()))
                                AddError(e.Errors, gvCelular.Columns["imei1"], "Este Imei já está cadastrado");

                            if (dados.checkDadoRepetido("imei2", e.NewValues["imei1"].ToString(), "celular", userId, e.Keys["id"].ToString()))
                                AddError(e.Errors, gvCelular.Columns["imei2"], "Este Imei já está cadastrado");
                        }
                        else
                        {
                            if (dados.checkDadoRepetido("imei1", e.NewValues["imei1"].ToString(), "celular", userId, ""))
                                AddError(e.Errors, gvCelular.Columns["imei1"], "Este Imei já está cadastrado");

                            if (dados.checkDadoRepetido("imei2", e.NewValues["imei1"].ToString(), "celular", userId, ""))
                                AddError(e.Errors, gvCelular.Columns["imei1"], "Este Imei já está cadastrado");
                        }
                    }

                if (dataColumn.FieldName == "imei2")
                    if (e.NewValues["imei2"] != null)
                    {
                        if (e.Keys["id"] != null)
                        {
                            if (dados.checkDadoRepetido("imei1", e.NewValues["imei2"].ToString(), "celular", userId, e.Keys["id"].ToString()))
                                AddError(e.Errors, gvCelular.Columns["imei2"], "Este Imei já está cadastrado");

                            if (dados.checkDadoRepetido("imei2", e.NewValues["imei2"].ToString(), "celular", userId, e.Keys["id"].ToString()))
                                AddError(e.Errors, gvCelular.Columns["imei2"], "Este Imei já está cadastrado");
                        }
                        else
                        {
                            if (dados.checkDadoRepetido("imei1", e.NewValues["imei2"].ToString(), "celular", userId, ""))
                                AddError(e.Errors, gvCelular.Columns["imei2"], "Este Imei já está cadastrado");

                            if (dados.checkDadoRepetido("imei2", e.NewValues["imei2"].ToString(), "celular", userId, ""))
                                AddError(e.Errors, gvCelular.Columns["imei2"], "Este Imei já está cadastrado");
                        }
                    }

            }




            if (string.IsNullOrEmpty(e.RowError) && e.Errors.Count > 0) e.RowError = "Corriga todos os erros.";
        }
        

        protected void dsEquipe_Selecting(object sender, System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs e)
        {
            // Seleciona os carros dos projetos que o usuario tem acesso
            //userId = Session["_userId"].ToString();
            //string cmd = string.Format(" SELECT equipe.id, equipe.codigo " +
            //    "FROM equipe, projeto_user, USER " +
            //    $"WHERE equipe.id_projeto = projeto_user.id_projeto AND projeto_user.id_user = {userId}");

            string cmd = string.Format(" SELECT equipe.id, equipe.codigo " +
                "FROM equipe ORDER BY equipe.codigo");

            e.Command.CommandText = cmd;
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {

            dados.setLog(Convert.ToInt32(userId), "inf", "exportou", "celular", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            gvExporter.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }

        #endregion
       

        void AddError(Dictionary<GridViewColumn, string> errors, GridViewColumn column, string errorText)
        {
            if (errors.ContainsKey(column)) return;
            errors[column] = errorText;
        }

        #region SIM CARD
        
        protected void gvSimcard_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "atualizou", "simcard", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }

        protected void gvSimcard_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "criou", "simcard", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }

        protected void brnExportExcelSimcard_Click(object sender, EventArgs e)
        {

            dados.setLog(Convert.ToInt32(userId), "inf", "exportou", "simcard", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            gvExporterSimcard.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }

        #endregion

        #region MODELO

        protected void gvModeloCelular_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
        {

        }

        protected void gvModeloCelular_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            e.NewValues["tipo"] = "celular";
        }

        protected void gvModeloCelular_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
        {

        }

        protected void gvModeloCelular_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            Session["_newNomeCelular"] = e.NewValues["nome"].ToString();
            Session["_oldNomeCelular"] = e.OldValues["nome"].ToString();
        }

        protected void dsTipoCelular_Updating(object sender, System.Web.UI.WebControls.SqlDataSourceCommandEventArgs e)
        {
            string sql = $"UPDATE tipo_equipamento SET nome='{Session["_newNomeCelular"].ToString()}' WHERE nome='{Session["_oldNomeCelular"].ToString()}' AND tipo='celular'";
            e.Command.CommandText = sql;
        }

        #endregion

        protected void gvFuncaoCelular_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
        {

        }

        protected void gvFuncaoCelular_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            e.NewValues["tipo"] = "funcao_celular";
        }

        protected void gvFuncaoCelular_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
        {

        }

        protected void gvFuncaoCelular_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            Session["_newFuncaoCelular"] = e.NewValues["nome"].ToString();
            Session["_oldFuncaoCelular"] = e.OldValues["nome"].ToString();
        }

        protected void dsFuncaoCelular_Updating(object sender, System.Web.UI.WebControls.SqlDataSourceCommandEventArgs e)
        {
            string sql = $"UPDATE tipo_equipamento SET nome='{Session["_newFuncaoCelular"].ToString()}' WHERE nome='{Session["_oldFuncaoCelular"].ToString()}' AND tipo='funcao_celular'";
            e.Command.CommandText = sql;
        }
    }
}