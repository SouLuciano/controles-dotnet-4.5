﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="rastreador.aspx.cs" Inherits="webequilibrio.app.cadastro.rastreador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="100%" HeaderText="Controle de rastreadores" Theme="Metropolis">
        <PanelCollection>
            <dx:PanelContent runat="server">
                <table class="nav-justified">
                    <tr>
                        <td>
                            <table class="nav-justified" style="width: 750px; margin-bottom: 5px;">
                                <tr>
                                    <td>
                                        <dx:ASPxComboBox ID="cbProjeto" runat="server" AutoPostBack="True" DataSourceID="dsProjetoCbx" TextField="nome" ValueField="id" ValueType="System.Int32" Width="300px" NullText="Projeto" Theme="Metropolis">
                                            <Columns>
                                                <dx:ListBoxColumn Caption="Nome" FieldName="nome" />
                                                <dx:ListBoxColumn Caption="Descrição" FieldName="descricao" />
                                            </Columns>
                                        </dx:ASPxComboBox>
                                        <asp:SqlDataSource ID="dsProjetoCbx" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" SelectCommand="SELECT id, nome, descricao, ativo FROM projeto ORDER BY nome, descricao"></asp:SqlDataSource>
                                    </td>
                                    <td>
                                        <dx:ASPxComboBox ID="cbEquipe" runat="server"  AutoPostBack="True" DataSourceID="dsEquipe" TextField="codigo" ValueField="id" ValueType="System.Int32" Width="300px" NullText="Equipe" Theme="Metropolis" Visible="False">
                                            <Columns>
                                                <dx:ListBoxColumn Caption="Código" FieldName="codigo" />
                                                <dx:ListBoxColumn Caption="Região" FieldName="regiao" />
                                            </Columns>
                                        </dx:ASPxComboBox>
                                        <asp:SqlDataSource ID="dsEquipe" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" SelectCommand="SELECT id, id_projeto, codigo, regiao, ativo FROM equipe WHERE ((id_projeto = @id_projeto) AND (ativo = @ativo)) ORDER BY codigo">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="cbProjeto" Name="id_projeto" PropertyName="Value" Type="Int32" />
                                                <asp:Parameter DefaultValue="1" Name="ativo" Type="Int16" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btnExportar" runat="server" OnClick="btnExportar_Click" Text="Exportar Excel" Theme="Metropolis">
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvRastreador" runat="server" DataSourceID="dsRastreador" AutoGenerateColumns="False" KeyFieldName="id" OnRowUpdating="gvRastreador_RowUpdating" OnRowUpdated="gvRastreador_RowUpdated" Theme="Metropolis" OnRowInserting="gvRastreador_RowInserting" OnCustomErrorText="gvRastreador_CustomErrorText">
                                <SettingsDetail AllowOnlyOneMasterRowExpanded="True" ShowDetailRow="True" />
                                <Templates>
                                    <DetailRow>
                                        <dx:ASPxGridView ID="gvRastreadorHistorico" runat="server" DataSourceID="dsRastreadorHistorico" AutoGenerateColumns="False" KeyFieldName="id_rastreador" OnBeforePerformDataSelect="gvRastreadorHistorico_BeforePerformDataSelect" Width="100%" Theme="Metropolis">
                                            <Settings ShowFilterRow="True" ShowTitlePanel="True" />
                                            <SettingsText Title="Histórico de alterações" />
                                            <Columns>
                                                <dx:GridViewCommandColumn ShowClearFilterButton="True" VisibleIndex="0">
                                                </dx:GridViewCommandColumn>
                                                <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="1" Visible="False">
                                                    <EditFormSettings Visible="False" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="id_rastreador" VisibleIndex="2" Visible="False">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="status" VisibleIndex="6" Caption="Status">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="observacao" VisibleIndex="9" Caption="Observação">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataDateColumn FieldName="dt_criacao" VisibleIndex="11" Caption="Atualizado em">
                                                    <PropertiesDateEdit DisplayFormatString="g">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn FieldName="user_email" VisibleIndex="10" Caption="Ultima alteração">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataComboBoxColumn Caption="Celular imei 1" FieldName="id_celular" VisibleIndex="5">
                                                    <PropertiesComboBox DataSourceID="dsCelular" TextField="imei1" ValueField="id">
                                                    </PropertiesComboBox>
                                                </dx:GridViewDataComboBoxColumn>
                                                <dx:GridViewDataComboBoxColumn Caption="Veículo" FieldName="id_veiculo" VisibleIndex="4">
                                                    <PropertiesComboBox DataSourceID="dsVeiculo" TextField="placa" ValueField="id">
                                                    </PropertiesComboBox>
                                                </dx:GridViewDataComboBoxColumn>
                                                <dx:GridViewDataComboBoxColumn Caption="Equipe" FieldName="id_equipe" VisibleIndex="3">
                                                    <PropertiesComboBox DataSourceID="dsEquipe" TextField="codigo" ValueField="id">
                                                    </PropertiesComboBox>
                                                </dx:GridViewDataComboBoxColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                        <asp:SqlDataSource ID="dsRastreadorHistorico" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                                            ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" 
                                            SelectCommand="SELECT id, id_rastreador, id_celular, id_veiculo, status, observacao, dt_criacao, user_email, id_equipe FROM rastreador_historico WHERE (id_rastreador = @id_rastreador) ORDER BY dt_criacao DESC">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="id_rastreador" SessionField="id_rastreador" Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </DetailRow>
                                </Templates>
                                <Settings ShowFilterRow="True" />
                                <SettingsBehavior AutoFilterRowInputDelay="1800" />
                                <Columns>
                                    <dx:GridViewCommandColumn ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0" ShowClearFilterButton="True">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="1" Visible="False">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="id_projeto" ShowInCustomizationForm="True" VisibleIndex="2" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn FieldName="dt_criacao" ShowInCustomizationForm="True" VisibleIndex="10" Visible="False">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataDateColumn FieldName="dt_atualizacao" ShowInCustomizationForm="True" VisibleIndex="12" Caption="Atualizado em">
                                        <PropertiesDateEdit DisplayFormatString="g">
                                        </PropertiesDateEdit>
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataComboBoxColumn FieldName="id_celular" ShowInCustomizationForm="True" VisibleIndex="3" Caption="Imei 1">
                                        <PropertiesComboBox DataSourceID="dsCelular" TextField="imei1" ValueField="id">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn FieldName="id_veiculo" ShowInCustomizationForm="True" VisibleIndex="5" Caption="Veículo">
                                        <PropertiesComboBox DataSourceID="dsVeiculo" TextField="placa" ValueField="id" AllowNull="True">
                                            <Columns>
                                                <dx:ListBoxColumn Caption="Placa" FieldName="placa" />
                                                <dx:ListBoxColumn Caption="Modelo" FieldName="modelo" />
                                            </Columns>
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataMemoColumn FieldName="observacao" ShowInCustomizationForm="True" VisibleIndex="9" Caption="Observação">
                                    </dx:GridViewDataMemoColumn>
                                    <dx:GridViewDataTextColumn Caption="Ultima alteração" FieldName="user_email" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="11">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Status" FieldName="status" ShowInCustomizationForm="True" UnboundType="String" VisibleIndex="6">
                                        <PropertiesComboBox>
                                            <Items>
                                                <dx:ListEditItem Text="Usando" Value="Usando" />
                                                <dx:ListEditItem Text="Reserva" Value="Reserva" />
                                            </Items>
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Equipe" FieldName="id_equipe" ShowInCustomizationForm="True" VisibleIndex="4">
                                        <PropertiesComboBox DataSourceID="dsEquipe" TextField="codigo" ValueField="id">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                </Columns>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="dsRastreador" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                    DeleteCommand="DELETE FROM rastreador WHERE id = @id" 
InsertCommand="INSERT INTO rastreador (id, id_projeto, id_celular, id_veiculo, status, observacao, dt_criacao, dt_atualizacao, user_email, id_equipe) VALUES (@id, @id_projeto, @id_celular, @id_veiculo, @status, @observacao, @dt_criacao, @dt_atualizacao, @user_email, @id_equipe)" 
ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" 
SelectCommand="SELECT id, id_projeto, id_celular, id_veiculo, status, observacao, dt_criacao, dt_atualizacao , user_email, id_equipe FROM rastreador  WHERE (id_projeto = @id_projeto)" 
UpdateCommand="UPDATE rastreador SET id_projeto = @id_projeto, id_celular = @id_celular, id_veiculo = @id_veiculo, status = @status, observacao = @observacao, dt_criacao = @dt_criacao, dt_atualizacao= @dt_atualizacao , user_email = @user_email, id_equipe = @id_equipe WHERE id = @id">
                                <DeleteParameters>
                                    <asp:Parameter Name="id" Type="Int32" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="id" Type="Int32" />
                                    <asp:Parameter Name="id_projeto" Type="Int32" />
                                    <asp:Parameter Name="id_celular" Type="Int32" />
                                    <asp:Parameter Name="id_veiculo" Type="Int32" />
                                    <asp:Parameter Name="id_equipe" Type="Int32" />
                                    <asp:Parameter Name="status" Type="String" />
                                    <asp:Parameter Name="observacao" Type="String" />
                                    <asp:Parameter Name="dt_criacao" Type="DateTime" />
                                    <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                                    <asp:Parameter Name="user_email" Type="String" />
                                </InsertParameters>
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="cbProjeto" Name="id_projeto" PropertyName="Value" Type="Int32" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="id_projeto" Type="Int32" />
                                    <asp:Parameter Name="id_celular" Type="Int32" />
                                    <asp:Parameter Name="id_veiculo" Type="Int32" />
                                    <asp:Parameter Name="id_equipe" Type="Int32" />
                                    <asp:Parameter Name="status" Type="String" />
                                    <asp:Parameter Name="observacao" Type="String" />
                                    <asp:Parameter Name="dt_criacao" Type="DateTime" />
                                    <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                                    <asp:Parameter Name="user_email" Type="String" />
                                    <asp:Parameter Name="id" Type="Int32" />
                                </UpdateParameters>
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="dsCelular" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                                ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" 
                                SelectCommand="SELECT id, id_projeto, modelo, imei1 FROM celular WHERE (id_projeto = @id_projeto) ORDER BY modelo, imei1">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="cbProjeto" Name="id_projeto" PropertyName="Value" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="dsVeiculo" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                                ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" 
                                SelectCommand="SELECT id, id_projeto, placa, modelo, ativo FROM veiculo WHERE (id_projeto = @id_projeto) ORDER BY placa, modelo">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="cbProjeto" Name="id_projeto" PropertyName="Value" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="dsUsuario" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" SelectCommand="SELECT id, ativo, nome, email FROM user"></asp:SqlDataSource>
                            <dx:ASPxGridViewExporter ID="gvExporter" runat="server" ExportedRowType="All" FileName="Controle de rastreador" GridViewID="gvRastreador" PaperKind="A4">
                            </dx:ASPxGridViewExporter>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>
</asp:Content>
