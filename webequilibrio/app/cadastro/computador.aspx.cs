﻿using System;
using System.Web;
using System.Data;
using DevExpress.Export;
using DevExpress.Web;
using DevExpress.XtraPrinting;
using System.Collections.Generic;

namespace webequilibrio.app.cadastro
{
    public partial class computador : System.Web.UI.Page
    {
        app.classe.bdados dados = new app.classe.bdados();
        string url;
        string userId;
        string email;
        app.classe.computador computadores = new classe.computador();

        protected void Page_Load(object sender, EventArgs e)
        {
            userId = Session["_userId"].ToString();
            url = HttpContext.Current.Request.Url.AbsolutePath;
            bool acesso = dados.temAcesso(Convert.ToInt32(userId), "~" + url);

            if (!acesso)
            {
                Response.Redirect("~/default.aspx", true);
            }

            using (DataTable dtUser = (DataTable)Session["_user"])
            {
                email = dtUser.Rows[0]["email"].ToString();
            }
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "exportou", "computador", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            gvExporter.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }

        protected void gvComputador_CustomErrorText(object sender, DevExpress.Web.ASPxGridViewCustomErrorTextEventArgs e)
        {

        }

        protected void gvComputador_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "criou", "computador", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);

        }

        protected void gvComputador_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            e.NewValues["dt_atualizacao"] = DateTime.Now;
            e.NewValues["user_email"] = email;

            if (e.NewValues["id_equipe"] != null)
            {
                int id_equipe = Convert.ToInt32(e.NewValues["id_equipe"]);

                using (DataTable regional = dados.getRegional(id_equipe, userId))
                {
                    e.NewValues["id_regional"] = regional.Rows[0]["id"];
                }
            }
           
        }

        protected void gvComputador_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
        {
            try
            {
                using (data.dsCadastrosTableAdapters.computadorTableAdapter ta = new data.dsCadastrosTableAdapters.computadorTableAdapter())
                {
                    ta.Insert(
                        computadores.id_projeto,
                        computadores.id_regional,
                        computadores.id_equipe,
                        computadores.id_funcionario,
                        computadores.id_pai,
                        computadores.nome,
                        computadores.teamviewer,
                        computadores.tipo,
                        computadores.marca,
                        computadores.modelo,
                        computadores.etiqueta,
                        computadores.os,
                        computadores.licenca_win,
                        computadores.office,
                        computadores.licenca_off,
                        computadores.ip,
                        computadores.cpu,
                        computadores.ram,
                        computadores.ram_modelo,
                        computadores.hd,
                        computadores.mac,
                        computadores.status,
                        computadores.observacao,
                        computadores.ativo,
                        computadores.dt_atualizacao,
                        computadores.user_email
                        );
                }
                dados.setLog(Convert.ToInt32(userId), "inf", "atualizou", "computador", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            }
            catch (Exception ex)
            {
                dados.setLog(Convert.ToInt32(userId), "err", "gvComputador_RowUpdated()", ex.Message, url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            }

        }

        protected void gvComputador_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            try
            {
                #region PUPULAR CLASSE COMPUTADOR
                computadores.id_pai = Convert.ToInt32(e.Keys["id"]);
                computadores.id_projeto = Convert.ToInt32(e.OldValues["id_projeto"]);
                if (e.OldValues["id_regional"].ToString() != "") computadores.id_regional = Convert.ToInt32(e.OldValues["id_regional"]);
                if (e.OldValues["id_equipe"].ToString() != "") computadores.id_equipe = Convert.ToInt32(e.OldValues["id_equipe"]);
                if (e.OldValues["id_funcionario"].ToString() != "") computadores.id_funcionario = Convert.ToInt32(e.OldValues["id_funcionario"]);
                computadores.nome = (e.OldValues["nome"].ToString().Trim() == "") ? null : e.OldValues["nome"].ToString();
                computadores.teamviewer = (e.OldValues["teamviewer"].ToString().Trim() == "") ? null : e.OldValues["teamviewer"].ToString();
                computadores.tipo = (e.OldValues["tipo"].ToString().Trim() == "") ? null : e.OldValues["tipo"].ToString();
                computadores.marca = (e.OldValues["marca"].ToString().Trim() == "") ? null : e.OldValues["marca"].ToString();
                computadores.modelo = (e.OldValues["modelo"].ToString().Trim() == "") ? null : e.OldValues["modelo"].ToString();
                computadores.etiqueta = (e.OldValues["etiqueta"].ToString().Trim() == "") ? null : e.OldValues["etiqueta"].ToString();
                computadores.os = (e.OldValues["os"].ToString().Trim() == "") ? null : e.OldValues["os"].ToString();
                computadores.licenca_win = (e.OldValues["licenca_win"].ToString().Trim() == "") ? null : e.OldValues["licenca_win"].ToString();
                computadores.office = (e.OldValues["office"].ToString().Trim() == "") ? null : e.OldValues["office"].ToString();
                computadores.licenca_off = (e.OldValues["licenca_off"].ToString().Trim() == "") ? null : e.OldValues["licenca_off"].ToString();
                computadores.ip = (e.OldValues["ip"].ToString().Trim() == "") ? null : e.OldValues["ip"].ToString();
                computadores.cpu = (e.OldValues["cpu"].ToString().Trim() == "") ? null : e.OldValues["cpu"].ToString();
                computadores.ram = (e.OldValues["ram"].ToString().Trim() == "") ? null : e.OldValues["ram"].ToString();
                computadores.ram_modelo = (e.OldValues["ram_modelo"].ToString().Trim() == "") ? null : e.OldValues["ram_modelo"].ToString();
                computadores.hd = (e.OldValues["hd"].ToString().Trim() == "") ? null : e.OldValues["hd"].ToString();
                computadores.mac = (e.OldValues["mac"].ToString().Trim() == "") ? null : e.OldValues["mac"].ToString();
                computadores.status = (e.OldValues["status"].ToString().Trim() == "") ? null : e.OldValues["status"].ToString();
                computadores.observacao = (e.OldValues["observacao"].ToString().Trim() == "") ? null : e.OldValues["observacao"].ToString();
                computadores.ativo = Convert.ToBoolean(e.OldValues["ativo"]);
                computadores.dt_atualizacao = Convert.ToDateTime(e.OldValues["dt_atualizacao"]);
                computadores.user_email = e.OldValues["user_email"].ToString();
                #endregion

                e.NewValues["user_email"] = email;
                e.NewValues["dt_atualizacao"] = DateTime.Now;

                #region ATUALIZAÇÕES DA REGIONAL E STATUS

                if (e.NewValues["status"].ToString().Contains("Reserva") || e.NewValues["status"].ToString() == "Manutenção")
                {

                    e.NewValues["id_equipe"] = null;

                    if (e.NewValues["status"].ToString().Contains("Reserva"))
                    {
                        int idRegional = dados.getIdRegionalPeloSatatus(e.NewValues["status"].ToString(), userId);
                        if (idRegional != 0)
                        {
                            e.NewValues["id_regional"] = idRegional;
                        }
                    }
                }


                if (e.NewValues["id_equipe"] != null)
                {
                    int id_equipe = Convert.ToInt32(e.NewValues["id_equipe"]);

                    using (DataTable regional = dados.getRegional(id_equipe, userId))
                    {
                        e.NewValues["id_regional"] = regional.Rows[0]["id"].ToString();
                        e.NewValues["status"] = "Em uso";
                    }
                }

                #endregion


            }

            catch (Exception ex)
            {
                e.Cancel = true;
                dados.setLog(Convert.ToInt32(userId), "err", "gvComputador_RowUpdating()", ex.Message, url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            }
        }

        protected void gvComputador_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            foreach (GridViewColumn column in gvComputador.Columns)
            {
                GridViewDataColumn dataColumn = column as GridViewDataColumn;
                if (dataColumn == null) continue;

                if (e.NewValues["id_projeto"] == null)
                {
                    AddError(e.Errors, gvComputador.Columns["id_projeto"], "Não pode ser em branco");
                }
                if (e.NewValues["status"] == null)
                {
                    AddError(e.Errors, gvComputador.Columns["status"], "Não pode ser em branco");
                }
                if (e.NewValues["ativo"] == null)
                {
                    AddError(e.Errors, gvComputador.Columns["ativo"], "Ativo não pode ser em branco");
                }

                if (dataColumn.FieldName == "nome")
                    if (e.NewValues["nome"] != null)
                    {
                        if (e.Keys["id"] != null)
                        {
                            if (dados.checkDadoRepetido("nome", e.NewValues["nome"].ToString(), "computador", userId, e.Keys["id"].ToString(), true, true, true))
                                AddError(e.Errors, gvComputador.Columns["nome"], "Este nome já está cadastrado");
                        }
                        else
                        {
                            if (dados.checkDadoRepetido("nome", e.NewValues["nome"].ToString(), "computador", userId, "", false, true, true))
                                AddError(e.Errors, gvComputador.Columns["nome"], "Este nome já está cadastrado");
                        }
                    }

            }

            if (string.IsNullOrEmpty(e.RowError) && e.Errors.Count > 0) e.RowError = "Corriga todos os erros.";
        }

        void AddError(Dictionary<GridViewColumn, string> errors, GridViewColumn column, string errorText)
        {
            if (errors.ContainsKey(column)) return;
            errors[column] = errorText;
        }

        protected void gvComputadorHist_BeforePerformDataSelect(object sender, EventArgs e)
        {
            Session["id_pai"] = (sender as DevExpress.Web.ASPxGridView).GetMasterRowKeyValue();
        }
    }
}