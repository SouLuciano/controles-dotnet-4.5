﻿using System;
using System.Web;
using System.Web.UI.WebControls;

namespace webequilibrio.app.cadastro
{
    public partial class eqp_tipo : System.Web.UI.Page
    {
        string url;
        string userId;
        app.classe.bdados dados = new classe.bdados();
        protected void Page_Load(object sender, EventArgs e)
        {
            userId = Session["_userId"].ToString();
            url = HttpContext.Current.Request.Url.AbsolutePath;
            bool acesso = dados.temAcesso(Convert.ToInt32(userId), "~" + url);

            if (!acesso)
            {
                Response.Redirect("~/default.aspx", true);
            }
        }



        protected void gvEpi_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            e.NewValues["tipo"] = "epi";
        }

       

        protected void dsTipoEquipamento_Updating(object sender, SqlDataSourceCommandEventArgs e)
        {
            string sql =  $"UPDATE tipo_equipamento SET nome='{Session["_newNomeEpi"].ToString()}' WHERE nome='{Session["_oldNomeEpi"].ToString()}' AND tipo='epi'";
           e.Command.CommandText = sql;
          
        }

        protected void gvEpi_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            Session["_newNomeEpi"] = e.NewValues["nome"].ToString();
            Session["_oldNomeEpi"] = e.OldValues["nome"].ToString();
        }
    }
}