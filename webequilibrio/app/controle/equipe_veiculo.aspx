﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="equipe_veiculo.aspx.cs" Inherits="webequilibrio.app.cadastro.equipe_veiculo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" ShowCollapseButton="true" Width="600px">
        <PanelCollection>
            <dx:PanelContent runat="server">
                <table class="nav-justified">
                    <tr>
                        <td>
                            <table class="nav-justified">
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <dx:ASPxComboBox ID="cbProjeto" runat="server" 
                                            DataSourceID="dsProjetoCbx" TextField="nome" ValueField="id" ValueType="System.Int32" Width="300px" AutoPostBack="True">
                                            <Columns>
                                                <dx:ListBoxColumn Caption="Nome" FieldName="nome" />
                                                <dx:ListBoxColumn Caption="Descrição" FieldName="descricao" />
                                            </Columns>
                                        </dx:ASPxComboBox>
                                        <asp:SqlDataSource ID="dsProjetoCbx" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                                            ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" 
                                            SelectCommand="SELECT id, nome, descricao, ativo FROM projeto ORDER BY nome, descricao"></asp:SqlDataSource>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvEquipeVeiculo" runat="server" DataSourceID="dsEquipeVeiculo" AutoGenerateColumns="False" KeyFieldName="id" Width="100%" OnRowInserting="gvEquipeVeiculo_RowInserting" Theme="Metropolis">
                                <Columns>
                                    <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="1" Visible="False">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="id_projeto" ShowInCustomizationForm="True" VisibleIndex="4" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Equipe" FieldName="id_equipe" ShowInCustomizationForm="True" VisibleIndex="2">
                                        <PropertiesComboBox DataSourceID="dsEquipe" TextField="codigo" ValueField="id">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Veículo" FieldName="id_veiculo" ShowInCustomizationForm="True" VisibleIndex="3">
                                        <PropertiesComboBox DataSourceID="dsVeiculo" TextField="placa" ValueField="id">
                                            <Columns>
                                                <dx:ListBoxColumn Caption="Placa" FieldName="placa" />
                                                <dx:ListBoxColumn Caption="Modelo" FieldName="modelo" />
                                            </Columns>
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                </Columns>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="dsEquipeVeiculo" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                                ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" 
                                SelectCommand="SELECT id, id_veiculo, id_equipe, id_projeto FROM veiculo_equipe WHERE (id_projeto = @id_projeto) ORDER BY id_equipe, id_veiculo" 
                                DeleteCommand="DELETE FROM veiculo_equipe WHERE id = @id" 
                                InsertCommand="INSERT INTO veiculo_equipe (id, id_veiculo, id_equipe, id_projeto) VALUES (@id, @id_veiculo, @id_equipe, @id_projeto)" 
                                UpdateCommand="UPDATE veiculo_equipe SET id_veiculo = @id_veiculo, id_equipe = @id_equipe, id_projeto = @id_projeto WHERE id = @id">
                                <DeleteParameters>
                                    <asp:Parameter Name="id" Type="Int32" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="id" Type="Int32" />
                                    <asp:Parameter Name="id_veiculo" Type="Int32" />
                                    <asp:Parameter Name="id_equipe" Type="Int32" />
                                    <asp:Parameter Name="id_projeto" Type="Int32" />
                                </InsertParameters>
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="cbProjeto" Name="id_projeto" PropertyName="Value" Type="Int32" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="id_veiculo" Type="Int32" />
                                    <asp:Parameter Name="id_equipe" Type="Int32" />
                                    <asp:Parameter Name="id_projeto" Type="Int32" />
                                    <asp:Parameter Name="id" Type="Int32" />
                                </UpdateParameters>
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="dsVeiculo" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                                ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" 
                                SelectCommand="SELECT id, placa, modelo FROM veiculo WHERE (id_projeto = @id)  ORDER BY placa">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="cbProjeto" Name="id" PropertyName="Value" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="dsEquipe" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                                ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" 
                                SelectCommand="SELECT id, codigo, descricao, regiao, ativo FROM equipe WHERE (id_projeto = @id)  ORDER BY codigo">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="cbProjeto" Name="id" PropertyName="Value" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>
</asp:Content>
