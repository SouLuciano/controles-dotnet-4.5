﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using MySql.Data.MySqlClient;
using DevExpress.Web;
using System.Reflection;
using System.Data.SqlClient;

namespace webequilibrio.app.controle
{
    public partial class usergrupourl : System.Web.UI.Page
    {
        app.classe.bdados dados = new app.classe.bdados();
        string url;
        protected void Page_Load(object sender, EventArgs e)
        {
            url = HttpContext.Current.Request.Url.AbsolutePath;
            bool acesso = dados.temAcesso(Convert.ToInt32(Session["_userId"]), "~" + url);

            if (!acesso)
            {
                Response.Redirect("~/default.aspx", true);
            }

        }

        #region Criação updatePanel na construção da pagina
        private void RegisterUpdatePanel1(UpdatePanel sender)
        {
            var sType = typeof(ScriptManager);
            var mInfo = sType.GetMethod("System.Web.UI.IScriptManagerInternal.RegisterUpdatePanel", BindingFlags.NonPublic | BindingFlags.Instance);
            if (mInfo != null)
                mInfo.Invoke(ScriptManager.GetCurrent(Page), new object[] { UpdatePanel1 });
        }

        private void RegisterUpdatePanel2(UpdatePanel sender)
        {
            var sType = typeof(ScriptManager);
            var mInfo = sType.GetMethod("System.Web.UI.IScriptManagerInternal.RegisterUpdatePanel", BindingFlags.NonPublic | BindingFlags.Instance);
            if (mInfo != null)
                mInfo.Invoke(ScriptManager.GetCurrent(Page), new object[] { UpdatePanel2 });
        }

        private void RegisterUpdatePanel3(UpdatePanel sender)
        {
            var sType = typeof(ScriptManager);
            var mInfo = sType.GetMethod("System.Web.UI.IScriptManagerInternal.RegisterUpdatePanel", BindingFlags.NonPublic | BindingFlags.Instance);
            if (mInfo != null)
                mInfo.Invoke(ScriptManager.GetCurrent(Page), new object[] { UpdatePanel3 });
        }

        private void RegisterUpdatePanel4(UpdatePanel sender)
        {
            var sType = typeof(ScriptManager);
            var mInfo = sType.GetMethod("System.Web.UI.IScriptManagerInternal.RegisterUpdatePanel", BindingFlags.NonPublic | BindingFlags.Instance);
            if (mInfo != null)
                mInfo.Invoke(ScriptManager.GetCurrent(Page), new object[] { UpdatePanel4 });
        }

        private void RegisterUpdatePanel5(UpdatePanel sender)
        {
            var sType = typeof(ScriptManager);
            var mInfo = sType.GetMethod("System.Web.UI.IScriptManagerInternal.RegisterUpdatePanel", BindingFlags.NonPublic | BindingFlags.Instance);
            if (mInfo != null)
                mInfo.Invoke(ScriptManager.GetCurrent(Page), new object[] { UpdatePanel5 });
        }

        private void RegisterUpdatePanel6(UpdatePanel sender)
        {
            var sType = typeof(ScriptManager);
            var mInfo = sType.GetMethod("System.Web.UI.IScriptManagerInternal.RegisterUpdatePanel", BindingFlags.NonPublic | BindingFlags.Instance);
            if (mInfo != null)
                mInfo.Invoke(ScriptManager.GetCurrent(Page), new object[] { UpdatePanel6 });
        }


        protected void UpdatePanel1_Unload(object sender, EventArgs e)
        {
            RegisterUpdatePanel1((UpdatePanel)sender);
        }

        protected void UpdatePanel2_Unload(object sender, EventArgs e)
        {
            RegisterUpdatePanel2((UpdatePanel)sender);
        }

        protected void UpdatePanel3_Unload(object sender, EventArgs e)
        {
            RegisterUpdatePanel3((UpdatePanel)sender);
        }

        protected void UpdatePanel4_Unload(object sender, EventArgs e)
        {
            RegisterUpdatePanel4((UpdatePanel)sender);
        }

        protected void UpdatePanel5_Unload(object sender, EventArgs e)
        {
            RegisterUpdatePanel5((UpdatePanel)sender);
        }

        protected void UpdatePanel6_Unload(object sender, EventArgs e)
        {
            RegisterUpdatePanel6((UpdatePanel)sender);
        }
        #endregion

        private DataTable getDadosUserGrupo(int id_user)
        {
            DataTable s_dt = new DataTable();
            string s_SQL;

            s_SQL =
            "SELECT        grupo.id, grupo.nome " +
            "FROM [grupo], [USER], [grupo_user] " +
            "WHERE grupo_user.id_grupo = grupo.id AND grupo_user.id_user = USER.id AND user.id = @p1";


            try
            {
                using (SqlConnection cnx = new SqlConnection(app.classe.bdados.GetCnx()))
                {
                    cnx.Open();

                    using (SqlCommand cmd = new SqlCommand(s_SQL, cnx))
                    {
                        cmd.Parameters.AddWithValue("@p1", id_user);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                            s_dt.Load(reader);
                    }

                    cnx.Close();
                }
            }
            catch (Exception ex)
            {
                // TODO Checar id usuario
                // setLog(0, "err", "getDadosUserByIdOrEmail", ex.Message, "bdados.cs");
            }

            return s_dt;
        }

        protected void listUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                btnUserGrupo1.Enabled = true;
                listGrupo.UnselectAll();
                int id_user = Convert.ToInt32(listUser.SelectedItem.Value);

                data.dsAcesso.grupoDataTable dtGrupo = new data.dsAcesso.grupoDataTable();
                data.dsAcessoTableAdapters.grupoTableAdapter taGrupo = new data.dsAcessoTableAdapters.grupoTableAdapter();

                using (dtGrupo = taGrupo.GetDataByUSER_ID(id_user))
                {
                    for (int i = 0; i < dtGrupo.Rows.Count; i++)
                    {
                        for (int indice = 0; indice < listGrupo.Items.Count; indice++)
                        {
                            if (listGrupo.Items[indice].Value.ToString() == dtGrupo.Rows[i]["id"].ToString())
                            {
                                listGrupo.Items[indice].Selected = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblErroUserGrupo.Visible = true;
                dados.setLog(Convert.ToInt32(Session["_userId"]), "err", "listUser_SelectedIndexChanged", ex.Message, url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            }
        }

        protected void btnUserGrupo_Click(object sender, EventArgs e)
        {
            btnUserGrupo1.Enabled = false;
            int userId = (int)listUser.Value;
            var list = listGrupo.SelectedValues;
            
            data.dsAcesso.grupo_userDataTable dt_GrupoUser = new data.dsAcesso.grupo_userDataTable();
            data.dsAcessoTableAdapters.grupo_userTableAdapter ta_GrupoUser = new data.dsAcessoTableAdapters.grupo_userTableAdapter();

            data.dsAcesso.grupo_user_tmpDataTable dt_GrupoUser_modificado = new data.dsAcesso.grupo_user_tmpDataTable();

            try
            {
                // Deletar registros.
                for (int i = 0; i < list.Count; i++)
                {
                    dt_GrupoUser_modificado.Addgrupo_user_tmpRow(userId, (int)list[i]);
                }

                dt_GrupoUser_modificado.AcceptChanges();
                dt_GrupoUser = ta_GrupoUser.GetDataByID_USER(userId);

                for (int i = dt_GrupoUser.Rows.Count - 1; i >= 0; i--)
                {
                    int idGrupo = Convert.ToInt32(dt_GrupoUser.Rows[i]["id_grupo"]);

                    DataRow[] foundRows = dt_GrupoUser_modificado.Select("id_grupo = " + idGrupo + " and id_user = " + userId);

                    if (foundRows.Count() == 0)
                    {
                        ta_GrupoUser.DeleteQueryByIdUser_IdGrupo(userId, idGrupo);
                    }
                }
            }
            catch (Exception ex)
            {
                lblErroUserGrupo.Visible = true;
                dados.setLog(Convert.ToInt32(Session["_userId"]), "err", "btnUserGrupo_Click DeleteQuery", ex.Message, url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            }

            try
            {
                // Adicionar registros.
                for (int i = dt_GrupoUser_modificado.Rows.Count - 1; i >= 0; i--)
                {
                    int idGrupo = Convert.ToInt32(dt_GrupoUser_modificado.Rows[i]["id_grupo"]);
                    DataRow[] foundRows = dt_GrupoUser.Select("id_grupo = " + idGrupo + " and id_user = " + userId);

                    if (foundRows.Count() == 0)
                    {
                        ta_GrupoUser.Insert(userId, idGrupo);
                    }
                }
            }
            catch (Exception ex)
            {
                lblErroUserGrupo.Visible = true;
                dados.setLog(Convert.ToInt32(Session["_userId"]), "err", "btnUserGrupo_Click InsertQuery", ex.Message, url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            }

            btnUserGrupo1.Enabled = true;
        }

        protected void listGrupoUrl_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                btnGrupoUrl1.Enabled = true;
                listUrl.UnselectAll();
                int id_grupo = Convert.ToInt32(listGrupoUrl.SelectedItem.Value);

                data.dsAcesso.urlDataTable dtUrl = new data.dsAcesso.urlDataTable();
                data.dsAcessoTableAdapters.urlTableAdapter taUrl = new data.dsAcessoTableAdapters.urlTableAdapter();

                using (dtUrl = taUrl.GetDataByGRUPO_ID(id_grupo))
                {
                    for (int i = 0; i < dtUrl.Rows.Count; i++)
                    {
                        for (int indice = 0; indice < listUrl.Items.Count; indice++)
                        {
                            if (listUrl.Items[indice].Value.ToString() == dtUrl.Rows[i]["id"].ToString())
                            {
                                listUrl.Items[indice].Selected = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblErroGrupoUrl.Visible = true;
                dados.setLog(Convert.ToInt32(Session["_userId"]), "err", "listGrupoUrl_SelectedIndexChanged", ex.Message, url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            }
        }

        protected void btnGrupoUrl_Click(object sender, EventArgs e)
        {
            btnGrupoUrl1.Enabled = false;
            int grupoId = (int)listGrupoUrl.Value;
            var list = listUrl.SelectedValues;

            //data.dsEquilibrio.grupo_urlDataTable dt_GrupoUrl = new data.dsEquilibrio.grupo_urlDataTable();
            //data.dsEquilibrioTableAdapters.grupo_urlTableAdapter ta_GrupoUrl = new data.dsEquilibrioTableAdapters.grupo_urlTableAdapter();
            //data.dsEquilibrio.grupo_url_tmpDataTable dt_GrupoUrl_modificado = new data.dsEquilibrio.grupo_url_tmpDataTable();

            data.dsAcesso.grupo_urlDataTable dt_GrupoUrl = new data.dsAcesso.grupo_urlDataTable();
            data.dsAcessoTableAdapters.grupo_urlTableAdapter ta_GrupoUrl = new data.dsAcessoTableAdapters.grupo_urlTableAdapter();
            data.dsAcesso.grupo_url_tmpDataTable dt_GrupoUrl_modificado = new data.dsAcesso.grupo_url_tmpDataTable();

            try
            {
                for (int i = 0; i < list.Count; i++)
                {
                    dt_GrupoUrl_modificado.Addgrupo_url_tmpRow(grupoId, (int)list[i]);
                }
                dt_GrupoUrl_modificado.AcceptChanges();

                dt_GrupoUrl = ta_GrupoUrl.GetDataByID_GRUPO(grupoId);

                for (int i = dt_GrupoUrl.Rows.Count - 1; i >= 0; i--)
                {
                    int idUrl = Convert.ToInt32(dt_GrupoUrl.Rows[i]["id_url"]);

                    DataRow[] foundRows = dt_GrupoUrl_modificado.Select("id_url = " + idUrl + " and id_grupo = " + grupoId);

                    if (foundRows.Count() == 0)
                    {
                        ta_GrupoUrl.DeleteQueryByIdGrupo_IdUrl(grupoId, idUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                lblErroGrupoUrl.Visible = true;
                dados.setLog(Convert.ToInt32(Session["_userId"]), "err", "btnGrupoUrl_Click InsertQuery", ex.Message, url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            }

            try
            {
                // Adicionar registros.
                for (int i = dt_GrupoUrl_modificado.Rows.Count - 1; i >= 0; i--)
                {
                    int idUrl = Convert.ToInt32(dt_GrupoUrl_modificado.Rows[i]["id_url"]);
                    DataRow[] foundRows = dt_GrupoUrl.Select("id_url = " + idUrl + " and id_grupo = " + grupoId);

                    if (foundRows.Count() == 0)
                    {
                        ta_GrupoUrl.InsertQueryByIdGrupo_IdUrl(grupoId, idUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                lblErroGrupoUrl.Visible = true;
                dados.setLog(Convert.ToInt32(Session["_userId"]), "err", "btnGrupoUrl_Click InsertQuery", ex.Message, url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            }

            btnGrupoUrl1.Enabled = true;
        }
    }
}