﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using System.Data;
using DevExpress.Export;
using DevExpress.XtraPrinting;

namespace webequilibrio.app.cadastro
{
    public partial class rastreador : System.Web.UI.Page
    {

        app.classe.bdados dados = new app.classe.bdados();
        string url;
        int id_projeto, id_celular, id_veiculo, id_equipe, id_projetoNew, id_celularNew, id_veiculoNew, id_rastreador;

        protected void gvRastreador_CustomErrorText(object sender, ASPxGridViewCustomErrorTextEventArgs e)
        {
            if (e.Exception.Message.Contains("fk_idCelular_idRastreador"))
                e.ErrorText = "O rastreador já foi cadastrado";

            if (e.Exception.Message.Contains("UniqeIdVeiculo"))
                e.ErrorText = "O veículo já foi cadastrado";
        }

        string status, numero1, numero2, observacao, user_email, statusNew, numero1New, numero2New, observacaoNew;


        protected void Page_Load(object sender, EventArgs e)
        {
            url = HttpContext.Current.Request.Url.AbsolutePath;
            bool acesso = dados.temAcesso(Convert.ToInt32(Session["_userId"]), "~" + url);

            if (!acesso)
            {
                Response.Redirect("~/default.aspx", true);
            }

            if (cbProjeto.Value == null)
            {
                gvRastreador.Visible = false;
                btnExportar.Enabled = false;
            }
            else
            {
                gvRastreador.Visible = true;
                btnExportar.Enabled = true;
            }
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            gvExporter.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }

        protected void gvRastreadorHistorico_BeforePerformDataSelect(object sender, EventArgs e)
        {
            Session["id_rastreador"] = (sender as ASPxGridView).GetMasterRowKeyValue();
        }

        protected void gvRastreador_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
        {
            data.dsEquilibrioTableAdapters.rastreador_historicoTableAdapter ta = new data.dsEquilibrioTableAdapters.rastreador_historicoTableAdapter();
            ta.Insert(id_rastreador, id_celular, id_veiculo, status, numero1, numero2, observacao, DateTime.Now, user_email, id_equipe);
        }

        protected void gvRastreador_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            DataTable dtUser = (DataTable)Session["_user"];
            e.NewValues["id_projeto"] = cbProjeto.Value;
            //e.NewValues["id_equipe"] = cbEquipe.Value;
            e.NewValues["user_email"] = dtUser.Rows[0]["email"];
            e.NewValues["dt_atualizacao"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

        }

        protected void gvRastreador_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            e.NewValues["dt_atualizacao"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            id_rastreador = Convert.ToInt32(e.Keys["id"]);
            id_projeto = Convert.ToInt32(e.OldValues["id_projeto"]);
            id_celular = Convert.ToInt32(e.OldValues["id_celular"]);
            if (e.OldValues["id_veiculo"].ToString() != "") id_veiculo = Convert.ToInt32(e.OldValues["id_veiculo"]);
            id_equipe = Convert.ToInt32(e.OldValues["id_equipe"]);
            user_email = e.OldValues["user_email"].ToString();
            id_projeto = Convert.ToInt32(e.OldValues["id_projeto"]);
            status = e.OldValues["status"].ToString();
            if (e.OldValues["numero1"].ToString().Trim() == "") numero1 = null; else numero1 = e.OldValues["numero1"].ToString();
            if (e.OldValues["numero2"].ToString().Trim() == "") numero2 = null; else numero2 = e.OldValues["numero2"].ToString();
            if (e.OldValues["observacao"].ToString().Trim() == "") observacao = null; else observacao = e.OldValues["observacao"].ToString();

        }

    }
}