﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webequilibrio.app.classe
{
    public class celular
    {
        public int id { get; set; }
        public int id_celular { get; set; }
        public int id_projeto { get; set; }
        public string modelo { get; set; }
        public string imei1 { get; set;}
        public string imei2 { get; set; }
        public Boolean ativo { get; set; }
        public string observacao { get; set; }
        public int? id_simcard1 { get; set; }
        public int? id_simcard2 { get; set; }
        public string sistema_op { get; set; }
        public string senha { get; set; }
        public string status { get; set; }
        public string user_email { get; set; }
        public DateTime dt_criacao { get; set; }
        public int id_equipe { get; set; }
        public string funcao { get; set; }
        public int id_regional { get; set; }

    }
}