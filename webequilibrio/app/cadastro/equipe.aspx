﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="equipe.aspx.cs" Inherits="webequilibrio.app.cadastro.equipe" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" HeaderText="Cadastro de equipes" Theme="Metropolis" Width="100%">
        <PanelCollection>
            <dx:PanelContent runat="server">
                <table style="width: 100%">
                    <tr>
                        <td>
                            <table style="width: 400px">
                                <tr>
                                    <td><span style="color: rgb(64, 64, 64); font-family: Roboto, 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: center; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none; background-color: rgb(255, 255, 255)">&nbsp;<asp:Label ID="Label1" runat="server" Text="Mostrar itens"></asp:Label>
                                        </span></td>
                                    <td>
                                        <dx:ASPxComboBox ID="cbxAtivo" runat="server" AutoPostBack="True" SelectedIndex="0" Theme="Metropolis">
                                            <Items>
                                                <dx:ListEditItem Selected="True" Text="Ativos" Value="True" />
                                                <dx:ListEditItem Text="Inativos" Value="False" />
                                            </Items>
                                        </dx:ASPxComboBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvEquipe" runat="server" AutoGenerateColumns="False" DataSourceID="ds_equipe" KeyFieldName="id" OnRowInserted="gvEquipe_RowInserted" OnRowInserting="gvEquipe_RowInserting" OnRowUpdated="gvEquipe_RowUpdated" OnRowUpdating="gvEquipe_RowUpdating" OnRowValidating="gvEquipe_RowValidating" Theme="Metropolis" OnCustomErrorText="gvEquipe_CustomErrorText" OnCellEditorInitialize="gvEquipe_CellEditorInitialize" ClientInstanceName="grid" OnCancelRowEditing="gvEquipe_CancelRowEditing">
                                <SettingsEditing Mode="PopupEditForm">
                                </SettingsEditing>
                                <Settings ShowFilterRow="True" ShowFilterRowMenu="True" />
                                <SettingsSearchPanel Visible="True" />
                                <Columns>
                                    <dx:GridViewCommandColumn ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="10">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Código" FieldName="codigo" ShowInCustomizationForm="True" VisibleIndex="3">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Descrição" FieldName="descricao" ShowInCustomizationForm="True" VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Projeto equipe" FieldName="projeto_equipe" ShowInCustomizationForm="True" VisibleIndex="5" >
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataCheckColumn Caption="Ativo" FieldName="ativo" ShowInCustomizationForm="True" Visible="False" VisibleIndex="6">
                                        <EditFormSettings Visible="True" />
                                    </dx:GridViewDataCheckColumn>
                                    <dx:GridViewDataTextColumn Caption="e-mail" FieldName="user_email" ShowInCustomizationForm="True" Visible="False" VisibleIndex="7">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn FieldName="dt_criacao" ReadOnly="True" ShowInCustomizationForm="True" Visible="False" VisibleIndex="8">
                                        <PropertiesDateEdit DisplayFormatString="g">
                                        </PropertiesDateEdit>
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataDateColumn Caption="Data hora atualização" FieldName="dt_atualizacao" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="9">
                                        <PropertiesDateEdit DisplayFormatString="g">
                                        </PropertiesDateEdit>
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Projeto" FieldName="id_projeto" ShowInCustomizationForm="True" VisibleIndex="1">
                                        <PropertiesComboBox DataSourceID="dsProjeto" TextField="nome" ValueField="id">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Regional" FieldName="id_regional" ShowInCustomizationForm="True" VisibleIndex="2">
                                        <PropertiesComboBox DataSourceID="dsRegional" TextField="regional" ValueField="id">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                </Columns>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" Text="Exportar para excel" Theme="Metropolis">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="ds_equipe" runat="server" 
        ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
        DeleteCommand="DELETE FROM [equipe] WHERE [id] = @id" 
        InsertCommand="INSERT INTO [equipe] ([id_projeto], [id_regional], [codigo], [descricao], [projeto_equipe], [ativo], [user_email], [dt_criacao], [dt_atualizacao]) VALUES (@id_projeto, @id_regional, @codigo, @descricao, @projeto_equipe, @ativo, @user_email, @dt_criacao, @dt_atualizacao)" SelectCommand="SELECT [id], [id_projeto], [id_regional], [codigo], [descricao], [projeto_equipe], [ativo], [user_email], [dt_criacao], [dt_atualizacao] FROM [equipe] WHERE ([ativo] = @ativo)" 
        UpdateCommand="UPDATE [equipe] SET [id_projeto] = @id_projeto, [id_regional] = @id_regional, [codigo] = @codigo, [descricao] = @descricao, [projeto_equipe] = @projeto_equipe, [ativo] = @ativo, [user_email] = @user_email, [dt_criacao] = @dt_criacao, [dt_atualizacao] = @dt_atualizacao WHERE [id] = @id">
                    <DeleteParameters>
                        <asp:Parameter Name="id" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="id_projeto" Type="Int32" />
                        <asp:Parameter Name="id_regional" Type="Int32" />
                        <asp:Parameter Name="codigo" Type="String" />
                        <asp:Parameter Name="descricao" Type="String" />
                        <asp:Parameter Name="projeto_equipe" Type="String" />
                        <asp:Parameter Name="ativo" Type="Boolean" />
                        <asp:Parameter Name="user_email" Type="String" />
                        <asp:Parameter Name="dt_criacao" Type="DateTime" />
                        <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="cbxAtivo" DefaultValue="True" Name="ativo" PropertyName="Value" Type="Boolean" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="id_projeto" Type="Int32" />
                        <asp:Parameter Name="id_regional" Type="Int32" />
                        <asp:Parameter Name="codigo" Type="String" />
                        <asp:Parameter Name="descricao" Type="String" />
                        <asp:Parameter Name="projeto_equipe" Type="String" />
                        <asp:Parameter Name="ativo" Type="Boolean" />
                        <asp:Parameter Name="user_email" Type="String" />
                        <asp:Parameter Name="dt_criacao" Type="DateTime" />
                        <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                        <asp:Parameter Name="id" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsProjeto" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [nome] FROM [projeto] WHERE ([ativo] = @ativo) ORDER BY [nome]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsRegional" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [regional] FROM [regional] WHERE ([ativo] = @ativo) ORDER BY [regional]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:HiddenField ID="HiddenField1" runat="server" />
                <dx:ASPxGridViewExporter ID="gvExporter" runat="server" ExportedRowType="All" FileName="Equipes" GridViewID="gvEquipe" PaperKind="A4">
                </dx:ASPxGridViewExporter>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>
</asp:Content>
