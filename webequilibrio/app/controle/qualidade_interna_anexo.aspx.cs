﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace webequilibrio.app.controle
{
    public partial class qualidade_interna_anexo : System.Web.UI.Page
    {
        string userId;
        app.classe.bdados dados = new classe.bdados();
        string TipoDeacessoTblNC = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            TipoDeacessoTblNC = dados.getTipoDeAcessoQualidadeInterna(Session["_userId"].ToString());
        }

        protected void gvFoto_CommandButtonInitialize(object sender, DevExpress.Web.ASPxGridViewCommandButtonEventArgs e)
        {
            if (TipoDeacessoTblNC != "criarEditarQualquerNc")
            {
                // ocultar botões
                if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Edit)
                    e.Visible = false;

                if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.New)
                    e.Visible = false;

                if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Delete)
                    e.Visible = false;
            }
        }
    }
}