﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Data;
using Dapper;
using System.Data.SqlClient;
using DevExpress.Web.ASPxPivotGrid;
using System.Collections;
using System.Web;


namespace webequilibrio.app.grafico.veiculo
{
    public partial class veiculo_km_rodado : System.Web.UI.Page
    {
        app.classe.bdados clsDados = new classe.bdados();
        dsVeiculo.porEquipeDataTable dtPorEquipe = new dsVeiculo.porEquipeDataTable();
        dsVeiculo.porProjetoDataTable dtPorProjeto = new dsVeiculo.porProjetoDataTable();
        dsVeiculo.porAnoDataTable dtPorAno = new dsVeiculo.porAnoDataTable();
        app.classe.bdados dados = new app.classe.bdados();
        DataRow row;
        string url;
        string userId;
        string email;

        protected void Page_Load(object sender, EventArgs e)
        {
            userId = Session["_userId"].ToString();
            url = HttpContext.Current.Request.Url.AbsolutePath;

            if (!IsPostBack)
            {
                ASPxPageControl1.ActiveTabIndex = 0;

                cbxAno.Value = DateTime.Now.Year;
                cbxRegional.Value = 0;
                cbxMes.SelectedIndex = DateTime.Now.Month - 1;

                wcMediaKmPorMes.Titles[0].Text = "Média Km por mês em " + cbxAno.Text;
                wcSomaKmPorMes.Titles[0].Text = "Soma Km por mês em " + cbxAno.Text;
                wcPorProjeto.Titles[0].Text = "Média Km por projeto no mês de " + cbxMes.Text;
                wcSomaPorEquipeNoMes.Titles[0].Text = "Soma Km por equipe no mês de " + cbxMes.Text;

                //EXPANDE SOMENTE A ULTIMA COLUNA DO ANO
                this.gvSomaKm.Fields["Ano"].CollapseAll();
                PivotGridField field = gvSomaKm.Fields["Ano"];
                int qtd = field.GetUniqueValues().Length - 1;              
                field.ExpandValue(field.GetUniqueValues()[qtd]);

               
            }
        }

        private List<porEquipe> getKmPorEquipe()
        {
            List<porEquipe> kmPorEquipe = new List<porEquipe>();

            string sql = @"SELECT equipe.codigo as equipe, SUM(veiculo_km_rodado.km_rodado) as kmRodado 
                        FROM equipe, veiculo_km_rodado 
                        WHERE 
                        veiculo_km_rodado.id_equipe = equipe.id 
                        GROUP BY equipe.codigo ";

            using (var sqlConnection = new SqlConnection(classe.bdados.GetCnx()))
            {
                var result = sqlConnection.Query<porEquipe>(sql);

                foreach (porEquipe dados in result)
                    kmPorEquipe.Add(dados);

                //foreach (porEquipe dados in result)
                //{
                //    row = dtPorEquipe.NewRow();
                //    row["equipe"] = dados.equipe;
                //    row["kmRodado"] = dados.kmRodado;
                //    dtPorEquipe.Rows.Add(row);
                //}
            }

            return kmPorEquipe;
        }

        private List<porProjeto> getKmPorProjeto()
        {
            int ano = (int)cbxAno.Value;

            List<porProjeto> kmPorProjeto = new List<porProjeto>();

            string sql = $@"SELECT equipe.projeto_equipe AS projeto, AVG(veiculo_km_rodado.km_rodado)AS kmRodado 
                        FROM equipe, veiculo_km_rodado 
                        WHERE veiculo_km_rodado.id_equipe = equipe.id AND veiculo_km_rodado.ano = {ano}
                        GROUP BY projeto_equipe";

            using (var sqlConnection = new SqlConnection(classe.bdados.GetCnx()))
            {
                var result = sqlConnection.Query<porProjeto>(sql);

                foreach (porProjeto dados in result)
                    kmPorProjeto.Add(dados);

                //foreach (porProjeto dados in result)
                //{
                //    row = dtPorProjeto.NewRow();
                //    row["projeto"] = dados.projeto;
                //    row["kmRodado"] = dados.kmRodado;
                //    dtPorProjeto.Rows.Add(row);
                //}
            }

            return kmPorProjeto;
        }

        private List<porAno> getKmPorAno()
        {
            List<porAno> kmPorAno = new List<porAno>();
            string sql = @"SELECT ano, AVG(veiculo_km_rodado.km_rodado)AS kmRodado 
                            FROM equipe, veiculo_km_rodado 
                            WHERE veiculo_km_rodado.id_equipe = equipe.id 
                            GROUP BY ano";

            using (var sqlConnection = new SqlConnection(classe.bdados.GetCnx()))
            {
                var result = sqlConnection.Query<porAno>(sql);

                foreach (porAno dados in result)
                    kmPorAno.Add(dados);
            }

            return kmPorAno;
        }

        /// <summary>
        ///  Média KM rodado por ano
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dsPorAno_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            string sql = @"SELECT ano, ROUND(AVG(CAST((kmRodado) AS FLOAT)), 2) AS kmRodado 
                            FROM  
                            (
                            SELECT ano, equipe.projeto_equipe AS projeto, SUM(veiculo_km_rodado.km_rodado)AS kmRodado 
                                            FROM equipe, veiculo_km_rodado, veiculo 
                                            WHERE veiculo_km_rodado.id_equipe = equipe.id 
							                    AND veiculo_km_rodado.id_veiculo = veiculo.id ";

            if (cbxRegional.Value.ToString() != "0")
                sql = sql + $"AND veiculo_km_rodado.id_regional = {cbxRegional.Value} ";

            if (chkQuadriciclo.Checked)
                sql = sql + " AND veiculo.modelo = 'Quadriciclo' ";
            else
                sql = sql + " AND veiculo.modelo <> 'Quadriciclo' ";

            sql = sql + @"GROUP BY projeto_equipe, ano
                        ) as inner_query
                        group by ano ";

            try
            {
                e.Command.Connection.Open();
                e.Command.CommandText = sql;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                e.Command.Connection.Close();
            }

        }

        protected void dsCbxRegional_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Connection.Open();
            e.Command.CommandText = "SELECT 0 AS id, 'Todas' AS regional " +
                                    "UNION ALL " +
                                    "SELECT id, regional " +
                                    "FROM regional";

            e.Command.ExecuteNonQuery();
            e.Command.Connection.Close();
        }

        // Média por projeto no mês filtrado.
        protected void dsPorProjeto_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            string sql = $@"SELECT projeto, ROUND(AVG(CAST((kmRodado) AS FLOAT)), 2) AS kmRodado
                            FROM (
                            SELECT equipe.projeto_equipe AS projeto, SUM(veiculo_km_rodado.km_rodado)AS kmRodado 
                              FROM equipe, veiculo_km_rodado, veiculo 
                              WHERE veiculo_km_rodado.id_equipe = equipe.id 
						      AND veiculo_km_rodado.ano = {cbxAno.Value}  
						      AND veiculo_km_rodado.id_veiculo = veiculo.id 
                              AND veiculo_km_rodado.mes = '{cbxMes.Value}' ";

            if (cbxRegional.Value.ToString() != "0")
                sql = sql + $"AND veiculo_km_rodado.id_regional = {cbxRegional.Value} ";


            if (chkQuadriciclo.Checked)
                sql = sql + " AND veiculo.modelo = 'Quadriciclo' ";
            else
                sql = sql + " AND veiculo.modelo <> 'Quadriciclo' ";


            sql = sql + @"GROUP BY projeto_equipe, codigo
                        ) AS inner_query
                        GROUP BY projeto ";
            try
            {
                e.Command.Connection.Open();
                e.Command.CommandText = sql;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                e.Command.Connection.Close();
            }
        }

        protected void dsPorEquipe_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            string sql = $@"SELECT equipe.projeto_equipe AS projeto, equipe.codigo AS equipe, SUM(veiculo_km_rodado.km_rodado) AS kmRodado 
                         FROM veiculo, veiculo_km_rodado, equipe 
                         WHERE veiculo_km_rodado.id_equipe = equipe.id AND veiculo_km_rodado.id_veiculo = veiculo.id 
                         AND veiculo_km_rodado.ano = {cbxAno.Value} 
                         AND veiculo_km_rodado.mes = '{cbxMes.Value}' ";

            if (cbxRegional.Value.ToString() != "0")
                sql = sql + $"AND veiculo_km_rodado.id_regional = {cbxRegional.Value} ";


            if (chkQuadriciclo.Checked)
                sql = sql + " AND veiculo.modelo = 'Quadriciclo' ";
            else
                sql = sql + " AND veiculo.modelo <> 'Quadriciclo' ";


            sql = sql + "GROUP BY equipe.codigo, equipe.projeto_equipe; ";

            try
            {
                e.Command.Connection.Open();
                e.Command.CommandText = sql;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                e.Command.Connection.Close();
            }
        }

        /// <summary>
        /// Média rodado por mês
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dsPorMes_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            string sql = $@"SELECT mes, ROUND(AVG(CAST((kmRodado) AS FLOAT)), 2) AS kmRodado 
                    FROM(
	                    SELECT veiculo_km_rodado.mes , SUM(veiculo_km_rodado.km_rodado) AS kmRodado 
                                                FROM equipe, veiculo_km_rodado, veiculo  
                                                WHERE veiculo_km_rodado.id_equipe = equipe.id 
                                                    AND ano = {cbxAno.Value}  
                                                    AND veiculo_km_rodado.id_veiculo = veiculo.id
							                    ";

            if (cbxRegional.Value.ToString() != "0")
                sql = sql + $"AND veiculo_km_rodado.id_regional = {cbxRegional.Value} ";


            if (chkQuadriciclo.Checked)
                sql = sql + " AND veiculo.modelo = 'Quadriciclo' ";
            else
                sql = sql + " AND veiculo.modelo <> 'Quadriciclo' ";


            sql = sql + @" GROUP BY  mes,veiculo_km_rodado.id_equipe 
                    ) as inner_query
                    GROUP BY mes  
                           ORDER BY case
                            when mes = 'jan' then 1
                            when mes = 'fev' then 2
                            when mes = 'mar' then 3
                            when mes = 'abr' then 4
                            when mes = 'mai' then 5
                            when mes = 'jun' then 6
                            when mes = 'jul' then 7
                            when mes = 'ago' then 8
                            when mes = 'set' then 9
                            when mes = 'out' then 10
                            when mes = 'nov' then 11
                            when mes = 'dez' then 12
                            else 13 -- Might want the default case, too
                        end";

            try
            {
                e.Command.Connection.Open();
                e.Command.CommandText = sql;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                e.Command.Connection.Close();
            }
        }

        /// <summary>
        /// Soma rodado por mês
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dsPorMesSoma_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            string sql = $@"SELECT veiculo_km_rodado.mes , ROUND(SUM(CAST(veiculo_km_rodado.km_rodado AS FLOAT)), 2) AS kmRodado 
                            FROM equipe, veiculo_km_rodado, veiculo  
                            WHERE veiculo_km_rodado.id_equipe = equipe.id 
                                AND ano = {cbxAno.Value} 
                                AND veiculo_km_rodado.id_veiculo = veiculo.id ";

            if (cbxRegional.Value.ToString() != "0")
                sql = sql + $"AND veiculo_km_rodado.id_regional = {cbxRegional.Value} ";


            if (chkQuadriciclo.Checked)
                sql = sql + " AND veiculo.modelo = 'Quadriciclo' ";
            else
                sql = sql + " AND veiculo.modelo <> 'Quadriciclo' ";


            sql = sql + @" GROUP BY ano, mes 
                           ORDER BY case
                            when mes = 'jan' then 1
                            when mes = 'fev' then 2
                            when mes = 'mar' then 3
                            when mes = 'abr' then 4
                            when mes = 'mai' then 5
                            when mes = 'jun' then 6
                            when mes = 'jul' then 7
                            when mes = 'ago' then 8
                            when mes = 'set' then 9
                            when mes = 'out' then 10
                            when mes = 'nov' then 11
                            when mes = 'dez' then 12
                            else 13 -- Might want the default case, too
                        end";

            try
            {
                e.Command.Connection.Open();
                e.Command.CommandText = sql;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                e.Command.Connection.Close();
            }
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            wcPorProjeto.Titles[0].Text = "Média Km por projeto no mês de " + cbxMes.Text;
            wcSomaPorEquipeNoMes.Titles[0].Text = "Soma Km por equipe no mês de " + cbxMes.Text;
            wcMediaKmPorMes.Titles[0].Text = "Média Km por mês em " + cbxAno.Text;
            wcSomaKmPorMes.Titles[0].Text = "Soma Km por mês em " + cbxAno.Text;
        }


        protected void gvSomaKm_CustomFieldSort(object sender, DevExpress.Web.ASPxPivotGrid.PivotGridCustomFieldSortEventArgs e)
        {

            object orderValue1 = e.GetListSourceColumnValue(e.ListSourceRowIndex1, "numMes"),
             orderValue2 = e.GetListSourceColumnValue(e.ListSourceRowIndex2, "numMes");
            e.Result = Comparer.Default.Compare(orderValue1, orderValue2);
            e.Handled = true;

           /*
            //if (e.Field.FieldName == "mes")
            //{
            //    string s1, s2;

            //    #region Valor 1
            //    switch (e.Value1.ToString())
            //    {
            //        case "jan":
            //            s1 = "1";
            //            break;
            //        case "fev":
            //            s1 = "2";
            //            break;
            //        case "mar":
            //            s1 = "3";
            //            break;
            //        case "abr":
            //            s1 = "4";
            //            break;
            //        case "mai":
            //            s1 = "5";
            //            break;
            //        case "jun":
            //            s1 = "6";
            //            break;
            //        case "jul":
            //            s1 = "7";
            //            break;
            //        case "ago":
            //            s1 = "8";
            //            break;
            //        case "set":
            //            s1 = "9";
            //            break;
            //        case "out":
            //            s1 = "10";
            //            break;
            //        case "nov":
            //            s1 = "11";
            //            break;
            //        case "dez":
            //            s1 = "12";
            //            break;
            //        default:
            //            s1 = "";
            //            break;
            //    }
            //    #endregion
            //    #region Valor 2
            //    switch (e.Value2.ToString())
            //    {
            //        case "jan":
            //            s2 = "1";
            //            break;
            //        case "fev":
            //            s2 = "2";
            //            break;
            //        case "mar":
            //            s2 = "3";
            //            break;
            //        case "abr":
            //            s2 = "4";
            //            break;
            //        case "mai":
            //            s2 = "5";
            //            break;
            //        case "jun":
            //            s2 = "6";
            //            break;
            //        case "jul":
            //            s2 = "7";
            //            break;
            //        case "ago":
            //            s2 = "8";
            //            break;
            //        case "set":
            //            s2 = "9";
            //            break;
            //        case "out":
            //            s2 = "10";
            //            break;
            //        case "nov":
            //            s2 = "11";
            //            break;
            //        case "dez":
            //            s2 = "12";
            //            break;
            //        default:
            //            s2 = "";
            //            break;
            //    }
            //    #endregion

            //    if (Convert.ToInt32(s1) > Convert.ToInt32(s2))
            //        e.Result = 1;
            //    else
            //   if (Convert.ToInt32(s1) == Convert.ToInt32(s2))
            //        e.Result = Comparer<Int32>.Default.Compare(Convert.ToInt32(s1),
            //          Convert.ToInt32(s2));
            //    else
            //        e.Result = -1;
            //}
            */

        }

        protected void btnExpPGridSomaKm_Click(object sender, EventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "exportou", "Dados cruzados soma km", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            ASPxPivotGridExporter1.ExportXlsxToResponse("Km_Rodado_soma_por_equipe"); // .WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }
    }
}