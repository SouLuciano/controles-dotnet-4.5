﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="cadastro.aspx.cs" Inherits="webequilibrio.app.residuo.cadastro" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxGridView ID="gvResiduo" runat="server" DataSourceID="dsEquilibrio" AutoGenerateColumns="False" KeyFieldName="id" OnRowUpdating="gvResiduo_RowUpdating" OnRowInserted="gvResiduo_RowInserted" OnRowValidating="gvResiduo_RowValidating" Theme="Metropolis" EnableTheming="True">
        <SettingsEditing Mode="PopupEditForm">
        </SettingsEditing>
        <Settings ShowFilterRow="True" ShowFilterRowMenu="True" />
        <SettingsBehavior AutoFilterRowInputDelay="2200" />
        <SettingsDataSecurity AllowDelete="False" />
        <SettingsPopup>
            <EditForm HorizontalAlign="WindowCenter" MinWidth="700px" />
        </SettingsPopup>
        <Columns>
            <dx:GridViewCommandColumn ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0" ShowClearFilterButton="True">
            </dx:GridViewCommandColumn>
<dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="31" Caption="Nº da amostra">
<EditFormSettings Visible="False"></EditFormSettings>
</dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="dt_monitoramento" VisibleIndex="4" Caption="Data monitoramento">
                <PropertiesDateEdit AllowMouseWheel="False">
                </PropertiesDateEdit>
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn FieldName="hr_inicio" VisibleIndex="5" Caption="Hr Início">
                <PropertiesTextEdit>
                    <MaskSettings Mask="HH:mm" />
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="hr_fim" VisibleIndex="6" Caption="Hr Fim">
                <PropertiesTextEdit>
                    <MaskSettings Mask="HH:mm" />
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="placa" VisibleIndex="7" Caption="Placa">
                <PropertiesTextEdit>
                    <MaskSettings Mask="AAA-0000" />
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="go" VisibleIndex="8" Caption="GO">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="emp_transp" VisibleIndex="9" Caption="Empresa transporte">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="unidade" VisibleIndex="10" Caption="Unidade">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="cod_projeto" VisibleIndex="11" Caption="Cod. Projeto">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="projeto" VisibleIndex="12" Caption="Projeto">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="talhao" VisibleIndex="13" Caption="Talhão">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="modulo" VisibleIndex="15" Caption="Modulo">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="semana_corte" VisibleIndex="16" Caption="Semana corte">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="peso_bruto" VisibleIndex="17" Caption="Peso bruto">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="peso_liquido" VisibleIndex="18" Caption="Peso líquido">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="casca_amostra" VisibleIndex="19" Caption="Amostra casca presa">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="num_tora" VisibleIndex="20" Caption="Nº Toras">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="casca_presa" VisibleIndex="21" Caption="Casca presa">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="casca_solta" VisibleIndex="22" Caption="Casca solta">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="madeira_inapto" VisibleIndex="23" Caption="Madeira Fora da Especificação">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="areia" VisibleIndex="24" Caption="Areia/Terra">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="outro" VisibleIndex="25" Caption="Outros">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="descricao" VisibleIndex="26" Caption="Descrição de outros">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="avaliador" VisibleIndex="27" Caption="Avaliador">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="classe" VisibleIndex="28" Visible="False">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="dt_criacao" VisibleIndex="29" Visible="False">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataDateColumn FieldName="dt_atualizacao" VisibleIndex="30" Visible="False">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn FieldName="genetico" VisibleIndex="14" Caption="Mat. Genético">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn Caption="Remover amostra" FieldName="ativo" VisibleIndex="32">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataCheckColumn Caption="Teste" FieldName="teste" VisibleIndex="33">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataCheckColumn Caption="Teste no consolidado" FieldName="teste_noconsolidado" ToolTip="Alem do teste, o valor da amostra também é usado nos gráficos do consolidado" VisibleIndex="34">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataComboBoxColumn Caption="Mês" FieldName="mes" VisibleIndex="2">
                <PropertiesComboBox>
                    <Items>
                        <dx:ListEditItem Text="Janeiro" Value="1" />
                        <dx:ListEditItem Text="Fevereiro" Value="2" />
                        <dx:ListEditItem Text="Março" Value="3" />
                        <dx:ListEditItem Text="Abril" Value="4" />
                        <dx:ListEditItem Text="Maio" Value="5" />
                        <dx:ListEditItem Text="Junho" Value="6" />
                        <dx:ListEditItem Text="Julho" Value="7" />
                        <dx:ListEditItem Text="Agosto" Value="8" />
                        <dx:ListEditItem Text="Setembro" Value="9" />
                        <dx:ListEditItem Text="Outubro" Value="10" />
                        <dx:ListEditItem Text="Novembro" Value="11" />
                        <dx:ListEditItem Text="Dezembro" Value="12" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn Caption="Semana" FieldName="semana" VisibleIndex="3">
                <PropertiesComboBox>
                    <Items>
                        <dx:ListEditItem Text="1 Semana" Value="1" />
                        <dx:ListEditItem Text="2 Semana" Value="2" />
                        <dx:ListEditItem Text="3 Semana" Value="3" />
                        <dx:ListEditItem Text="4 Semana" Value="4" />
                        <dx:ListEditItem Text="5 Semana" Value="5" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataTextColumn Caption="Observações" FieldName="obs" VisibleIndex="35">
                <PropertiesTextEdit MaxLength="256">
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Ano" FieldName="ano" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
    <dx:ASPxGridViewExporter ID="gvExporter" runat="server" FileName="residuo" GridViewID="gvResiduo">
    </dx:ASPxGridViewExporter>
    <asp:Button ID="btnExportar" runat="server" OnClick="btnExportar_Click" Text="Exportar" Visible="False" />
    <asp:SqlDataSource ID="dsEquilibrio" runat="server" 
        ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
        DeleteCommand="DELETE FROM [residuo_madeira] WHERE [id] = @id" 
        InsertCommand="INSERT INTO [residuo_madeira] ([mes], [semana], [dt_monitoramento], [hr_inicio], [hr_fim], [placa], [go], [emp_transp], [unidade], [cod_projeto], [projeto], [talhao], [genetico], [id_residuo_madeira_modulo], [semana_corte], [peso_bruto], [peso_liquido], [casca_amostra], [num_tora], [casca_presa], [casca_solta], [madeira_inapto], [areia], [outro], [descricao], [avaliador], [teste], [teste_noconsolidado], [obs], [dt_criacao], [dt_atualizacao], [user_email], [ativo], [ano]) VALUES (@mes, @semana, @dt_monitoramento, @hr_inicio, @hr_fim, @placa, @go, @emp_transp, @unidade, @cod_projeto, @projeto, @talhao, @genetico, @id_residuo_madeira_modulo, @semana_corte, @peso_bruto, @peso_liquido, @casca_amostra, @num_tora, @casca_presa, @casca_solta, @madeira_inapto, @areia, @outro, @descricao, @avaliador, @teste, @teste_noconsolidado, @obs, @dt_criacao, @dt_atualizacao, @user_email, @ativo, @ano)" 
        SelectCommand="SELECT * FROM [residuo_madeira]" 
        UpdateCommand="UPDATE [residuo_madeira] SET [mes] = @mes, [semana] = @semana, [dt_monitoramento] = @dt_monitoramento, [hr_inicio] = @hr_inicio, [hr_fim] = @hr_fim, [placa] = @placa, [go] = @go, [emp_transp] = @emp_transp, [unidade] = @unidade, [cod_projeto] = @cod_projeto, [projeto] = @projeto, [talhao] = @talhao, [genetico] = @genetico, [id_residuo_madeira_modulo] = @id_residuo_madeira_modulo, [semana_corte] = @semana_corte, [peso_bruto] = @peso_bruto, [peso_liquido] = @peso_liquido, [casca_amostra] = @casca_amostra, [num_tora] = @num_tora, [casca_presa] = @casca_presa, [casca_solta] = @casca_solta, [madeira_inapto] = @madeira_inapto, [areia] = @areia, [outro] = @outro, [descricao] = @descricao, [avaliador] = @avaliador, [teste] = @teste, [teste_noconsolidado] = @teste_noconsolidado, [obs] = @obs, [dt_criacao] = @dt_criacao, [dt_atualizacao] = @dt_atualizacao, [user_email] = @user_email, [ativo] = @ativo, [ano] = @ano WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="mes" Type="String" />
            <asp:Parameter Name="semana" Type="String" />
            <asp:Parameter Name="dt_monitoramento" Type="DateTime" />
            <asp:Parameter Name="hr_inicio" DbType="Time" />
            <asp:Parameter Name="hr_fim" DbType="Time" />
            <asp:Parameter Name="placa" Type="String" />
            <asp:Parameter Name="go" Type="String" />
            <asp:Parameter Name="emp_transp" Type="String" />
            <asp:Parameter Name="unidade" Type="String" />
            <asp:Parameter Name="cod_projeto" Type="String" />
            <asp:Parameter Name="projeto" Type="String" />
            <asp:Parameter Name="talhao" Type="String" />
            <asp:Parameter Name="genetico" Type="String" />
            <asp:Parameter Name="id_residuo_madeira_modulo" Type="Int16" />
            <asp:Parameter Name="semana_corte" Type="Byte" />
            <asp:Parameter Name="peso_bruto" Type="Double" />
            <asp:Parameter Name="peso_liquido" Type="Double" />
            <asp:Parameter Name="casca_amostra" Type="Double" />
            <asp:Parameter Name="num_tora" Type="Byte" />
            <asp:Parameter Name="casca_presa" Type="Double" />
            <asp:Parameter Name="casca_solta" Type="Double" />
            <asp:Parameter Name="madeira_inapto" Type="Double" />
            <asp:Parameter Name="areia" Type="Double" />
            <asp:Parameter Name="outro" Type="Double" />
            <asp:Parameter Name="descricao" Type="String" />
            <asp:Parameter Name="avaliador" Type="String" />
            <asp:Parameter Name="teste" Type="Boolean" />
            <asp:Parameter Name="teste_noconsolidado" Type="Boolean" />
            <asp:Parameter Name="obs" Type="String" />
            <asp:Parameter Name="dt_criacao" Type="DateTime" />
            <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
            <asp:Parameter Name="user_email" Type="String" />
            <asp:Parameter Name="ativo" Type="Boolean" />
            <asp:Parameter Name="ano" Type="Int16" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="mes" Type="String" />
            <asp:Parameter Name="semana" Type="String" />
            <asp:Parameter Name="dt_monitoramento" Type="DateTime" />
            <asp:Parameter Name="hr_inicio" DbType="Time" />
            <asp:Parameter Name="hr_fim" DbType="Time" />
            <asp:Parameter Name="placa" Type="String" />
            <asp:Parameter Name="go" Type="String" />
            <asp:Parameter Name="emp_transp" Type="String" />
            <asp:Parameter Name="unidade" Type="String" />
            <asp:Parameter Name="cod_projeto" Type="String" />
            <asp:Parameter Name="projeto" Type="String" />
            <asp:Parameter Name="talhao" Type="String" />
            <asp:Parameter Name="genetico" Type="String" />
            <asp:Parameter Name="id_residuo_madeira_modulo" Type="Int16" />
            <asp:Parameter Name="semana_corte" Type="Byte" />
            <asp:Parameter Name="peso_bruto" Type="Double" />
            <asp:Parameter Name="peso_liquido" Type="Double" />
            <asp:Parameter Name="casca_amostra" Type="Double" />
            <asp:Parameter Name="num_tora" Type="Byte" />
            <asp:Parameter Name="casca_presa" Type="Double" />
            <asp:Parameter Name="casca_solta" Type="Double" />
            <asp:Parameter Name="madeira_inapto" Type="Double" />
            <asp:Parameter Name="areia" Type="Double" />
            <asp:Parameter Name="outro" Type="Double" />
            <asp:Parameter Name="descricao" Type="String" />
            <asp:Parameter Name="avaliador" Type="String" />
            <asp:Parameter Name="teste" Type="Boolean" />
            <asp:Parameter Name="teste_noconsolidado" Type="Boolean" />
            <asp:Parameter Name="obs" Type="String" />
            <asp:Parameter Name="dt_criacao" Type="DateTime" />
            <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
            <asp:Parameter Name="user_email" Type="String" />
            <asp:Parameter Name="ativo" Type="Boolean" />
            <asp:Parameter Name="ano" Type="Int16" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
