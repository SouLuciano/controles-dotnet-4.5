﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="modelo_veiculo.aspx.cs" Inherits="webequilibrio.app.cadastro.modelo_veiculo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" HeaderText="Cadastro de modelo de veículo" Theme="Metropolis" Width="100%">
        <PanelCollection>
            <dx:PanelContent runat="server">
                <table class="dxflInternalEditorTable_Moderno">
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvModeloVeiculo" runat="server" AutoGenerateColumns="False" DataSourceID="dsTipoEquipamento" KeyFieldName="nome" OnRowInserting="gvModeloVeiculo_RowInserting" OnRowUpdating="gvModeloVeiculo_RowUpdating" Theme="Metropolis" OnRowInserted="gvModeloVeiculo_RowInserted" OnRowUpdated="gvModeloVeiculo_RowUpdated">
                                <SettingsDetail AllowOnlyOneMasterRowExpanded="True" />
                                <Templates>
                                    <PreviewRow>
                                    </PreviewRow>
                                </Templates>
                                <SettingsEditing Mode="PopupEditForm">
                                </SettingsEditing>
                                <Settings ShowFilterRow="True" ShowFilterRowMenu="True" />
                                <SettingsBehavior ConfirmDelete="True" />
                                <SettingsPopup>
                                    <EditForm HorizontalAlign="Center" />
                                </SettingsPopup>
                                <SettingsSearchPanel Delay="2000" Visible="True" />
                                <Columns>
                                    <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn FieldName="nome" ShowInCustomizationForm="True" VisibleIndex="1" Caption="Modelo">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="tipo" ShowInCustomizationForm="True" VisibleIndex="2" ReadOnly="True" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="dsTipoEquipamento" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                                DeleteCommand="DELETE FROM tipo_equipamento WHERE (nome = @nome) AND (tipo = 'veiculo')" 
                                InsertCommand="INSERT INTO [tipo_equipamento] ([nome], [tipo]) VALUES (@nome, @tipo)" 
                                SelectCommand="SELECT [nome], [tipo] FROM [tipo_equipamento] WHERE ([tipo] = @tipo) ORDER BY [nome]" 
                                UpdateCommand="UPDATE tipo_equipamento SET nome = @nome, tipo = @tipo WHERE (nome = @nome) AND (tipo = 'veiculo')" OnUpdating="dsTipoEquipamento_Updating">
                                <DeleteParameters>
                                    <asp:Parameter Name="nome" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="nome" Type="String" />
                                    <asp:Parameter Name="tipo" Type="String" />
                                </InsertParameters>
                                <SelectParameters>
                                    <asp:Parameter DefaultValue="veiculo" Name="tipo" Type="String" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="nome" />
                                    <asp:Parameter DefaultValue="veiculo" Name="tipo" Type="String" />
                                </UpdateParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>
</asp:Content>
