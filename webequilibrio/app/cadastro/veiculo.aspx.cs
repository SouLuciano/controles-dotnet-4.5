﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Export;
using DevExpress.Web;
using DevExpress.XtraPrinting;
using System.Collections.Generic;
using System.Collections;

namespace webequilibrio.app.cadastro
{
    public partial class veiculo : System.Web.UI.Page
    {
        app.classe.bdados dados = new app.classe.bdados();
        string url;
        string userId;
        app.classe.veiculo veiculos = new classe.veiculo();
        SortedList celularEquipe;
        string email;

        struct updCel
        {
            public int idRegional;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            userId = Session["_userId"].ToString();
            url = HttpContext.Current.Request.Url.AbsolutePath;
            bool acesso = dados.temAcesso(Convert.ToInt32(userId), "~" + url);

            if (!acesso)
            {
                Response.Redirect("~/default.aspx", true);
            }

            using (DataTable dtUser = (DataTable)Session["_user"])
            {
                email = dtUser.Rows[0]["email"].ToString();
            }

        }

        protected void dsEquipe_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            // Seleciona os carros dos projetos que o usuario tem acesso
            //userId = Session["_userId"].ToString();
            //string cmd = string.Format(" SELECT equipe.id, equipe.codigo " +
            //    "FROM equipe, projeto_user, USER " +
            //    $"WHERE equipe.id_projeto = projeto_user.id_projeto AND projeto_user.id_user = {userId}");

            string cmd = string.Format(" SELECT equipe.id, equipe.codigo " +
                "FROM equipe WHERE ativo = 1 ORDER BY equipe.codigo");

            e.Command.CommandText = cmd;

        }

        protected void dsCelular_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            // Seleciona os celulares dos projetos que o usuario tem acesso
            //Não implementado.
            userId = Session["_userId"].ToString();
            string cmd = string.Format("SELECT celular.id, celular.imei1, simcard.numero, celular.status " +
                                        "FROM celular, projeto_user, simcard " +
                                        $"WHERE celular.id_projeto = projeto_user.id_projeto AND simcard.id = celular.id_simcard1 AND projeto_user.id_user = {userId}" +
                                        "AND celular.tipo = 'rastreador'");


        }

        protected void gvVeiculoHist_BeforePerformDataSelect(object sender, EventArgs e)
        {
            Session["id_veiculo"] = (sender as DevExpress.Web.ASPxGridView).GetMasterRowKeyValue();
        }

        protected void gvVeiculo_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
        {

            using (data.dsCadastrosTableAdapters.veiculo_histTableAdapter ta = new data.dsCadastrosTableAdapters.veiculo_histTableAdapter())
            {

                ta.Insert(
                    veiculos.id_veiculo,
                    veiculos.id_projeto,
                    veiculos.placa,
                    veiculos.modelo,
                    veiculos.cartao,
                    veiculos.ativo,
                    veiculos.id_equipe,
                    veiculos.id_celular,
                    veiculos.dt_criacao,
                    veiculos.observacao,
                    veiculos.user_email,
                    veiculos.id_regional,
                    veiculos.dt_contrato_ini,
                    veiculos.dt_contrato_fim,
                    veiculos.alugado,
                    veiculos.combustivel,
                    veiculos.status
                    );
            }

            dados.setLog(Convert.ToInt32(userId), "inf", "atualizou", "veiculos", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }

        protected void gvVeiculo_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            string id_regional = "";

            try
            {
                veiculos.id_veiculo = Convert.ToInt32(e.Keys["id"]);
                veiculos.id_projeto = Convert.ToInt32(e.OldValues["id_projeto"]);
                if (e.OldValues["id_equipe"].ToString() != "") veiculos.id_equipe = Convert.ToInt32(e.OldValues["id_equipe"]);
                if (e.OldValues["id_celular"].ToString() != "") veiculos.id_celular = Convert.ToInt32(e.OldValues["id_celular"]);
                veiculos.ativo = Convert.ToBoolean(e.OldValues["ativo"]);
                veiculos.cartao = (e.OldValues["cartao"].ToString().Trim() == "") ? null : e.OldValues["cartao"].ToString();
                veiculos.dt_criacao = DateTime.Now;
                veiculos.modelo = (e.OldValues["modelo"].ToString().Trim() == "") ? null : e.OldValues["modelo"].ToString();
                veiculos.placa = e.OldValues["placa"].ToString();
                e.NewValues["dt_atualizacao"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                veiculos.observacao = (e.OldValues["observacao"].ToString().Trim() == "") ? null : e.OldValues["observacao"].ToString();
                veiculos.user_email = (e.OldValues["user_email"].ToString().Trim() == "") ? null : e.OldValues["user_email"].ToString();
                veiculos.alugado = (e.OldValues["alugado"].ToString().Trim() == "") ? null : e.OldValues["alugado"].ToString();
                veiculos.combustivel = (e.OldValues["combustivel"].ToString().Trim() == "") ? null : e.OldValues["combustivel"].ToString();
                veiculos.status = (e.OldValues["status"].ToString().Trim() == "") ? null : e.OldValues["status"].ToString();


                if (e.OldValues["dt_contrato_fim"].ToString() != "")
                    veiculos.dt_contrato_fim = Convert.ToDateTime(e.OldValues["dt_contrato_fim"]);

                if (e.OldValues["dt_contrato_ini"].ToString() != "")
                    veiculos.dt_contrato_ini = Convert.ToDateTime(e.OldValues["dt_contrato_ini"]);

                if (e.OldValues["id_regional"].ToString() != "")
                {
                    veiculos.id_regional = Convert.ToInt32(e.OldValues["id_regional"]);
                    id_regional = e.OldValues["id_regional"].ToString();
                };

                e.NewValues["user_email"] = email;

                #region ATUALIZAÇÕES DA REGIONAL E STATUS

                if (e.NewValues["status"].ToString().Contains("Reserva") || e.NewValues["status"].ToString() == "Manutenção")
                {
                    e.NewValues["id_equipe"] = null;

                    if (e.NewValues["status"].ToString().Contains("Reserva"))
                    {
                        int idRegional = dados.getIdRegionalPeloSatatus(e.NewValues["status"].ToString(), userId);
                        if (idRegional != 0)
                        {
                            e.NewValues["id_regional"] = idRegional;
                        }
                    }
                }

                if (e.NewValues["id_equipe"] != null)
                {
                    int id_equipe = Convert.ToInt32(e.NewValues["id_equipe"]);

                    using (DataTable regional = dados.getRegional(id_equipe, userId))
                    {
                        e.NewValues["id_regional"] = regional.Rows[0]["id"].ToString();
                        e.NewValues["status"] = "Em uso";
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                e.Cancel = true;
                dados.setLog(Convert.ToInt32(userId), "err", "gvVeiculo_RowUpdating()", ex.Message, url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            }

            #region Atualizar tbl Cel
            try
            {
                string novoEquipe = (e.NewValues["id_equipe"] == null) ? "" : e.NewValues["id_equipe"].ToString();
                string velhoEquipe = (e.OldValues["id_equipe"] == null) ? "" : e.OldValues["id_equipe"].ToString();

                string novoCel = (e.NewValues["id_celular"] == null) ? "" : e.NewValues["id_celular"].ToString();
                string velhoCel = (e.OldValues["id_celular"] == null) ? "" : e.OldValues["id_celular"].ToString();

                if (!(velhoCel == "" && novoCel == ""))
                {

                    celularEquipe = new SortedList();

                    bool atualizar = true;

                    //Quando altera o cel e a equipe
                    if (velhoCel != novoCel && velhoEquipe != novoEquipe)
                    {
                        if (novoCel != "")
                            atualizar = !dados.checkCelEmVeiculo(Convert.ToInt32(userId), Convert.ToInt32(novoCel));

                        celularEquipe.Add("update", "celularEquipe");
                        celularEquipe.Add("novoCel", novoCel);
                        celularEquipe.Add("velhoCel", velhoCel);

                        celularEquipe.Add("novoEquipe", novoEquipe);
                        celularEquipe.Add("velhoEquipe", velhoEquipe);
                    }

                    //quando altera o celular
                    if (velhoCel != novoCel && velhoEquipe == novoEquipe)
                    {
                        if (novoCel != "")
                            atualizar = !dados.checkCelEmVeiculo(Convert.ToInt32(userId), Convert.ToInt32(novoCel)); //Verifica se o cel já esta cadastrado em veiculo.

                        celularEquipe.Add("update", "celular");
                        celularEquipe.Add("novoCel", novoCel);
                        celularEquipe.Add("velhoCel", velhoCel);
                        celularEquipe.Add("novoEquipe", novoEquipe);
                        celularEquipe.Add("velhoEquipe", velhoEquipe);
                    }

                    //quando altera a equipe
                    if (velhoCel == novoCel && velhoEquipe != novoEquipe)
                    {
                        celularEquipe.Add("update", "equipe");
                        celularEquipe.Add("novoEquipe", novoEquipe);
                        celularEquipe.Add("velhoEquipe", velhoEquipe);
                        celularEquipe.Add("novoCel", novoCel);
                    }


                    if (atualizar && celularEquipe.Count > 0)
                        updateTbCelularEquipe(celularEquipe, id_regional);


                }
            }
            catch (Exception ex)
            {
                e.Cancel = true;
                dados.setLog(Convert.ToInt32(userId), "err", "gvVeiculo_RowUpdating()", "ATUALIZAR TBL CEL: " + ex.Message, url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            }
            #endregion


        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "exportou", "veiculos", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            gvExporter.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });

        }

        void AddError(Dictionary<GridViewColumn, string> errors, GridViewColumn column, string errorText)
        {
            if (errors.ContainsKey(column)) return;
            errors[column] = errorText;
        }

        protected void gvVeiculo_CustomErrorText(object sender, ASPxGridViewCustomErrorTextEventArgs e)
        {
            if (e.ErrorText.Contains("duplicate"))
            {
                if (e.ErrorText.Contains("IX_veiculo_placa") || e.ErrorText.Contains("UNIQUE KEY"))
                    e.ErrorText = "Esta Placa já está cadastrada";


                if (e.ErrorText.Contains("id_celular"))
                    e.ErrorText = "Este rastreador está associado a outro veículo";

                if (e.ErrorText.Contains("PLACA"))
                    e.ErrorText = "Este veículo já está cadastrado";
            }

            if (e.ErrorText.Contains("Este Imei está associado a outro carro"))
                e.ErrorText.ToString();

            if (e.ErrorText.Contains("NULL"))
            {
                if (e.ErrorText.Contains("ativo"))
                    e.ErrorText = "Marque a opção Ativo";
            }


        }

        private void updateTbCelularEquipe(SortedList celularEquipe, string id_regional)
        {
            updCel updCelular = new updCel();
            if (id_regional != "")
                updCelular.idRegional = Convert.ToInt32(id_regional);


            string observacao = $"(mensagem do sistema) Atualizado pelo usuario { veiculos.user_email } no cadastro de veículo";
            try
            {
                using (data.dsCadastrosTableAdapters.celularTableAdapter taCelular = new data.dsCadastrosTableAdapters.celularTableAdapter())
                {
                    string atulizarItem = (string)celularEquipe["update"];

                    string statusCelular = "";


                    if (celularEquipe["velhoEquipe"].ToString() != "")
                        using (DataTable regional = dados.getRegional(Convert.ToInt32(celularEquipe["velhoEquipe"]), userId))
                        {
                            if (regional.Rows[0]["regional"].ToString() == "AR")
                                statusCelular = "Reserva AR";
                            else if (regional.Rows[0]["regional"].ToString() == "SM")
                                statusCelular = "Reserva SM";
                            else if (regional.Rows[0]["regional"].ToString() == "TX")
                                statusCelular = "Reserva TX";
                        }
                    else
                        statusCelular = "";

                    if (atulizarItem == "equipe")
                    {
                        if (celularEquipe["novoEquipe"].ToString() != "" && updCelular.idRegional != 0)
                        {
                            taCelular.UpdateQueryFromVeiculo(observacao, "Em uso", DateTime.Now, veiculos.user_email, Convert.ToInt32(celularEquipe["novoEquipe"]), "rastreador", updCelular.idRegional, Convert.ToInt32(celularEquipe["novoCel"]));
                        }
                        else
                        {
                            if (updCelular.idRegional == 0)
                                taCelular.UpdateQueryFromVeiculo(observacao, "Em uso", DateTime.Now, veiculos.user_email, null, "rastreador", null, Convert.ToInt32(celularEquipe["novoCel"]));
                            else
                                taCelular.UpdateQueryFromVeiculo(observacao, "Em uso", DateTime.Now, veiculos.user_email, null, "rastreador", updCelular.idRegional, Convert.ToInt32(celularEquipe["novoCel"]));
                        }

                    }

                    if (atulizarItem == "celular" || atulizarItem == "celularEquipe")
                    {
                        if (celularEquipe["velhoCel"].ToString() != "")
                        {
                            if (updCelular.idRegional == 0)
                                taCelular.UpdateQueryFromVeiculo(observacao, statusCelular, DateTime.Now, veiculos.user_email, null, "rastreador", null, Convert.ToInt32(celularEquipe["velhoCel"]));
                            else
                                taCelular.UpdateQueryFromVeiculo(observacao, statusCelular, DateTime.Now, veiculos.user_email, null, "rastreador", updCelular.idRegional, Convert.ToInt32(celularEquipe["velhoCel"]));
                        }
                        if (celularEquipe["novoCel"].ToString() != "")
                        {
                            if (celularEquipe["novoEquipe"].ToString() != "")
                            {
                                if (updCelular.idRegional == 0)
                                    taCelular.UpdateQueryFromVeiculo(observacao, "Em uso", DateTime.Now, veiculos.user_email, Convert.ToInt32(celularEquipe["novoEquipe"]), "rastreador", null, Convert.ToInt32(celularEquipe["novoCel"]));
                                else
                                    taCelular.UpdateQueryFromVeiculo(observacao, "Em uso", DateTime.Now, veiculos.user_email, Convert.ToInt32(celularEquipe["novoEquipe"]), "rastreador", updCelular.idRegional, Convert.ToInt32(celularEquipe["novoCel"]));
                            }
                            else
                            {
                                if (updCelular.idRegional == 0)
                                    taCelular.UpdateQueryFromVeiculo(observacao, "Em uso", DateTime.Now, veiculos.user_email, null, "rastreador", null, Convert.ToInt32(celularEquipe["novoCel"]));
                                else
                                    taCelular.UpdateQueryFromVeiculo(observacao, "Em uso", DateTime.Now, veiculos.user_email, null, "rastreador", updCelular.idRegional, Convert.ToInt32(celularEquipe["novoCel"]));
                            }
                        }

                        if (atulizarItem == "equipe")
                        {
                            if (celularEquipe["novoEquipe"].ToString() != "")
                            {
                                if (updCelular.idRegional == 0)
                                    taCelular.UpdateQueryFromVeiculo(observacao, "Em uso", DateTime.Now, veiculos.user_email, Convert.ToInt32(celularEquipe["novoEquipe"]), "rastreador", null, Convert.ToInt32(celularEquipe["novoCel"]));
                                else
                                    taCelular.UpdateQueryFromVeiculo(observacao, "Em uso", DateTime.Now, veiculos.user_email, Convert.ToInt32(celularEquipe["novoEquipe"]), "rastreador", updCelular.idRegional, Convert.ToInt32(celularEquipe["novoCel"]));
                            }

                            else
                            {
                                if (updCelular.idRegional == 0)
                                    taCelular.UpdateQueryFromVeiculo(observacao, statusCelular, DateTime.Now, veiculos.user_email, null, "rastreador", null, Convert.ToInt32(celularEquipe["novoCel"]));
                                else
                                    taCelular.UpdateQueryFromVeiculo(observacao, statusCelular, DateTime.Now, veiculos.user_email, null, "rastreador", updCelular.idRegional, Convert.ToInt32(celularEquipe["novoCel"]));
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                dados.setLog(Convert.ToInt32(userId), "err", "updateTbCelularEquipe()", ex.Message, url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            }
        }

        protected void gvVeiculo_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            string id_regional = "";
            e.NewValues["dt_atualizacao"] = DateTime.Now;
            e.NewValues["user_email"] = email;
            veiculos.user_email = email;

            if (e.NewValues["id_equipe"] != null)
            {
                int id_equipe = Convert.ToInt32(e.NewValues["id_equipe"]);

                using (DataTable regional = dados.getRegional(id_equipe, userId))
                {
                    id_regional = regional.Rows[0]["id"].ToString();
                    e.NewValues["id_regional"] = regional.Rows[0]["id"];
                }
            }

            #region Atualizar tbl Cel
            try
            {
                string novoEquipe = (e.NewValues["id_equipe"] == null) ? "" : e.NewValues["id_equipe"].ToString();
                string novoCel = (e.NewValues["id_celular"] == null) ? "" : e.NewValues["id_celular"].ToString();

                if (novoCel != "")
                {
                    celularEquipe = new SortedList();
                    bool atualizar = true;
                    celularEquipe["update"] = "celular";
                    celularEquipe.Add("velhoCel", "");
                    celularEquipe.Add("velhoEquipe", "");

                    if (novoEquipe != "")
                    {
                        celularEquipe.Add("novoCel", novoCel);
                        celularEquipe.Add("novoEquipe", novoEquipe);
                    }
                    else
                    {
                        celularEquipe.Add("novoCel", novoCel);
                        celularEquipe.Add("novoEquipe", "");
                    }

                    if (atualizar && celularEquipe.Count > 0)
                        updateTbCelularEquipe(celularEquipe, id_regional);
                }

                dados.setLog(Convert.ToInt32(userId), "inf", "criou", "veiculo", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            }
            catch (Exception ex)
            {
                e.Cancel = true;
                dados.setLog(Convert.ToInt32(userId), "err", "gvVeiculo_RowInserting()", "ATUALIZAR TBL CEL: " + ex.Message, url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            }
            #endregion
        }

        protected void gvVeiculo_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {


            foreach (GridViewColumn column in gvVeiculo.Columns)
            {
                GridViewDataColumn dataColumn = column as GridViewDataColumn;
                if (dataColumn == null) continue;

                if (e.NewValues["id_projeto"] == null)
                {
                    AddError(e.Errors, gvVeiculo.Columns["id_projeto"], "Não pode ser em branco");
                }

                if (dataColumn.FieldName == "placa")
                    if (e.NewValues["placa"] != null)
                    {
                        if (e.Keys["id"] != null)
                        {
                            if (dados.checkDadoRepetido("placa", e.NewValues["placa"].ToString(), "veiculo", userId, e.Keys["id"].ToString(), true, true))
                                AddError(e.Errors, gvVeiculo.Columns["placa"], "Este Núm. de série já está cadastrado");
                        }
                        else
                        {
                            if (dados.checkDadoRepetido("placa", e.NewValues["placa"].ToString(), "veiculo", userId, "", true, true))
                                AddError(e.Errors, gvVeiculo.Columns["placa"], "Este Núm. de série já está cadastrado");
                        }
                    }

                if (e.NewValues["ativo"] == null)
                {
                    AddError(e.Errors, gvVeiculo.Columns["ativo"], "Ativo não pode ser em branco");
                }

                if (e.NewValues["status"] == null)
                {
                    AddError(e.Errors, gvVeiculo.Columns["status"], "Status não pode ser em branco");
                }

                if (dataColumn.FieldName == "id_celular")
                    if (e.NewValues["id_celular"] != null)
                    {
                        if (e.Keys["id"] != null)
                        {
                            if (dados.checkDadoRepetido("id_celular", e.NewValues["id_celular"].ToString(), "veiculo", userId, e.Keys["id"].ToString()))
                                AddError(e.Errors, gvVeiculo.Columns["id_celular"], "Este Imei está associado a outro carro");
                        }
                        else
                        {
                            if (dados.checkDadoRepetido("id_celular", e.NewValues["id_celular"].ToString(), "veiculo", userId, ""))
                                AddError(e.Errors, gvVeiculo.Columns["id_celular"], "Este Imei está associado a outro carro");
                        }
                    }

            }

            if (string.IsNullOrEmpty(e.RowError) && e.Errors.Count > 0) e.RowError = "Corriga todos os erros.";
        }
    }
}