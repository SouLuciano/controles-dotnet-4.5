﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace webequilibrio.app.cadastro
{
    public partial class equipe_veiculo : System.Web.UI.Page
    {
        app.classe.bdados dados = new app.classe.bdados();
        string url;
        protected void Page_Load(object sender, EventArgs e)
        {
            url = HttpContext.Current.Request.Url.AbsolutePath;
            bool acesso = dados.temAcesso(Convert.ToInt32(Session["_userId"]), "~" + url);

            if (!acesso)
            {
                Response.Redirect("~/default.aspx", true);
            }
        }

        protected void gvEquipeVeiculo_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            e.NewValues["id_projeto"] = cbProjeto.Value;
        }
    }
}