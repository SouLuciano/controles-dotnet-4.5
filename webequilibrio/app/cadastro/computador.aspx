﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="computador.aspx.cs" Inherits="webequilibrio.app.cadastro.computador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" HeaderText="Cadastro de computadores" Theme="Metropolis" Width="100%">
        <PanelCollection>
            <dx:PanelContent runat="server">
                <table style="width: 100%">
                    <tr>
                        <td>
                            <table style="width: 400px">
                                <tr>
                                    <td><span style="color: rgb(64, 64, 64); font-family: Roboto, 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: center; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none; background-color: rgb(255, 255, 255)">&nbsp;<asp:Label ID="Label2" runat="server" Text="Mostrar itens"></asp:Label>
                                        </span></td>
                                    <td>
                                        <dx:ASPxComboBox ID="cbxAtivo" runat="server" AutoPostBack="True" SelectedIndex="0" Theme="Metropolis">
                                            <Items>
                                                <dx:ListEditItem Selected="True" Text="Ativos" Value="True" />
                                                <dx:ListEditItem Text="Inativos" Value="False" />
                                            </Items>
                                        </dx:ASPxComboBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvComputador" runat="server" AutoGenerateColumns="False" DataSourceID="dsComputador" KeyFieldName="id" Theme="Metropolis" OnCustomErrorText="gvComputador_CustomErrorText" OnRowInserted="gvComputador_RowInserted" OnRowInserting="gvComputador_RowInserting" OnRowUpdated="gvComputador_RowUpdated" OnRowUpdating="gvComputador_RowUpdating" OnRowValidating="gvComputador_RowValidating">
                                <SettingsDetail AllowOnlyOneMasterRowExpanded="True" ShowDetailRow="True" />
                                <Templates>
                                    <DetailRow>
                                        <dx:ASPxGridView ID="gvComputadorHist" runat="server" AutoGenerateColumns="False" DataSourceID="dsComputadorFilho" EnableTheming="True" KeyFieldName="id" OnBeforePerformDataSelect="gvComputadorHist_BeforePerformDataSelect" Theme="Metropolis">
                                            <SettingsPager PageSize="4">
                                            </SettingsPager>
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="26">
                                                    <EditFormSettings Visible="False" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="nome" VisibleIndex="4" Caption="Nome">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="teamviewer" VisibleIndex="5" Caption="TeamViewer">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="tipo" VisibleIndex="6" Caption="Tipo">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="marca" VisibleIndex="7" Caption="Marca">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="modelo" VisibleIndex="8" Caption="Modelo">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="etiqueta" VisibleIndex="9" Caption="Etiqueta">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="os" VisibleIndex="10" Caption="Sistema">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="licenca_win" VisibleIndex="11" Caption="Licença Win">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="office" VisibleIndex="12" Caption="Office">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="licenca_off" VisibleIndex="13" Caption="Licença office">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="ip" VisibleIndex="14" Caption="Endereço IP">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="cpu" VisibleIndex="15">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="ram" VisibleIndex="16">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="ram_modelo" VisibleIndex="17">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="hd" VisibleIndex="18">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="mac" VisibleIndex="19" Caption="Endereço MAC">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="observacao" VisibleIndex="22">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataCheckColumn FieldName="ativo" VisibleIndex="23">
                                                </dx:GridViewDataCheckColumn>
                                                <dx:GridViewDataDateColumn FieldName="dt_atualizacao" VisibleIndex="24" Caption="Dt Hota atualização">
                                                    <PropertiesDateEdit DisplayFormatString="g">
                                                    </PropertiesDateEdit>
                                                    <EditFormSettings Visible="True" />
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn FieldName="user_email" VisibleIndex="25" Caption="Cadastrado Alterado pelo(a)">
                                                    <EditFormSettings Visible="False" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataComboBoxColumn Caption="Projeto" FieldName="id_projeto" VisibleIndex="0">
                                                    <PropertiesComboBox DataSourceID="dsProjeto" TextField="nome" ValueField="id">
                                                    </PropertiesComboBox>
                                                </dx:GridViewDataComboBoxColumn>
                                                <dx:GridViewDataComboBoxColumn Caption="Regional" FieldName="id_regional" VisibleIndex="1">
                                                    <PropertiesComboBox DataSourceID="dsRegional" TextField="regional" ValueField="id">
                                                    </PropertiesComboBox>
                                                </dx:GridViewDataComboBoxColumn>
                                                <dx:GridViewDataComboBoxColumn Caption="Equipe" FieldName="id_equipe" VisibleIndex="2">
                                                    <PropertiesComboBox DataSourceID="dsEquipe" TextField="codigo" ValueField="id">
                                                    </PropertiesComboBox>
                                                </dx:GridViewDataComboBoxColumn>
                                                <dx:GridViewDataComboBoxColumn Caption="Responsável" FieldName="id_funcionario" VisibleIndex="3">
                                                    <PropertiesComboBox DataSourceID="dsFuncionario" TextField="nome" ValueField="id">
                                                    </PropertiesComboBox>
                                                </dx:GridViewDataComboBoxColumn>
                                                <dx:GridViewDataTextColumn FieldName="status" VisibleIndex="21">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                    </DetailRow>
                                </Templates>
                                <SettingsEditing Mode="PopupEditForm">
                                </SettingsEditing>
                                <Settings ShowFilterRow="True" ShowFilterRowMenu="True" />
                                <SettingsBehavior AutoFilterRowInputDelay="1800" />
                                <SettingsDataSecurity AllowDelete="False" />
                                <SettingsPopup>
                                    <EditForm HorizontalAlign="Center" />
                                </SettingsPopup>
                                <SettingsSearchPanel Delay="2000" Visible="True" />
                                <Columns>
                                    <dx:GridViewCommandColumn ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="26">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="nome" ShowInCustomizationForm="True" VisibleIndex="5" Caption="Nome EPF">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="teamviewer" ShowInCustomizationForm="True" VisibleIndex="6" Caption="TeamViewer">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="marca" ShowInCustomizationForm="True" VisibleIndex="8" Caption="Marca">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="modelo" ShowInCustomizationForm="True" VisibleIndex="9" Caption="Modelo">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="etiqueta" ShowInCustomizationForm="True" VisibleIndex="10" Caption="Etiqueta">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="os" ShowInCustomizationForm="True" VisibleIndex="11" Caption="Sistema">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="licenca_off" ShowInCustomizationForm="True" VisibleIndex="14" Caption="Licença office">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="cpu" ShowInCustomizationForm="True" VisibleIndex="16">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="ram" ShowInCustomizationForm="True" VisibleIndex="17">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="ram_modelo" ShowInCustomizationForm="True" VisibleIndex="18">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="hd" ShowInCustomizationForm="True" VisibleIndex="19">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="mac" ShowInCustomizationForm="True" VisibleIndex="20" Caption="Endereço MAC">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="observacao" ShowInCustomizationForm="True" VisibleIndex="22">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataCheckColumn FieldName="ativo" ShowInCustomizationForm="True" VisibleIndex="23">
                                    </dx:GridViewDataCheckColumn>
                                    <dx:GridViewDataDateColumn FieldName="dt_atualizacao" ShowInCustomizationForm="True" VisibleIndex="24" ReadOnly="True" Caption="Dt. hora ultima atualização">
                                        <PropertiesDateEdit DisplayFormatString="g">
                                        </PropertiesDateEdit>
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Projeto" FieldName="id_projeto" ShowInCustomizationForm="True" VisibleIndex="1">
                                        <PropertiesComboBox DataSourceID="dsProjeto" TextField="nome" ValueField="id">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Regional" FieldName="id_regional" ShowInCustomizationForm="True" VisibleIndex="2">
                                        <PropertiesComboBox DataSourceID="dsRegional" TextField="regional" ValueField="id">
                                        </PropertiesComboBox>
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Equipe" FieldName="id_equipe" ShowInCustomizationForm="True" VisibleIndex="3">
                                        <PropertiesComboBox DataSourceID="dsEquipe" TextField="codigo" ValueField="id" AllowNull="True">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Responsável" FieldName="id_funcionario" ShowInCustomizationForm="True" VisibleIndex="4">
                                        <PropertiesComboBox DataSourceID="dsFuncionario" TextField="nome" ValueField="id" AllowNull="True">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Status" FieldName="status" ShowInCustomizationForm="True" VisibleIndex="21">
                                        <PropertiesComboBox DataSourceID="dsStatusEquipamento" TextField="condicao" ValueField="condicao">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataTextColumn Caption="Cadastrado/Alterado pelo(a)" FieldName="user_email" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="25">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Tipo" FieldName="tipo" ShowInCustomizationForm="True" VisibleIndex="7">
                                        <PropertiesComboBox>
                                            <Items>
                                                <dx:ListEditItem Text="NOT" Value="NOT" />
                                                <dx:ListEditItem Text="DSK" Value="DSK" />
                                            </Items>
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Licença Win" FieldName="licenca_win" ShowInCustomizationForm="True" VisibleIndex="12">
                                        <PropertiesComboBox>
                                            <Items>
                                                <dx:ListEditItem Text="EPF" Value="EPF" />
                                                <dx:ListEditItem Text="OEM" Value="OEM" />
                                            </Items>
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataTextColumn Caption="Office" FieldName="office" ShowInCustomizationForm="True" VisibleIndex="13">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Endereço IP" FieldName="ip" ShowInCustomizationForm="True" VisibleIndex="15">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" Text="Exportar para excel" Theme="Metropolis">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="dsComputador" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [id_projeto], [id_regional], [id_equipe], [id_funcionario], [nome], [teamviewer], [tipo], [marca], [modelo], [etiqueta], [os], [licenca_win], [office], [licenca_off], [ip], [cpu], [ram], [ram_modelo], [hd], [mac], [status], [observacao], [ativo], [dt_atualizacao], [user_email] FROM [computador] WHERE (([ativo] = @ativo) AND ([id_pai] IS NULL))" DeleteCommand="DELETE FROM [computador] WHERE [id] = @id" InsertCommand="INSERT INTO [computador] ([id_projeto], [id_regional], [id_equipe], [id_funcionario], [nome], [teamviewer], [tipo], [marca], [modelo], [etiqueta], [os], [licenca_win], [office], [licenca_off], [ip], [cpu], [ram], [ram_modelo], [hd], [mac], [status], [observacao], [ativo], [dt_atualizacao], [user_email]) VALUES (@id_projeto, @id_regional, @id_equipe, @id_funcionario, @nome, @teamviewer, @tipo, @marca, @modelo, @etiqueta, @os, @licenca_win, @office, @licenca_off, @ip, @cpu, @ram, @ram_modelo, @hd, @mac, @status, @observacao, @ativo, @dt_atualizacao, @user_email)" UpdateCommand="UPDATE [computador] SET [id_projeto] = @id_projeto, [id_regional] = @id_regional, [id_equipe] = @id_equipe, [id_funcionario] = @id_funcionario, [nome] = @nome, [teamviewer] = @teamviewer, [tipo] = @tipo, [marca] = @marca, [modelo] = @modelo, [etiqueta] = @etiqueta, [os] = @os, [licenca_win] = @licenca_win, [office] = @office, [licenca_off] = @licenca_off, [ip] = @ip, [cpu] = @cpu, [ram] = @ram, [ram_modelo] = @ram_modelo, [hd] = @hd, [mac] = @mac, [status] = @status, [observacao] = @observacao, [ativo] = @ativo, [dt_atualizacao] = @dt_atualizacao, [user_email] = @user_email WHERE [id] = @id">
                    <DeleteParameters>
                        <asp:Parameter Name="id" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="id_projeto" Type="Int32" />
                        <asp:Parameter Name="id_regional" Type="Int32" />
                        <asp:Parameter Name="id_equipe" Type="Int32" />
                        <asp:Parameter Name="id_funcionario" Type="Int32" />
                        <asp:Parameter Name="nome" Type="String" />
                        <asp:Parameter Name="teamviewer" Type="String" />
                        <asp:Parameter Name="tipo" Type="String" />
                        <asp:Parameter Name="marca" Type="String" />
                        <asp:Parameter Name="modelo" Type="String" />
                        <asp:Parameter Name="etiqueta" Type="String" />
                        <asp:Parameter Name="os" Type="String" />
                        <asp:Parameter Name="licenca_win" Type="String" />
                        <asp:Parameter Name="office" Type="String" />
                        <asp:Parameter Name="licenca_off" Type="String" />
                        <asp:Parameter Name="ip" Type="String" />
                        <asp:Parameter Name="cpu" Type="String" />
                        <asp:Parameter Name="ram" Type="String" />
                        <asp:Parameter Name="ram_modelo" Type="String" />
                        <asp:Parameter Name="hd" Type="String" />
                        <asp:Parameter Name="mac" Type="String" />
                        <asp:Parameter Name="status" Type="String" />
                        <asp:Parameter Name="observacao" Type="String" />
                        <asp:Parameter Name="ativo" Type="Boolean" />
                        <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                        <asp:Parameter Name="user_email" Type="String" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="cbxAtivo" DefaultValue="True" Name="ativo" PropertyName="Value" Type="Boolean" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="id_projeto" Type="Int32" />
                        <asp:Parameter Name="id_regional" Type="Int32" />
                        <asp:Parameter Name="id_equipe" Type="Int32" />
                        <asp:Parameter Name="id_funcionario" Type="Int32" />
                        <asp:Parameter Name="nome" Type="String" />
                        <asp:Parameter Name="teamviewer" Type="String" />
                        <asp:Parameter Name="tipo" Type="String" />
                        <asp:Parameter Name="marca" Type="String" />
                        <asp:Parameter Name="modelo" Type="String" />
                        <asp:Parameter Name="etiqueta" Type="String" />
                        <asp:Parameter Name="os" Type="String" />
                        <asp:Parameter Name="licenca_win" Type="String" />
                        <asp:Parameter Name="office" Type="String" />
                        <asp:Parameter Name="licenca_off" Type="String" />
                        <asp:Parameter Name="ip" Type="String" />
                        <asp:Parameter Name="cpu" Type="String" />
                        <asp:Parameter Name="ram" Type="String" />
                        <asp:Parameter Name="ram_modelo" Type="String" />
                        <asp:Parameter Name="hd" Type="String" />
                        <asp:Parameter Name="mac" Type="String" />
                        <asp:Parameter Name="status" Type="String" />
                        <asp:Parameter Name="observacao" Type="String" />
                        <asp:Parameter Name="ativo" Type="Boolean" />
                        <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                        <asp:Parameter Name="user_email" Type="String" />
                        <asp:Parameter Name="id" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsComputadorFilho" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [id_projeto], [id_regional], [id_equipe], [id_funcionario], [nome], [teamviewer], [tipo], [marca], [modelo], [etiqueta], [os], [licenca_win], [office], [licenca_off], [ip], [cpu], [ram], [ram_modelo], [hd], [mac], [status], [observacao], [ativo], [dt_atualizacao], [user_email], [id_pai] FROM [computador] WHERE (([id_pai] IS NOT NULL) AND ([id_pai] = @id_pai)) ORDER BY [id] DESC">
                    <SelectParameters>
                        <asp:SessionParameter Name="id_pai" SessionField="id_pai" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsProjeto" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [nome], [id] FROM [projeto] WHERE ([ativo] = @ativo) ORDER BY [nome]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsEquipe" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [codigo] FROM [equipe] WHERE ([ativo] = @ativo) ORDER BY [codigo]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsRegional" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [regional] FROM [regional] WHERE ([ativo] = @ativo) ORDER BY [regional]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsFuncionario" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [nome] FROM [funcionario] WHERE ([ativo] = @ativo) ORDER BY [nome]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsStatusEquipamento" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [condicao] FROM [status_equipamento] WHERE ([tipo_equipamento] = @tipo_equipamento) ORDER BY [condicao]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="computador" Name="tipo_equipamento" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <dx:ASPxGridViewExporter ID="gvExporter" runat="server" ExportedRowType="All" FileName="Computadores" GridViewID="gvComputador" PaperKind="A4">
                </dx:ASPxGridViewExporter>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>
</asp:Content>
