﻿using System;

namespace webequilibrio.app.classe
{
    public class computador
    {
        public int id { get; set; }
        public int id_projeto { get; set; }
        public int id_regional { get; set; }
        public int id_equipe { get; set; }
        public int id_funcionario { get; set; }
        public int MyProperty { get; set; }
        public int id_pai { get; set; }
        public string nome { get; set; }
        public string teamviewer { get; set; }
        public string tipo { get; set; }
        public string marca { get; set; }
        public string modelo { get; set; }
        public string etiqueta { get; set; }
        public string os { get; set; }
        public string licenca_win { get; set; }
        public string office { get; set; }
        public string licenca_off { get; set; }
        public string ip { get; set; }
        public string cpu { get; set; }
        public string ram { get; set; }
        public string ram_modelo { get; set; }
        public string hd { get; set; }
        public string mac { get; set; }
        public string status { get; set; }
        public string observacao { get; set; }
        public bool ativo { get; set; }
        public DateTime dt_atualizacao { get; set; }
        public string user_email { get; set; }
    }
}