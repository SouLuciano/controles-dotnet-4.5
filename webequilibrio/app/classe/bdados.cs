﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace webequilibrio.app.classe
{
    public class bdados
    {

        public static string GetCnx()
        {
            string cnx = ConfigurationManager.ConnectionStrings["equilibrioConnectionString"].ConnectionString;
            return cnx;
        }

        public string getTipoDeAcessoQualidadeInterna(string userId)
        {
            DataTable s_dt = new DataTable();
            string s_SQL = $@"Select nome_filtro from qual_verif_monitora_acesso Where id_user = {userId}";
            string tipoAcesso = "";

            try
            {
                using (SqlConnection cnx = new SqlConnection(GetCnx()))
                {
                    cnx.Open();

                    using (SqlCommand cmd = new SqlCommand(s_SQL, cnx))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                            s_dt.Load(reader);
                    }

                    cnx.Close();

                    tipoAcesso = s_dt.Rows[0]["nome_filtro"].ToString();
                }
            }
            catch (Exception ex)
            {
                setLog(0, "err", "getTipoDeAcessoQualidadeInterna()",  ex.Message, "bdados.cs");
            }
            finally
            {
                s_dt.Dispose();
            }

            return tipoAcesso;
        }

        public DataTable getDadosUserByIdOrEmail(string emailOuId)
        {
            DataTable s_dt = new DataTable();
            string s_SQL = @"SELECT [user].id, [user].ativo, funcionario.nome, email, senha, funcionario.id_regional, funcionario.id_regional 
                             FROM dbo.[user], funcionario 
                             WHERE  [user].idFuncionario = funcionario.id AND ";

            if (auxiliar.ehNumero32(emailOuId))
                s_SQL = s_SQL + $@" [user].id={emailOuId} ";
            else
                s_SQL = s_SQL + $@" email='{emailOuId}' ";

            try
            {
                using (SqlConnection cnx = new SqlConnection(GetCnx()))
                {
                    cnx.Open();

                    using (SqlCommand cmd = new SqlCommand(s_SQL, cnx))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                            s_dt.Load(reader);
                    }

                    cnx.Close();
                }
            }
            catch (Exception ex)
            {
                //TODO Checar id usuario
                //setLog(0, "err", "getDadosUserByIdOrEmail", ex.Message, "bdados.cs");
            }

            return s_dt;
        }

        public bool checkLogin(string email, string senha)
        {
            string sqlUser = $"SELECT email, senha FROM [user] WHERE(email = '{email}') AND(senha = '{senha}')";

            bool login = false;

            try
            {
                using (SqlConnection cnx = new SqlConnection(GetCnx()))
                {
                    cnx.Open();

                    using (SqlCommand cmd = new SqlCommand(sqlUser, cnx))
                    {
                        //cmd.Parameters.AddWithValue("@p1", email);
                        //cmd.Parameters.AddWithValue("@p2", senha);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                            if (reader.HasRows)
                                login = true;
                    }

                    cnx.Close();
                }
            }
            catch (Exception ex)
            {
                setLog(0, "err", "checkLogin", "email: " + email + " " + ex.Message, "bdados.cs");
            }

            return login;
        }

        public void setLog(long id_user, string tipo, string acao, string descricao = null, string url = null, string ip = null, string os = null, string browser = null, string browser_ver = null)
        {
            DataTable s_dt = new DataTable();

            const string s_SQL = "INSERT INTO LOG(id_user,tipo,acao,descricao,url,ip,os,browser,browser_ver) VALUES(@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8,@p9)";

            try
            {
                using (SqlConnection cnx = new SqlConnection(GetCnx()))
                {
                    cnx.Open();

                    using (SqlCommand cmd = new SqlCommand(s_SQL, cnx))
                    {
                        cmd.Parameters.AddWithValue("@p1", id_user);
                        cmd.Parameters.AddWithValue("@p2", tipo);
                        cmd.Parameters.AddWithValue("@p3", acao);

                        if (descricao == null)
                            cmd.Parameters.AddWithValue("@p4", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@p4", descricao);

                        if (url == null)
                            cmd.Parameters.AddWithValue("@p5", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@p5", url);

                        if (ip == null)
                            cmd.Parameters.AddWithValue("@p6", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@p6", ip);

                        if (os == null)
                            cmd.Parameters.AddWithValue("@p7", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@p7", os);

                        if (browser == null)
                            cmd.Parameters.AddWithValue("@p8", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@p8", browser);

                        if (browser_ver == null)
                            cmd.Parameters.AddWithValue("@p9", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@p9", browser_ver);

                        cmd.CommandText = s_SQL;
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }

                    cnx.Close();
                }
            }
            catch (Exception ex)
            {
                // TODO: Enviar email
            }

        }

        public DataTable getDadosNbLateral(int id_user)
        {
            DataTable s_dt = new DataTable();

            // const string s_SQL = "SELECT id, id_user, grupo, nomePagina, url, editar FROM permissao WHERE id_user=@p1";

            const string s_SQL = "SELECT grupo.nome AS 'grupo', url.nome as 'nomePagina', url.url " +
                                 "FROM [grupo], url, grupo_user, grupo_url " +
                                 "WHERE grupo.id = grupo_user.id_grupo " +
                                 "AND url.id = grupo_url.id_url " +
                                 "AND grupo_url.id_grupo = grupo_user.id_grupo " +
                                 "AND grupo_user.id_user = @p1";

            try
            {
                using (SqlConnection cnx = new SqlConnection(GetCnx()))
                {
                    cnx.Open();

                    using (SqlCommand cmd = new SqlCommand(s_SQL, cnx))
                    {
                        cmd.Parameters.AddWithValue("@p1", id_user);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                            s_dt.Load(reader);
                    }

                    cnx.Close();
                }
            }
            catch (Exception ex)
            {
                //TODO: Checar id usuario
                setLog(0, "err", "getDadosNbLateral", ex.Message, "bdados.cs");
            }

            return s_dt;
        }

        public bool temAcesso(int id_user, string url)
        {
            url = url.Replace("/cde", "");

            // setLog(0, "err", "temAcesso", "URL: " + url, "bdados.cs");

            DataTable s_dt = new DataTable();
            const string s_SQL = "SELECT COUNT(*) " +
                                 "FROM [grupo], url, grupo_user, grupo_url " +
                                 "WHERE grupo.id = grupo_user.id_grupo " +
                                 "AND url.id = grupo_url.id_url " +
                                 "AND grupo_url.id_grupo = grupo_user.id_grupo " +
                                 "AND grupo_user.id_user = @p1 AND url.url = @p2";

            try
            {
                using (SqlConnection cnx = new SqlConnection(GetCnx()))
                {
                    cnx.Open();

                    using (SqlCommand cmd = new SqlCommand(s_SQL, cnx))
                    {
                        cmd.Parameters.AddWithValue("@p1", id_user);
                        cmd.Parameters.AddWithValue("@p2", url);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                            s_dt.Load(reader);
                    }

                    cnx.Close();
                }
            }
            catch (Exception ex)
            {
                //TODO: Checar id usuario
                setLog(0, "err", "temAcesso", ex.Message, "bdados.cs");
            }

            if (s_dt.Rows[0][0].ToString() == "0")
                return false;
            else
                return true;
        }

        public bool checkCelEmVeiculo(int id_user, int idCelular)
        {
            const string sqlUser = "SELECT id_celular FROM [veiculo] WHERE id_celular = @p1";

            bool resposta = false;

            try
            {
                using (SqlConnection cnx = new SqlConnection(GetCnx()))
                {
                    cnx.Open();

                    using (SqlCommand cmd = new SqlCommand(sqlUser, cnx))
                    {
                        cmd.Parameters.AddWithValue("@p1", idCelular);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                            if (reader.HasRows)
                                resposta = true;
                    }

                    cnx.Close();
                }
            }
            catch (Exception ex)
            {
                setLog(id_user, "err", "checkCelEmVeiculo()", ex.Message, "bdados.cs");
            }

            return resposta;
        }

        public bool checkDadoRepetido(string coluna, string valor, string tabela, string id_user, string ignoreIndex, bool strTirarEspacoValor = false, bool valorEhString = false, bool TemColIdPai = false)
        {
            if (strTirarEspacoValor)
                valor = valor.ToLower().Trim();

            if (valorEhString)
                valor = "'" + valor + "'";

            int iduser = Convert.ToInt32(id_user);
            string select = "";
            if (ignoreIndex != "")
                select = $"SELECT id FROM [{tabela}] WHERE NOT id = {ignoreIndex} AND {coluna} = {valor}";
            else
                select = $"SELECT id FROM [{tabela}] WHERE {coluna} = {valor}";

            if (TemColIdPai)
                select = select + " AND id_pai IS NULL";

            bool resposta = false;

            try
            {
                using (SqlConnection cnx = new SqlConnection(GetCnx()))
                {
                    cnx.Open();

                    using (SqlCommand cmd = new SqlCommand(select, cnx))
                    {

                        using (SqlDataReader reader = cmd.ExecuteReader())
                            if (reader.HasRows)
                                resposta = true;
                    }

                    cnx.Close();
                }
            }
            catch (Exception ex)
            {
                setLog(iduser, "err", "checkDadoRepetido()", ex.Message, "bdados.cs");
            }

            return resposta;
        }

        public void setHistorico(int fk_id, string tabela, string objAlterado, string valorAnterior, long idUser, DateTime dtAlteracao)
        {
            DataTable s_dt = new DataTable();

            const string s_SQL = "INSERT INTO historico(fk_id,tabela,objAlterado,valorAnterior,idUser,dtAlteracao) VALUES(@p1,@p2,@p3,@p4,@p5,@p6)";

            try
            {
                using (SqlConnection cnx = new SqlConnection(GetCnx()))
                {
                    cnx.Open();

                    using (SqlCommand cmd = new SqlCommand(s_SQL, cnx))
                    {
                        cmd.Parameters.AddWithValue("@p1", fk_id);
                        cmd.Parameters.AddWithValue("@p2", tabela);
                        cmd.Parameters.AddWithValue("@p3", objAlterado);
                        cmd.Parameters.AddWithValue("@p4", valorAnterior);
                        cmd.Parameters.AddWithValue("@p5", idUser);
                        cmd.Parameters.AddWithValue("@p6", dtAlteracao);
                        cmd.CommandText = s_SQL;
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }

                    cnx.Close();
                }
            }
            catch (Exception ex)
            {
                // TODO: tratar exeção
            }
        }

        public DataTable getRegional(int id_equipe, string id_user)
        {
            DataTable s_dt = new DataTable();
            int iduser = Convert.ToInt32(id_user);

            string s_SQL = "SELECT regional.id, regional.regional " +
                                "FROM regional, equipe " +
                                "WHERE equipe.id_regional = regional.id AND equipe.id = " + id_equipe;

            try
            {
                using (SqlConnection cnx = new SqlConnection(GetCnx()))
                {
                    cnx.Open();

                    using (SqlCommand cmd = new SqlCommand(s_SQL, cnx))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                            s_dt.Load(reader);
                    }

                    cnx.Close();
                }
            }
            catch (Exception ex)
            {
                setLog(iduser, "err", "getRegional", ex.Message, "bdados.cs");
            }

            return s_dt;
        }

        public int getIdRegionalPeloSatatus(string statusReserva, string id_user)
        {
            DataTable s_dt = new DataTable();
            int iduser = Convert.ToInt32(id_user);
            int idRegional = 0;

            string s_SQL = "SELECT     id " +
                           "FROM regional ";

            if (statusReserva.Contains("TX"))
            {
                s_SQL = s_SQL + "WHERE(regional = 'TX')";
            }
            else if (statusReserva.Contains("AR"))
            {
                s_SQL = s_SQL + "WHERE(regional = 'AR')";
            }
            else if (statusReserva.Contains("SM"))
            {
                s_SQL = s_SQL + "WHERE(regional = 'SM')";
            }
            else if (statusReserva == "Reserva")
            {
                s_SQL = s_SQL + "WHERE(regional = 'N/A')";
            }

            try
            {
                using (SqlConnection cnx = new SqlConnection(GetCnx()))
                {
                    cnx.Open();

                    using (SqlCommand cmd = new SqlCommand(s_SQL, cnx))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                            s_dt.Load(reader);

                        idRegional = (int)s_dt.Rows[0][0];
                    }
                    cnx.Close();
                }


            }
            catch (Exception ex)
            {
                setLog(iduser, "err", "getIdRegionalPeloSatatus", ex.Message, "bdados.cs");
            }

            return idRegional;
        }

        public void setNumDiasAuditoriaInterna(string numDias)
        {
            DataTable s_dt = new DataTable();

            string s_SQL = $"UPDATE filtro_regra SET nome_filtro = '{numDias}' WHERE tipo =  'qual_verif_monitora_limite_prazo'";

            try
            {
                using (SqlConnection cnx = new SqlConnection(GetCnx()))
                {
                    cnx.Open();

                    using (SqlCommand cmd = new SqlCommand(s_SQL, cnx))
                    {
                        cmd.CommandText = s_SQL;
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }

                    cnx.Close();
                }
            }
            catch (Exception ex)
            {
                // TODO: tratar exeção
            }
        }
    }
}