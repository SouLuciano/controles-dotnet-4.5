﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="epi_controle.aspx.cs" Inherits="webequilibrio.app.controle.epi_controle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxGridView runat="server" AutoGenerateColumns="False" DataSourceID="dsEpiControle" Theme="Metropolis" ID="gvColabFuncao">
        <SettingsDetail AllowOnlyOneMasterRowExpanded="True">
        </SettingsDetail>
        <SettingsEditing Mode="PopupEditForm">
        </SettingsEditing>
        <Settings ShowFilterRow="True" ShowFilterRowMenu="True">
        </Settings>
        <SettingsBehavior AutoFilterRowInputDelay="1800">
        </SettingsBehavior>
        <SettingsPopup>
            <EditForm HorizontalAlign="Center">
            </EditForm>
        </SettingsPopup>
        <SettingsSearchPanel Delay="2000">
        </SettingsSearchPanel>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="dsEpiControle" runat="server" 
        ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
        DeleteCommand="DELETE FROM [epi_controle] WHERE [id] = ?" 
        InsertCommand="INSERT INTO [epi_controle] ([id], [id_colaborador], [id_tipo_equipamento], [dt_ultimo_epi], [dt_novo_epi], [dt_vencimento_ca], [valor], [dt_criacao], [dt_atualizacao], [user_email]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" 
        ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" 
        SelectCommand="SELECT [id], [id_colaborador], [id_tipo_equipamento], [dt_ultimo_epi], [dt_novo_epi], [dt_vencimento_ca], [valor], [dt_criacao], [dt_atualizacao], [user_email] FROM [epi_controle]" 
        UpdateCommand="UPDATE [epi_controle] SET [id_colaborador] = ?, [id_tipo_equipamento] = ?, [dt_ultimo_epi] = ?, [dt_novo_epi] = ?, [dt_vencimento_ca] = ?, [valor] = ?, [dt_criacao] = ?, [dt_atualizacao] = ?, [user_email] = ? WHERE [id] = ?">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="id" Type="Int32" />
            <asp:Parameter Name="id_colaborador" Type="Int32" />
            <asp:Parameter Name="id_tipo_equipamento" Type="Int32" />
            <asp:Parameter Name="dt_ultimo_epi" Type="DateTime" />
            <asp:Parameter Name="dt_novo_epi" Type="DateTime" />
            <asp:Parameter Name="dt_vencimento_ca" Type="DateTime" />
            <asp:Parameter Name="valor" Type="Decimal" />
            <asp:Parameter Name="dt_criacao" Type="DateTime" />
            <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
            <asp:Parameter Name="user_email" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="id_colaborador" Type="Int32" />
            <asp:Parameter Name="id_tipo_equipamento" Type="Int32" />
            <asp:Parameter Name="dt_ultimo_epi" Type="DateTime" />
            <asp:Parameter Name="dt_novo_epi" Type="DateTime" />
            <asp:Parameter Name="dt_vencimento_ca" Type="DateTime" />
            <asp:Parameter Name="valor" Type="Decimal" />
            <asp:Parameter Name="dt_criacao" Type="DateTime" />
            <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
            <asp:Parameter Name="user_email" Type="String" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
