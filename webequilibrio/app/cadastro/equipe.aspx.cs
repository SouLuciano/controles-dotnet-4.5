﻿using System;
using System.Collections.Generic;
using System.Web;
using DevExpress.Web;
using System.Data;
using DevExpress.XtraPrinting;
using DevExpress.Export;

namespace webequilibrio.app.cadastro
{


    public partial class equipe : System.Web.UI.Page
    {

        app.classe.bdados dados = new app.classe.bdados();
        string url;
        string userId;
        string email;



        protected void Page_Load(object sender, EventArgs e)
        {
            userId = Session["_userId"].ToString();
            url = HttpContext.Current.Request.Url.AbsolutePath;
            bool acesso = dados.temAcesso(Convert.ToInt32(userId), "~" + url);

            if (!acesso)
            {
                Response.Redirect("~/default.aspx", true);
            }

            using (DataTable dtUser = (DataTable)Session["_user"])
            {
                email = dtUser.Rows[0]["email"].ToString();
            }

            //if (!IsPostBack)
            //    Session["_manterProjetoIdAntigo"] = false;
        }

        protected void gvEquipe_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "criou", "equipe", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }

        protected void gvEquipe_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "atualizou", "equipe", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            dsRegional.SelectCommand = $"SELECT id, regional FROM regional WHERE (ativo = 'true') ORDER BY regional";
            dsRegional.DataBind();
        }

        protected void gvEquipe_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            e.NewValues["user_email"] = email;
            e.NewValues["dt_criacao"] = DateTime.Now;
            e.NewValues["dt_atualizacao"] = DateTime.Now;
        }

        protected void gvEquipe_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            e.NewValues["user_email"] = email;
            e.NewValues["dt_atualizacao"] = DateTime.Now;

        }

        void AddError(Dictionary<GridViewColumn, string> errors, GridViewColumn column, string errorText)
        {
            if (errors.ContainsKey(column)) return;
            errors[column] = errorText;
        }

        protected void gvEquipe_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            foreach (GridViewColumn column in gvEquipe.Columns)
            {
                GridViewDataColumn dataColumn = column as GridViewDataColumn;
                if (dataColumn == null) continue;

                if (e.NewValues["id_projeto"] == null)
                {
                    AddError(e.Errors, gvEquipe.Columns["id_projeto"], "Não pode ser em branco");
                }

                if (e.NewValues["id_regional"] == null)
                {
                    AddError(e.Errors, gvEquipe.Columns["id_regional"], "Não pode ser em branco");

                }

                if (e.NewValues["codigo"] == null)
                {
                    AddError(e.Errors, gvEquipe.Columns["codigo"], "Não pode ser em branco");
                }

                if (e.NewValues["ativo"] == null)
                {
                    AddError(e.Errors, gvEquipe.Columns["ativo"], "Ativo não pode ser em branco");
                }

                if (dataColumn.FieldName == "codigo")
                    if (e.NewValues["codigo"] != null)
                    {
                        if (e.Keys["id"] != null)
                        {
                            if (dados.checkDadoRepetido("codigo", e.NewValues["codigo"].ToString(), "equipe", userId, e.Keys["id"].ToString(), false, true))
                                AddError(e.Errors, gvEquipe.Columns["codigo"], "Esta equipe já está cadastrada");
                        }
                        else
                        {
                            if (dados.checkDadoRepetido("codigo", e.NewValues["codigo"].ToString(), "equipe", userId, "", false, true))
                                AddError(e.Errors, gvEquipe.Columns["codigo"], "Esta equipe já está cadastrada");
                        }
                    }
            }
        }

        protected void gvEquipe_CustomErrorText(object sender, DevExpress.Web.ASPxGridViewCustomErrorTextEventArgs e)
        {
            if (e.ErrorText.Contains("duplicate"))
            {
                if (e.ErrorText.Contains("IX_equipe_codigo") || e.ErrorText.Contains("UNIQUE KEY"))
                    e.ErrorText = "Esta equipe já está cadastrada";

            }
        }

        protected void gvEquipe_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            //if (e.Column.FieldName == "id_regional")
            //{
            //    var combo = (ASPxComboBox)e.Editor;
            //    combo.Callback += new CallbackEventHandlerBase(combo_Callback);

            //    var grid = e.Column.Grid;
            //    if (!combo.IsCallback)
            //    {
            //        var id_projeto = -1;

            //            if (!grid.IsNewRowEditing && Convert.ToBoolean(Session["_manterProjetoIdAntigo"]) == !true)
            //            {
            //                id_projeto = (int)grid.GetRowValues(e.VisibleIndex, "id_projeto"); //PEGA O ID DO PROJETO
            //                Session["projetoID"] = id_projeto;
            //            }

            //        FillCitiesComboBox(combo, Convert.ToInt32(Session["projetoID"]));
            //    }
            //}
        }

        private void combo_Callback(object sender, CallbackEventArgsBase e)
        {
            //int id_projeto = -1;
            //Int32.TryParse(e.Parameter, out id_projeto);

            //Session["_manterProjetoIdAntigo"] = true;
            //Session["projetoID"] = id_projeto;
            //    FillCitiesComboBox(sender as ASPxComboBox, id_projeto);
        }
        protected void FillCitiesComboBox(ASPxComboBox combo, int projetoID)
        {
            dsRegional.SelectCommand = $"SELECT id, regional FROM regional WHERE (ativo = 'true') AND (id_projeto = {projetoID}) ORDER BY regional";
            dsRegional.DataBind();

            combo.ValueField = "id";
            combo.TextField = "regional";
            combo.DataBind();
            combo.DataBindItems();

            //combo.DataSourceID = "Cities";
            //Cities.SelectParameters["id_projeto"].DefaultValue = id_projeto.ToString();
            //combo.DataBindItems();
            //combo.Items.Insert(0, new ListEditItem("", null)); // Null Item
        }

        protected void gvEquipe_CancelRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            //Session["_manterProjetoIdAntigo"] = false;
            //dsRegional.SelectCommand = $"SELECT id, regional FROM regional WHERE (ativo = 'true') ORDER BY regional";
            //dsRegional.DataBind();
        }
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "exportou", "Equipes", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            gvExporter.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }
    }
}