﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using DevExpress.Export;
using DevExpress.XtraPrinting;


namespace webequilibrio.app.residuo
{
    public partial class defasagem : System.Web.UI.Page
    {
        app.classe.bdados dados = new app.classe.bdados();
        string url;
        protected void Page_Load(object sender, EventArgs e)
        {
            url = HttpContext.Current.Request.Url.AbsolutePath;
            bool acesso = dados.temAcesso(Convert.ToInt32(Session["_userId"]), "~" + url);

            if (!acesso)
            {
                Response.Redirect("~/default.aspx", true);
            }

        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            gvExporter.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }
    }
}