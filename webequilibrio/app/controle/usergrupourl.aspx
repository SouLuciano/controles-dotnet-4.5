﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="usergrupourl.aspx.cs" Inherits="webequilibrio.app.controle.usergrupourl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <table style="width: 800px">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Associar Usuário ao Grupos"></asp:Label>
            </td>
            <td>&nbsp;</td>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Grupos"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" OnUnload="UpdatePanel1_Unload">
                    <ContentTemplate>
                        <dx:ASPxListBox ID="listUser" runat="server" AutoPostBack="True" DataSourceID="dsUser" EnableTheming="True" OnSelectedIndexChanged="listUser_SelectedIndexChanged" Rows="10" TextField="nome" Theme="Metropolis" ValueField="id" ValueType="System.Int32" Width="430px" Height="250px">
                            <Columns>
                                <dx:ListBoxColumn Caption="Nome" FieldName="nome" />
                                <dx:ListBoxColumn Caption="e-mail" FieldName="email" />
                            </Columns>
                        </dx:ASPxListBox>
                        <asp:SqlDataSource ID="dsUser" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [user].id, funcionario.nome AS Nome, [user].email FROM [user] INNER JOIN funcionario ON [user].idFuncionario = funcionario.id WHERE ([user].ativo = @ativo) ORDER BY Nome">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="1" Name="ativo" Type="Int16" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>&nbsp;</td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel4" runat="server" OnUnload="UpdatePanel4_Unload">
                    <ContentTemplate>
                        <dx:ASPxListBox ID="listGrupo" runat="server" DataSourceID="dsListGrupo" Rows="10" SelectionMode="CheckColumn" TextField="nome" Theme="Metropolis" ValueField="id" ValueType="System.Int32" Width="250px" Height="250px">
                        </dx:ASPxListBox>
                        <asp:SqlDataSource ID="dsListGrupo" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" SelectCommand="SELECT id, nome, descricao FROM [grupo] ORDER BY nome"></asp:SqlDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td style="vertical-align: bottom">
                <asp:UpdatePanel ID="UpdatePanel5" runat="server" OnUnload="UpdatePanel5_Unload">
                    <ContentTemplate>
                        <asp:Label ID="lblErroUserGrupo" runat="server" ForeColor="Red" Text="Erro!" Visible="False"></asp:Label>
                        <dx:ASPxButton ID="btnUserGrupo1" runat="server" Enabled="False" OnClick="btnUserGrupo_Click" Text="Salvar" Theme="Metropolis">
                        </dx:ASPxButton>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Associar Grupo a páginas"></asp:Label>
            </td>
            <td>&nbsp;</td>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Páginas (URL)"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" OnUnload="UpdatePanel2_Unload">
                    <ContentTemplate>
                        <dx:ASPxListBox ID="listGrupoUrl" runat="server" AutoPostBack="True" DataSourceID="dsRbGrupo" OnSelectedIndexChanged="listGrupoUrl_SelectedIndexChanged" Rows="10" TextField="nome" Theme="Metropolis" ValueField="id" ValueType="System.Int32" Width="400px" Height="250px">
                        </dx:ASPxListBox>
                        <asp:SqlDataSource ID="dsRbGrupo" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>"
                            SelectCommand="SELECT id, nome FROM [grupo] ORDER BY nome"></asp:SqlDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>&nbsp;</td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel3" runat="server" OnUnload="UpdatePanel3_Unload">
                    <ContentTemplate>
                        <dx:ASPxListBox ID="listUrl" runat="server" DataSourceID="dsListUrl" EnableTheming="True" SelectionMode="CheckColumn" TextField="nome" Theme="Metropolis" ValueField="id" ValueType="System.Int32" Width="250px" Rows="10" Height="250px">
                        </dx:ASPxListBox>
                        <asp:SqlDataSource ID="dsListUrl" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>"
                            SelectCommand="SELECT id, nome, url, descricao FROM [url] ORDER BY nome"></asp:SqlDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td style="vertical-align: bottom">
                <asp:UpdatePanel ID="UpdatePanel6" runat="server" OnUnload="UpdatePanel6_Unload">
                    <ContentTemplate>
                        <asp:Label ID="lblErroGrupoUrl" runat="server" ForeColor="Red" Text="Erro!" Visible="False"></asp:Label>
                        <dx:ASPxButton ID="btnGrupoUrl1" runat="server" Enabled="False" OnClick="btnGrupoUrl_Click" Text="Salvar" Theme="Metropolis">
                        </dx:ASPxButton>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <br></asp:Content>
