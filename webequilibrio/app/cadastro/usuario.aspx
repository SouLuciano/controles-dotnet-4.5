﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="usuario.aspx.cs" Inherits="webequilibrio.app.cadastro.usuario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" Theme="Metropolis" EnableHierarchyRecreation="False" Width="800px">
        <TabPages>
            <dx:TabPage Name="usuario" Text="Criar usuários">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <dx:ASPxGridView ID="gvUsuario" runat="server" AutoGenerateColumns="False" DataSourceID="dsUsuarios" EnableTheming="True" KeyFieldName="id" Theme="Metropolis">
                            <SettingsEditing Mode="PopupEditForm">
                            </SettingsEditing>
                            <Settings ShowFilterRow="True" ShowHeaderFilterButton="True" />
                            <SettingsSearchPanel Delay="2000" Visible="True" />
                            <Columns>
                                <dx:GridViewCommandColumn ShowClearFilterButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="7">
                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataCheckColumn Caption="Ativo" FieldName="ativo" ShowInCustomizationForm="True" VisibleIndex="6">
                                </dx:GridViewDataCheckColumn>
                                <dx:GridViewDataTextColumn Caption="Nome" FieldName="nome" ShowInCustomizationForm="True" VisibleIndex="3">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="e-mail" FieldName="email" ShowInCustomizationForm="True" VisibleIndex="4">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Senha" FieldName="senha" ShowInCustomizationForm="True" VisibleIndex="5">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataComboBoxColumn Caption="Nome do funcionário" FieldName="idFuncionario" ShowInCustomizationForm="True" VisibleIndex="2">
                                    <PropertiesComboBox DataSourceID="dsFuncionario" TextField="nome" ValueField="id">
                                    </PropertiesComboBox>
                                </dx:GridViewDataComboBoxColumn>
                            </Columns>
                        </dx:ASPxGridView>
                        <asp:SqlDataSource ID="dsUsuarios" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                            DeleteCommand="DELETE FROM [user] WHERE [id] = @id" 
                            InsertCommand="INSERT INTO [user] ([ativo], [nome], [email], [senha], [idFuncionario]) VALUES (@ativo, @nome, @email, @senha, @idFuncionario)" 
                            SelectCommand="SELECT [id], [ativo], [nome], [email], [senha], [idFuncionario] FROM [user]" 
                            UpdateCommand="UPDATE [user] SET [ativo] = @ativo, [nome] = @nome, [email] = @email, [senha] = @senha, [idFuncionario] = @idFuncionario WHERE [id] = @id">
                            <DeleteParameters>
                                <asp:Parameter Name="id" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="ativo" Type="Boolean" />
                                <asp:Parameter Name="nome" Type="String" />
                                <asp:Parameter Name="email" Type="String" />
                                <asp:Parameter Name="senha" Type="String" />
                                <asp:Parameter Name="idFuncionario" Type="Int32" />
                            </InsertParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="ativo" Type="Boolean" />
                                <asp:Parameter Name="nome" Type="String" />
                                <asp:Parameter Name="email" Type="String" />
                                <asp:Parameter Name="senha" Type="String" />
                                <asp:Parameter Name="idFuncionario" Type="Int32" />
                                <asp:Parameter Name="id" Type="Int32" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="dsFuncionario" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [nome], [id] FROM [funcionario] WHERE ([ativo] = @ativo) ORDER BY [nome]">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Name="grupo" Text="Criar grupo">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <dx:ASPxGridView ID="gvGrupo" runat="server" AutoGenerateColumns="False" DataSourceID="dsGrupos" EnableTheming="True" KeyFieldName="id" Theme="Metropolis">
                            <SettingsEditing Mode="PopupEditForm">
                            </SettingsEditing>
                            <Settings ShowFilterRow="True" ShowHeaderFilterButton="True" />
                            <SettingsSearchPanel Delay="2000" Visible="True" />
                            <Columns>
                                <dx:GridViewCommandColumn ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0" ShowClearFilterButton="True">
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="3">
                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="nome" ShowInCustomizationForm="True" VisibleIndex="1">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataMemoColumn FieldName="descricao" ShowInCustomizationForm="True" VisibleIndex="2">
                                </dx:GridViewDataMemoColumn>
                            </Columns>
                        </dx:ASPxGridView>
                        <asp:SqlDataSource ID="dsGrupos" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" DeleteCommand="DELETE FROM [grupo] WHERE [id] = @id" InsertCommand="INSERT INTO [grupo] ([nome], [descricao]) VALUES (@nome, @descricao)" SelectCommand="SELECT [id], [nome], [descricao] FROM [grupo] ORDER BY [nome]" UpdateCommand="UPDATE [grupo] SET [nome] = @nome, [descricao] = @descricao WHERE [id] = @id">
                            <DeleteParameters>
                                <asp:Parameter Name="id" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="nome" Type="String" />
                                <asp:Parameter Name="descricao" Type="String" />
                            </InsertParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="nome" Type="String" />
                                <asp:Parameter Name="descricao" Type="String" />
                                <asp:Parameter Name="id" Type="Int32" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
    </dx:ASPxPageControl>
</asp:Content>
