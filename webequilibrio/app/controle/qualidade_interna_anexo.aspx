﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="qualidade_interna_anexo.aspx.cs" Inherits="webequilibrio.app.controle.qualidade_interna_anexo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">

span.dx-vam, span.dx-vat, span.dx-vab, a.dx-vam, a.dx-vat, a.dx-vab 
{ 
    line-height: 100%; 
    padding: 2px 0;
    text-decoration: inherit;
}

.dx-vam, .dx-valm { vertical-align: middle; }
.dx-vam, .dx-vat, .dx-vab { display: inline-block!important; }

.dxpDesignMode,
.dxpDesignMode b {
	float: none !important;
}

.dxpDesignMode {
	display: inline;
}

.dxp-summary,
.dxp-sep,
.dxp-button,
.dxp-pageSizeItem,
.dxp-num,
.dxp-current,
.dxp-ellip /*Bootstrap correction*/
{
    -moz-box-sizing: content-box;
    -webkit-box-sizing: content-box;
    box-sizing: content-box;
}
.dxp-summary,
.dxp-sep,
.dxp-button,
.dxp-pageSizeItem,
.dxp-num,
.dxp-current,
.dxp-ellip
{
	display: block;
	float: left;
    line-height: 100%;
}
.dxp-current,
.dxp-disabledButton, 
.dxp-disabledButton span
{
    cursor: default;
}
.dxp-button,
.dxp-dropDownButton,
.dxp-num
{
    cursor: pointer;
}
.dx-clear
{
	display: block;
	clear: both;
	height: 0;
	width: 0;
	font-size: 0;
	line-height: 0;
	overflow: hidden;
	visibility: hidden;
}
        .auto-style1 {
            border-width: 0;
            background-image: url('mvwres://DevExpress.Web.ASPxThemes.v15.2,%20Version=15.2.7.0,%20Culture=neutral,%20PublicKeyToken=b88d1754d700e49a/DevExpress.Web.ASPxThemes.App_Themes.Metropolis.Web.sprite.png');
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <dx:ASPxGridView runat="server" KeyFieldName="id" AutoGenerateColumns="False" DataSourceID="dsFoto" Theme="Metropolis" EnableTheming="True" ID="gvFoto" OnCommandButtonInitialize="gvFoto_CommandButtonInitialize">
            <SettingsEditing Mode="PopupEditForm">
            </SettingsEditing>
            <Columns>
                <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="1" Visible="False">
                    <EditFormSettings Visible="False">
                    </EditFormSettings>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="id_qual_verif_monitora_filho" ShowInCustomizationForm="True" VisibleIndex="2" Visible="False">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataBinaryImageColumn Caption="Foto" FieldName="image" ShowInCustomizationForm="True" VisibleIndex="3">
                    <PropertiesBinaryImage EnableServerResize="True" ImageSizeMode="FitProportional" ImageWidth="90%">
                        <EditingSettings Enabled="True">
                            <UploadSettings UploadMode="Advanced">
                                <UploadValidationSettings MaxFileSize="1000000">
                                </UploadValidationSettings>
                            </UploadSettings>
                        </EditingSettings>
                    </PropertiesBinaryImage>
                </dx:GridViewDataBinaryImageColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
            DeleteCommand="DELETE FROM [qual_verif_monit_foto] WHERE [id] = @id" 
            InsertCommand="INSERT INTO [qual_verif_monit_foto] ([id_qual_verif_monitora_filho], [image]) VALUES (@id_qual_verif_monitora_filho, @image)" 
            SelectCommand="SELECT [id], [id_qual_verif_monitora_filho], [image] FROM [qual_verif_monit_foto] WHERE ([id_qual_verif_monitora_filho] = @id_qual_verif_monitora_filho)" 
            UpdateCommand="UPDATE [qual_verif_monit_foto] SET [id_qual_verif_monitora_filho] = @id_qual_verif_monitora_filho, [image] = @image WHERE [id] = @id" ID="dsFoto">
            <DeleteParameters>
                <asp:Parameter Name="id" Type="Int32"></asp:Parameter>
            </DeleteParameters>
            <InsertParameters>
                <asp:QueryStringParameter Name="id_qual_verif_monitora_filho" QueryStringField="id_foto" Type="Int32" />
                <asp:Parameter Name="image" DbType="Binary"></asp:Parameter>
            </InsertParameters>
            <SelectParameters>
                <asp:QueryStringParameter Name="id_qual_verif_monitora_filho" QueryStringField="id_foto" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="id_qual_verif_monitora_filho" Type="Int32"></asp:Parameter>
                <asp:Parameter Name="image" DbType="Binary"></asp:Parameter>
                <asp:Parameter Name="id" Type="Int32"></asp:Parameter>
            </UpdateParameters>
        </asp:SqlDataSource>
    
    </div>
    </form>
</body>
</html>
