﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="funcionario.aspx.cs" Inherits="webequilibrio.app.cadastro.funcionario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" Theme="Metropolis">
        <TabPages>
            <dx:TabPage Text="Funcionário">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table class="dxflInternalEditorTable_Moderno">
                            <tr>
                                <td>
                                    <table style="width: 400px">
                                        <tr>
                                            <td><span style="color: rgb(64, 64, 64); font-family: Roboto, 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: center; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none; background-color: rgb(255, 255, 255)">&nbsp;<asp:Label ID="Label2" runat="server" Text="Mostrar itens"></asp:Label>
                                                </span></td>
                                            <td>
                                                <dx:ASPxComboBox ID="cbxAtivo" runat="server" AutoPostBack="True" SelectedIndex="0" Theme="Metropolis">
                                                    <Items>
                                                        <dx:ListEditItem Selected="True" Text="Ativos" Value="True" />
                                                        <dx:ListEditItem Text="Inativos" Value="False" />
                                                    </Items>
                                                </dx:ASPxComboBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxGridView ID="gvFuncionario" runat="server" AutoGenerateColumns="False" DataSourceID="dsFuncionario" EnableTheming="True" KeyFieldName="id" OnRowInserting="gvFuncionario_RowInserting" OnRowUpdated="gvFuncionario_RowUpdated" OnRowUpdating="gvFuncionario_RowUpdating" OnRowValidating="gvFuncionario_RowValidating" Theme="Metropolis" OnRowInserted="gvFuncionario_RowInserted">
                                        <SettingsEditing Mode="PopupEditForm">
                                        </SettingsEditing>
                                        <Settings ShowFilterRow="True" ShowFilterRowMenu="True" />
                                        <SettingsBehavior AutoFilterRowInputDelay="2000" />
                                        <SettingsSearchPanel Visible="True" />
                                        <Columns>
                                            <dx:GridViewCommandColumn ShowClearFilterButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                            </dx:GridViewCommandColumn>
                                            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="8">
                                                <EditFormSettings Visible="False" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Nome" FieldName="nome" ShowInCustomizationForm="True" VisibleIndex="4">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataCheckColumn FieldName="ativo" ShowInCustomizationForm="True" VisibleIndex="6" Visible="False">
                                                <EditFormSettings Visible="True" />
                                            </dx:GridViewDataCheckColumn>
                                            <dx:GridViewDataDateColumn Caption="Data hora atualização" FieldName="dt_atualizacao" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="7">
                                                <PropertiesDateEdit DisplayFormatString="g">
                                                </PropertiesDateEdit>
                                                <EditFormSettings Visible="False" />
                                            </dx:GridViewDataDateColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="Projeto" FieldName="id_projeto" ShowInCustomizationForm="True" VisibleIndex="1">
                                                <PropertiesComboBox DataSourceID="dsProjeto" TextField="nome" ValueField="id">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="Equipe" FieldName="id_equipe" ShowInCustomizationForm="True" VisibleIndex="3">
                                                <PropertiesComboBox DataSourceID="dsEquipe" TextField="codigo" ValueField="id">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="Regional" FieldName="id_regional" ShowInCustomizationForm="True" VisibleIndex="2">
                                                <PropertiesComboBox DataSourceID="dsRegional" TextField="regional" ValueField="id">
                                                </PropertiesComboBox>
                                                <EditFormSettings Visible="False" />
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="Cargo" FieldName="id_cargo" ShowInCustomizationForm="True" VisibleIndex="5">
                                                <PropertiesComboBox DataSourceID="dsCargo" TextField="nome" ValueField="id">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                        </Columns>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                        </table>
                        <asp:SqlDataSource ID="dsFuncionario" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                            SelectCommand="SELECT [id], [id_projeto], [id_equipe], [id_regional], [id_cargo], [nome], [ativo], [dt_atualizacao] FROM [funcionario] WHERE ([ativo] = @ativo)" 
                            DeleteCommand="DELETE FROM [funcionario] WHERE [id] = @id" 
                            InsertCommand="INSERT INTO [funcionario] ([id_projeto], [id_equipe], [id_regional], [id_cargo], [nome], [ativo], [dt_atualizacao]) VALUES (@id_projeto, @id_equipe, @id_regional, @id_cargo, @nome, @ativo, @dt_atualizacao)" 
                            UpdateCommand="UPDATE [funcionario] SET [id_projeto] = @id_projeto, [id_equipe] = @id_equipe, [id_regional] = @id_regional, [id_cargo] = @id_cargo, [nome] = @nome, [ativo] = @ativo, [dt_atualizacao] = @dt_atualizacao WHERE [id] = @id">
                            <DeleteParameters>
                                <asp:Parameter Name="id" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="id_projeto" Type="Int32" />
                                <asp:Parameter Name="id_equipe" Type="Int32" />
                                <asp:Parameter Name="id_regional" Type="Int32" />
                                <asp:Parameter Name="id_cargo" Type="Int16" />
                                <asp:Parameter Name="nome" Type="String" />
                                <asp:Parameter Name="ativo" Type="Boolean" />
                                <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:ControlParameter ControlID="cbxAtivo" Name="ativo" PropertyName="Value" Type="Boolean" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="id_projeto" Type="Int32" />
                                <asp:Parameter Name="id_equipe" Type="Int32" />
                                <asp:Parameter Name="id_regional" Type="Int32" />
                                <asp:Parameter Name="id_cargo" Type="Int16" />
                                <asp:Parameter Name="nome" Type="String" />
                                <asp:Parameter Name="ativo" Type="Boolean" />
                                <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                                <asp:Parameter Name="id" Type="Int32" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="dsEquipe" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [codigo] FROM [equipe] WHERE ([ativo] = @ativo) ORDER BY [codigo]">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="dsProjeto" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [nome] FROM [projeto] WHERE ([ativo] = @ativo)">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="dsRegional" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [regional] FROM [regional] WHERE ([ativo] = @ativo) ORDER BY [regional]">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="dsCargo" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [nome] FROM [cargo] WHERE ([ativo] = @ativo) ORDER BY [nome]">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="Cargos">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <dx:ASPxGridView ID="gvCargo" runat="server" AutoGenerateColumns="False" DataSourceID="dsCargoEdit" EnableTheming="True" KeyFieldName="id" Theme="Metropolis">
                            <SettingsEditing Mode="PopupEditForm">
                            </SettingsEditing>
                            <Settings ShowFilterRow="True" />
                            <SettingsSearchPanel Visible="True" />
                            <Columns>
                                <dx:GridViewCommandColumn ShowClearFilterButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="3">
                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Nome da Função" FieldName="nome" ShowInCustomizationForm="True" VisibleIndex="1">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataCheckColumn FieldName="ativo" ShowInCustomizationForm="True" VisibleIndex="2">
                                </dx:GridViewDataCheckColumn>
                            </Columns>
                        </dx:ASPxGridView>
                        <asp:SqlDataSource ID="dsCargoEdit" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" DeleteCommand="DELETE FROM [cargo] WHERE [id] = @id" InsertCommand="INSERT INTO [cargo] ([nome], [ativo]) VALUES (@nome, @ativo)" SelectCommand="SELECT [id], [nome], [ativo] FROM [cargo] ORDER BY [nome]" UpdateCommand="UPDATE [cargo] SET [nome] = @nome, [ativo] = @ativo WHERE [id] = @id">
                            <DeleteParameters>
                                <asp:Parameter Name="id" Type="Int16" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="nome" Type="String" />
                                <asp:Parameter Name="ativo" Type="Boolean" />
                            </InsertParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="nome" Type="String" />
                                <asp:Parameter Name="ativo" Type="Boolean" />
                                <asp:Parameter Name="id" Type="Int16" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
    </dx:ASPxPageControl>
</asp:Content>
