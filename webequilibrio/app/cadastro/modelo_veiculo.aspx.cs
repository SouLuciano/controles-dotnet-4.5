﻿using System;
using System.Web;
using System.Data;

namespace webequilibrio.app.cadastro
{
    public partial class modelo_veiculo : System.Web.UI.Page
    {
        app.classe.bdados dados = new app.classe.bdados();
        string url;
        string userId;
        string email;

        protected void Page_Load(object sender, EventArgs e)
        {
            userId = Session["_userId"].ToString();
            url = HttpContext.Current.Request.Url.AbsolutePath;
            bool acesso = dados.temAcesso(Convert.ToInt32(userId), "~" + url);

            if (!acesso)
            {
                Response.Redirect("~/default.aspx", true);
            }

            using (DataTable dtUser = (DataTable)Session["_user"])
            {
                email = dtUser.Rows[0]["email"].ToString();
            }
        }

        protected void gvModeloVeiculo_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            e.NewValues["tipo"] = "veiculo";
        }

        protected void gvModeloVeiculo_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            Session["_newNomeVeiculo"] = e.NewValues["nome"].ToString();
            Session["_oldNomeVeiculo"] = e.OldValues["nome"].ToString();
        }

        protected void gvModeloVeiculo_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "atualizou", "modelo_veiculo", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }

        protected void gvModeloVeiculo_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "criou", "modelo_veiculo", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }

        protected void dsTipoEquipamento_Updating(object sender, System.Web.UI.WebControls.SqlDataSourceCommandEventArgs e)
        {
                string sql = $"UPDATE tipo_equipamento SET nome='{Session["_newNomeVeiculo"].ToString()}' WHERE nome='{Session["_oldNomeVeiculo"].ToString()}' AND tipo='veiculo'";
                e.Command.CommandText = sql;
        }
    }
}