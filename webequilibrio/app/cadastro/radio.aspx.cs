﻿using System;
using System.Web;
using System.Data;
using DevExpress.Export;
using DevExpress.Web;
using DevExpress.XtraPrinting;
using System.Collections.Generic;


namespace webequilibrio.app.cadastro
{
    public partial class radio : System.Web.UI.Page
    {
        app.classe.bdados dados = new app.classe.bdados();
        string url;
        string userId;
        string email;

        protected void Page_Load(object sender, EventArgs e)
        {
            userId = Session["_userId"].ToString();
            url = HttpContext.Current.Request.Url.AbsolutePath;
            bool acesso = dados.temAcesso(Convert.ToInt32(userId), "~" + url);

            if (!acesso)
            {
                Response.Redirect("~/default.aspx", true);
            }

            using (DataTable dtUser = (DataTable)Session["_user"])
            {
                email = dtUser.Rows[0]["email"].ToString();
            }
        }

        protected void gvRadio_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            string id_regional = "";
            e.NewValues["dt_criacao"] = DateTime.Now;
            e.NewValues["dt_atualizacao"] = DateTime.Now;
            e.NewValues["user_email"] = email;

            if (e.NewValues["id_equipe"] != null)
            {
                int id_equipe = Convert.ToInt32(e.NewValues["id_equipe"]);

                using (DataTable regional = dados.getRegional(id_equipe, userId))
                {
                    id_regional = regional.Rows[0]["id"].ToString();
                    e.NewValues["id_regional"] = regional.Rows[0]["id"];
                }
            }
        }

        protected void gvRadio_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            string id_regional = "";

            #region ATUALIZAÇÕES DA REGIONAL E STATUS

            if (e.NewValues["status"].ToString().Contains("Reserva") || e.NewValues["status"].ToString() == "Manutenção")
            {
                e.NewValues["id_equipe"] = null;

                if (e.NewValues["status"].ToString().Contains("Reserva"))
                {
                    int idRegional = dados.getIdRegionalPeloSatatus(e.NewValues["status"].ToString(), userId);
                    if (idRegional != 0)
                    {
                        e.NewValues["id_regional"] = idRegional;
                    }
                }
            }

            if (e.NewValues["id_equipe"] != null)
            {
                int id_equipe = Convert.ToInt32(e.NewValues["id_equipe"]);

                using (DataTable regional = dados.getRegional(id_equipe, userId))
                {
                    e.NewValues["id_regional"] = regional.Rows[0]["id"].ToString();
                    e.NewValues["status"] = "Em uso";
                }
            }

            #endregion

            try
            {
                e.NewValues["user_email"] = email;
                e.NewValues["dt_atualizacao"] = DateTime.Now;

                if (e.NewValues["id_equipe"] != null)
                {
                    int id_equipe = Convert.ToInt32(e.NewValues["id_equipe"]);

                    using (DataTable regional = dados.getRegional(id_equipe, userId))
                    {
                        id_regional = regional.Rows[0]["id"].ToString();
                        e.NewValues["id_regional"] = id_regional;
                    }
                }
            }

            catch (Exception ex)
            {
                e.Cancel = true;
                dados.setLog(Convert.ToInt32(userId), "err", "gvRadio_RowUpdating()", ex.Message, url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            }
        }

        protected void gvRadio_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "atualizou", "radio", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }

        protected void gvRadio_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "criou", "radio", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "exportou", "radio", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
            gvExporter.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }

        void AddError(Dictionary<GridViewColumn, string> errors, GridViewColumn column, string errorText)
        {
            if (errors.ContainsKey(column)) return;
            errors[column] = errorText;
        }

        protected void gvRadio_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            foreach (GridViewColumn column in gvRadio.Columns)
            {
                GridViewDataColumn dataColumn = column as GridViewDataColumn;
                if (dataColumn == null) continue;

                if (e.NewValues["id_projeto"] == null)
                {
                    AddError(e.Errors, gvRadio.Columns["id_projeto"], "Não pode ser em branco");
                }


                if (e.NewValues["ativo"] == null)
                {
                    AddError(e.Errors, gvRadio.Columns["ativo"], "Ativo não pode ser em branco");
                }

                if (e.NewValues["num_serie"] == null)
                {
                    AddError(e.Errors, gvRadio.Columns["num_serie"], "Núm. série não pode ser em branco");
                }

                if (e.NewValues["sinal"] == null)
                {
                    AddError(e.Errors, gvRadio.Columns["sinal"], "Sinal não pode ser em branco");
                }

                if (e.NewValues["condicao"] == null)
                {
                    AddError(e.Errors, gvRadio.Columns["condicao"], "Condição não pode ser em branco");
                }

                if (e.NewValues["tipo"] == null)
                {
                    AddError(e.Errors, gvRadio.Columns["tipo"], "Tipo não pode ser em branco");
                }

                if (dataColumn.FieldName == "num_serie")
                    if (e.NewValues["num_serie"] != null)
                    {
                        if (e.Keys["id"] != null)
                        {
                            if (dados.checkDadoRepetido("num_serie", e.NewValues["num_serie"].ToString(), "radio", userId, e.Keys["id"].ToString(), true, true))
                                AddError(e.Errors, gvRadio.Columns["num_serie"], "Este Núm. de série já está cadastrado");
                        }
                        else
                        {
                            if (dados.checkDadoRepetido("num_serie", e.NewValues["num_serie"].ToString(), "radio", userId, "", false, true))
                                AddError(e.Errors, gvRadio.Columns["num_serie"], "Este Núm. de série já está cadastrado");
                        }
                    }

            }

            if (string.IsNullOrEmpty(e.RowError) && e.Errors.Count > 0) e.RowError = "Corriga todos os erros.";
        }

        protected void gvRadio_CustomErrorText(object sender, ASPxGridViewCustomErrorTextEventArgs e)
        {

            //NAO FUNCIONA, A MENSAGEM DE ERRO NÃO APARECE CERTO
            //    if (e.ErrorText.Contains("duplicate"))
            //{
            //    if (e.ErrorText.Contains("IX_num_serie") || e.ErrorText.Contains("UNIQUE KEY"))
            //        e.ErrorText = "Esta Núm. de série já está cadastrada";


            //}
        }
    }
}