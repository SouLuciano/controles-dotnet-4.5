using System;
using System.Data;
using DevExpress.Web;


namespace webequilibrio
{
    public partial class MainMaster : System.Web.UI.MasterPage {
        DataTable usuario = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                usuario = (DataTable)Session["_user"];
                montarMenuLateral();
            }
        }

        private void montarMenuLateral()
        {

            app.classe.bdados dados = new app.classe.bdados();

            if (Session["_menuLateral"] == null)
            {
                try
                {
                    Session["_menuLateral"] = dados.getDadosNbLateral(Convert.ToInt32(usuario.Rows[0]["id"]));
                    menu((DataTable)Session["_menuLateral"]);
                }
                catch (Exception ex)
                {
                    dados.setLog(Convert.ToInt32(usuario.Rows[0]["id"]), "err", "montarMenuLateral", ex.Message, "Main.master", Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
                }
            }
            else
            {
                menu((DataTable)Session["_menuLateral"]);
            }
        }

        private void menu(DataTable menuDados)
        {
            DataView view = new DataView(menuDados);
            DataTable distinctValuesGrupo = view.ToTable(true, "grupo");
            NavBarGroup s_grupo = new NavBarGroup();
            NavBarItem s_item;

            for (int i = 0; i < distinctValuesGrupo.Rows.Count; i++)
            {
                menuLateral.Groups.Add(distinctValuesGrupo.Rows[i].ItemArray[0].ToString());
                foreach (DataRow row in menuDados.Rows)
                {
                    if (row.ItemArray[0].ToString() == distinctValuesGrupo.Rows[i].ItemArray[0].ToString())
                    {
                        s_item = new NavBarItem();
                        s_item.Text = row.ItemArray[1].ToString();
                        s_item.NavigateUrl = row.ItemArray[2].ToString();
                        menuLateral.Groups[i].Items.Add(s_item);
                    }
                }
            }
        }

        //protected void menuLateral_ClientLayout(object sender, ASPxClientLayoutArgs e)
        //{
        //    Control control = (Control)sender;

        //    if (e.LayoutMode == ClientLayoutMode.Loading)
        //    {
        //        if (Session["_menuLateral" + control.ClientID] != null)
        //            e.LayoutData = Session["_menuLateral" + control.ClientID].ToString();
        //    }
        //    else
        //    {
        //        Session["_menuLateral" + control.ClientID] = e.LayoutData;
        //    }
        //}
    }
}