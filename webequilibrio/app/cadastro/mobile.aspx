﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="mobile.aspx.cs" Inherits="webequilibrio.app.cadastro.mobile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxPageControl ID="pcMobile" runat="server" ActiveTabIndex="0" EnableTheming="True" Theme="Metropolis" EnableCallBacks="True">
        <TabPages>
            <dx:TabPage Text="Celular">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table class="dxflInternalEditorTable_Moderno">
                            <tr>
                                <td>
                                    <table cellpadding="1" cellspacing="1" class="modal-sm">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label1" runat="server" Text="Mostrar itens"></asp:Label>
                                            </td>
                                            <td>
                                                <dx:ASPxComboBox ID="cbxAtivo" runat="server" SelectedIndex="0" Theme="Metropolis" AutoPostBack="True">
                                                    <Items>
                                                        <dx:ListEditItem Selected="True" Text="Ativos" Value="True" />
                                                        <dx:ListEditItem Text="Inativos" Value="False" />
                                                    </Items>
                                                </dx:ASPxComboBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxGridView ID="gvCelular" runat="server" AutoGenerateColumns="False" DataSourceID="dsCelular" KeyFieldName="id" OnCustomErrorText="gvCelular_CustomErrorText" OnRowInserting="gvCelular_RowInserting" OnRowUpdated="gvCelular_RowUpdated" OnRowUpdating="gvCelular_RowUpdating" OnRowValidating="gvCelular_RowValidating" Theme="Metropolis">
                                        <SettingsDetail AllowOnlyOneMasterRowExpanded="True" ShowDetailRow="True" />
                                        <Templates>
                                            <DetailRow>
                                                <dx:ASPxGridView ID="gvCelularHist" runat="server" AutoGenerateColumns="False" DataSourceID="dsCelularHist" EnableTheming="True" KeyFieldName="id_celular" OnBeforePerformDataSelect="gvCelularHist_BeforePerformDataSelect" Theme="Metropolis">
                                                    <Settings ShowTitlePanel="True" />
                                                    <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                                                    <SettingsText Title="Histórico de alterações" />
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" Visible="False" VisibleIndex="15">
                                                            <EditFormSettings Visible="False" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="id_celular" Visible="False" VisibleIndex="0">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Modelo" FieldName="modelo" VisibleIndex="7">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Imei 1" FieldName="imei1" VisibleIndex="3">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Imei 2" FieldName="imei2" VisibleIndex="4">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Observação" FieldName="observacao" VisibleIndex="11">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Sistema OP." FieldName="sistema_op" Visible="False" VisibleIndex="10">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Senha" FieldName="senha" Visible="False" VisibleIndex="9">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Status" FieldName="status" VisibleIndex="8">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Usuário" FieldName="user_email" VisibleIndex="13">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataDateColumn Caption="Data Hora da alteração" FieldName="dt_criacao" VisibleIndex="14">
                                                            <PropertiesDateEdit DisplayFormatString="G" EditFormat="DateTime">
                                                            </PropertiesDateEdit>
                                                        </dx:GridViewDataDateColumn>
                                                        <dx:GridViewDataComboBoxColumn Caption="Projeto" FieldName="id_projeto" VisibleIndex="1">
                                                            <PropertiesComboBox DataSourceID="dsProjeto" TextField="nome" ValueField="id">
                                                            </PropertiesComboBox>
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataComboBoxColumn Caption="Sim Card 1" FieldName="id_simcard1" VisibleIndex="5">
                                                            <PropertiesComboBox DataSourceID="dsSimcard" TextField="numero" ValueField="id">
                                                            </PropertiesComboBox>
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataComboBoxColumn Caption="Sim Card 2" FieldName="id_simcard2" VisibleIndex="6">
                                                            <PropertiesComboBox DataSourceID="dsSimcard" TextField="numero" ValueField="id">
                                                            </PropertiesComboBox>
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataCheckColumn FieldName="ativo" VisibleIndex="12">
                                                        </dx:GridViewDataCheckColumn>
                                                        <dx:GridViewDataComboBoxColumn Caption="Equipe" FieldName="id_equipe" VisibleIndex="2">
                                                            <PropertiesComboBox DataSourceID="dsEquipe" TextField="codigo" ValueField="id">
                                                            </PropertiesComboBox>
                                                        </dx:GridViewDataComboBoxColumn>
                                                    </Columns>
                                                </dx:ASPxGridView>
                                                <asp:SqlDataSource ID="dsCelularHist" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [id_celular], [id_projeto], [modelo], [imei1], [imei2], [ativo], [observacao], [id_simcard1], [id_simcard2], [sistema_op], [senha], [status], [dt_criacao], [user_email], [id_equipe] FROM [celular_hist] WHERE ([id_celular] = @id_celular) ORDER BY [dt_criacao] DESC">
                                                    <SelectParameters>
                                                        <asp:SessionParameter Name="id_celular" SessionField="id_celular" Type="Int32" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </DetailRow>
                                        </Templates>
                                        <SettingsEditing Mode="PopupEditForm">
                                        </SettingsEditing>
                                        <Settings ShowFilterRow="True" ShowFilterRowMenu="True" />
                                        <SettingsBehavior AutoFilterRowInputDelay="2000" />
                                        <SettingsPopup>
                                            <EditForm HorizontalAlign="Center" />
                                        </SettingsPopup>
                                        <SettingsSearchPanel Delay="2000" Visible="True" />
                                        <Columns>
                                            <dx:GridViewCommandColumn ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                            </dx:GridViewCommandColumn>
                                            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="17">
                                                <EditFormSettings Visible="False" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Imei 1" FieldName="imei1" ShowInCustomizationForm="True" VisibleIndex="4">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Imei 2" FieldName="imei2" ShowInCustomizationForm="True" VisibleIndex="5">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataCheckColumn Caption="Ativo" FieldName="ativo" ShowInCustomizationForm="True" VisibleIndex="14" Visible="False">
                                                <EditFormSettings Visible="True" />
                                            </dx:GridViewDataCheckColumn>
                                            <dx:GridViewDataTextColumn Caption="Sistema OP." FieldName="sistema_op" ShowInCustomizationForm="True" Visible="False" VisibleIndex="9">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Senha" FieldName="senha" ShowInCustomizationForm="True" Visible="False" VisibleIndex="10">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="Projeto" FieldName="id_projeto" ShowInCustomizationForm="True" VisibleIndex="1">
                                                <PropertiesComboBox DataSourceID="dsProjeto" TextField="nome" ValueField="id">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="Sim Card 1" FieldName="id_simcard1" ShowInCustomizationForm="True" VisibleIndex="6">
                                                <PropertiesComboBox AllowNull="True" DataSourceID="dsSimcard" TextField="numero" ValueField="id">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="Sim Card 2" FieldName="id_simcard2" ShowInCustomizationForm="True" VisibleIndex="7">
                                                <PropertiesComboBox AllowNull="True" DataSourceID="dsSimcard" TextField="numero" ValueField="id">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="Status" FieldName="status" ShowInCustomizationForm="True" VisibleIndex="12">
                                                <PropertiesComboBox DataSourceID="dsStatusEquipamento" TextField="condicao" ValueField="condicao">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataMemoColumn Caption="Observação" FieldName="observacao" ShowInCustomizationForm="True" VisibleIndex="13">
                                            </dx:GridViewDataMemoColumn>
                                            <dx:GridViewDataDateColumn Caption="Data hora atualização" FieldName="dt_atualizacao" ShowInCustomizationForm="True" VisibleIndex="16">
                                                <PropertiesDateEdit DisplayFormatString="g">
                                                </PropertiesDateEdit>
                                                <EditFormSettings Visible="False" />
                                            </dx:GridViewDataDateColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="Modelo" FieldName="modelo" ShowInCustomizationForm="True" VisibleIndex="8">
                                                <PropertiesComboBox DataSourceID="dsTipoEquipamento" TextField="nome" ValueField="nome">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="Equipe" FieldName="id_equipe" ShowInCustomizationForm="True" VisibleIndex="3">
                                                <PropertiesComboBox AllowNull="True" DataSourceID="dsEquipe" TextField="codigo" ValueField="id">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataTextColumn Caption="Usuário" FieldName="user_email" ReadOnly="True" ShowInCustomizationForm="True" Visible="False" VisibleIndex="15">
                                                <EditFormSettings Visible="False" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="Regional" FieldName="id_regional" ShowInCustomizationForm="True" VisibleIndex="2">
                                                <PropertiesComboBox DataSourceID="dsRegional" TextField="regional" ValueField="id">
                                                </PropertiesComboBox>
                                                <EditFormSettings Visible="False" />
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="Função" FieldName="funcao" ShowInCustomizationForm="True" VisibleIndex="11">
                                                <PropertiesComboBox TextField="nome" ValueField="nome" DataSourceID="dsFuncaoCel">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                        </Columns>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxButton ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" Text="Exportar para excel" Theme="Metropolis">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                        <asp:SqlDataSource ID="dsCelular" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>"
                            DeleteCommand="DELETE FROM [celular] WHERE [id] = @id" 
                            InsertCommand="INSERT INTO [celular] ([id_projeto], [modelo], [imei1], [imei2], [ativo], [observacao], [id_simcard1], [id_simcard2], [sistema_op], [senha], [status], [dt_criacao], [dt_atualizacao], [user_email], [id_equipe], [funcao], [id_regional]) VALUES (@id_projeto, @modelo, @imei1, @imei2, @ativo, @observacao, @id_simcard1, @id_simcard2, @sistema_op, @senha, @status, @dt_criacao, @dt_atualizacao, @user_email, @id_equipe, @funcao, @id_regional)"
                            SelectCommand="SELECT [id], [id_projeto], [modelo], [imei1], [imei2], [ativo], [observacao], [id_simcard1], [id_simcard2], [sistema_op], [senha], [status], [dt_criacao], [dt_atualizacao], [user_email], [id_equipe], [funcao], [id_regional] FROM [celular] WHERE ([ativo] = @ativo)"
                            UpdateCommand="UPDATE [celular] SET [id_projeto] = @id_projeto, [modelo] = @modelo, [imei1] = @imei1, [imei2] = @imei2, [ativo] = @ativo, [observacao] = @observacao, [id_simcard1] = @id_simcard1, [id_simcard2] = @id_simcard2, [sistema_op] = @sistema_op, [senha] = @senha, [status] = @status, [dt_criacao] = @dt_criacao, [dt_atualizacao] = @dt_atualizacao, [user_email] = @user_email, [id_equipe] = @id_equipe, [funcao] = @funcao, [id_regional] = @id_regional WHERE [id] = @id">
                            <DeleteParameters>
                                <asp:Parameter Name="id" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="id_projeto" Type="Int32" />
                                <asp:Parameter Name="modelo" Type="String" />
                                <asp:Parameter Name="imei1" Type="String" />
                                <asp:Parameter Name="imei2" Type="String" />
                                <asp:Parameter Name="ativo" Type="Boolean" />
                                <asp:Parameter Name="observacao" Type="String" />
                                <asp:Parameter Name="id_simcard1" Type="Int32" />
                                <asp:Parameter Name="id_simcard2" Type="Int32" />
                                <asp:Parameter Name="sistema_op" Type="String" />
                                <asp:Parameter Name="senha" Type="String" />
                                <asp:Parameter Name="status" Type="String" />
                                <asp:Parameter Name="dt_criacao" Type="DateTime" />
                                <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                                <asp:Parameter Name="user_email" Type="String" />
                                <asp:Parameter Name="id_equipe" Type="Int32" />
                                <asp:Parameter Name="funcao" Type="String" />
                                <asp:Parameter Name="id_regional" Type="Int32" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:ControlParameter ControlID="cbxAtivo" DefaultValue="True" Name="ativo" PropertyName="Value" Type="Boolean" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="id_projeto" Type="Int32" />
                                <asp:Parameter Name="modelo" Type="String" />
                                <asp:Parameter Name="imei1" Type="String" />
                                <asp:Parameter Name="imei2" Type="String" />
                                <asp:Parameter Name="ativo" Type="Boolean" />
                                <asp:Parameter Name="observacao" Type="String" />
                                <asp:Parameter Name="id_simcard1" Type="Int32" />
                                <asp:Parameter Name="id_simcard2" Type="Int32" />
                                <asp:Parameter Name="sistema_op" Type="String" />
                                <asp:Parameter Name="senha" Type="String" />
                                <asp:Parameter Name="status" Type="String" />
                                <asp:Parameter Name="dt_criacao" Type="DateTime" />
                                <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                                <asp:Parameter Name="user_email" Type="String" />
                                <asp:Parameter Name="id_equipe" Type="Int32" />
                                <asp:Parameter Name="funcao" Type="String" />
                                <asp:Parameter Name="id_regional" Type="Int32" />
                                <asp:Parameter Name="id" Type="Int32" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="dsProjeto" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [nome] FROM [projeto] WHERE ([ativo] = @ativo) ORDER BY [nome]">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="dsSimcard" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [numero] FROM [simcard] WHERE ([ativo] = @ativo) ORDER BY [numero]">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="dsStatusEquipamento" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" SelectCommand="SELECT condicao, tipo_equipamento FROM status_equipamento WHERE (tipo_equipamento = 'celular') ORDER BY condicao">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="celular" Name="tipo_equipamento" Type="String" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="dsTipoEquipamento" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" SelectCommand="SELECT nome, tipo FROM tipo_equipamento WHERE (tipo = @tipo) ORDER BY nome">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="celular" Name="tipo" Type="String" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="dsFuncaoCel" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [nome], [tipo] FROM [tipo_equipamento] WHERE ([tipo] = @tipo) ORDER BY [nome]">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="funcao_celular" Name="tipo" Type="String" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="dsEquipe" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" OnSelecting="dsEquipe_Selecting" SelectCommand="SELECT [id], [codigo] FROM [equipe] WHERE ([ativo] = @ativo) ORDER BY [codigo]">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <dx:ASPxGridViewExporter ID="gvExporter" runat="server" ExportedRowType="All" FileName="Celulares" GridViewID="gvCelular" PaperKind="A4">
                        </dx:ASPxGridViewExporter>
                        <asp:SqlDataSource ID="dsRegional" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [regional] FROM [regional] WHERE ([ativo] = @ativo) ORDER BY [regional]">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="Sim-Card (Chip)">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table class="dxflInternalEditorTable_Moderno">
                            <tr>
                                <td>
                                    <table class="modal-sm">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label2" runat="server" Text="Mostrar itens"></asp:Label>
                                            </td>
                                            <td>
                                                <dx:ASPxComboBox ID="cbxAtivo0" runat="server" AutoPostBack="True" SelectedIndex="0" Theme="Metropolis">
                                                    <Items>
                                                        <dx:ListEditItem Selected="True" Text="Ativos" Value="True" />
                                                        <dx:ListEditItem Text="Inativos" Value="False" />
                                                    </Items>
                                                </dx:ASPxComboBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxGridView ID="gvSimcard" runat="server" AutoGenerateColumns="False" DataSourceID="dsSimcard0" KeyFieldName="id" OnRowInserted="gvSimcard_RowInserted" OnRowUpdated="gvSimcard_RowUpdated" Theme="Metropolis">
                                        <SettingsEditing Mode="PopupEditForm">
                                        </SettingsEditing>
                                        <Settings ShowFilterRow="True" ShowFilterRowMenu="True" />
                                        <SettingsPopup>
                                            <EditForm HorizontalAlign="Center" />
                                        </SettingsPopup>
                                        <SettingsSearchPanel Delay="2000" Visible="True" />
                                        <Columns>
                                            <dx:GridViewCommandColumn ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                            </dx:GridViewCommandColumn>
                                            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="8">
                                                <EditFormSettings Visible="False" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Número" FieldName="numero" ShowInCustomizationForm="True" VisibleIndex="3">
                                                <PropertiesTextEdit>
                                                    <MaskSettings Mask="(99) 00000-0000" />
                                                </PropertiesTextEdit>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Serial" FieldName="serial" ShowInCustomizationForm="True" VisibleIndex="4">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Observação" FieldName="observacao" ShowInCustomizationForm="True" VisibleIndex="6">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="Projeto" FieldName="id_projeto" ShowInCustomizationForm="True" VisibleIndex="1">
                                                <PropertiesComboBox DataSourceID="dsProjeto0" TextField="nome" ValueField="id">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="Operadora" FieldName="operadora" ShowInCustomizationForm="True" VisibleIndex="2">
                                                <PropertiesComboBox>
                                                    <Items>
                                                        <dx:ListEditItem Text="Vivo" Value="vivo" />
                                                        <dx:ListEditItem Text="Tim" Value="tim" />
                                                        <dx:ListEditItem Text="Claro" Value="claro" />
                                                        <dx:ListEditItem Text="Oi" Value="oi" />
                                                        <dx:ListEditItem Text="Nextel" Value="nextel" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataCheckColumn Caption="Ativo" FieldName="ativo" ShowInCustomizationForm="True" Visible="False" VisibleIndex="7">
                                                <PropertiesCheckEdit ValueChecked="1" ValueType="System.Int32" ValueUnchecked="0">
                                                </PropertiesCheckEdit>
                                                <EditFormSettings Visible="True" />
                                            </dx:GridViewDataCheckColumn>
                                            <dx:GridViewDataSpinEditColumn Caption="Custo mensal" FieldName="custo" ShowInCustomizationForm="True" VisibleIndex="5">
                                                <PropertiesSpinEdit DisplayFormatString="c" NumberFormat="Currency">
                                                </PropertiesSpinEdit>
                                            </dx:GridViewDataSpinEditColumn>
                                        </Columns>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxButton ID="brnExportExcelSimcard" runat="server" OnClick="brnExportExcelSimcard_Click" Text="Exportar para excel" Theme="Metropolis">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                        <asp:SqlDataSource ID="dsSimcard0" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" DeleteCommand="DELETE FROM [simcard] WHERE [id] = @id" InsertCommand="INSERT INTO [simcard] ([id_projeto], [operadora], [numero], [serial], [custo], [observacao], [ativo], [dt_criacao], [dt_atualizacao]) VALUES (@id_projeto, @operadora, @numero, @serial, @custo, @observacao, @ativo, @dt_criacao, @dt_atualizacao)" SelectCommand="SELECT [id], [id_projeto], [operadora], [numero], [serial], [custo], [observacao], [ativo], [dt_criacao], [dt_atualizacao] FROM [simcard] WHERE ([ativo] = @ativo)" UpdateCommand="UPDATE [simcard] SET [id_projeto] = @id_projeto, [operadora] = @operadora, [numero] = @numero, [serial] = @serial, [custo] = @custo, [observacao] = @observacao, [ativo] = @ativo, [dt_criacao] = @dt_criacao, [dt_atualizacao] = @dt_atualizacao WHERE [id] = @id">
                            <DeleteParameters>
                                <asp:Parameter Name="id" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="id_projeto" Type="Int32" />
                                <asp:Parameter Name="operadora" Type="String" />
                                <asp:Parameter Name="numero" Type="String" />
                                <asp:Parameter Name="serial" Type="String" />
                                <asp:Parameter Name="custo" Type="Decimal" />
                                <asp:Parameter Name="observacao" Type="String" />
                                <asp:Parameter Name="ativo" Type="Boolean" />
                                <asp:Parameter Name="dt_criacao" Type="DateTime" />
                                <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:ControlParameter ControlID="cbxAtivo0" DefaultValue="True" Name="ativo" PropertyName="Value" Type="Boolean" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="id_projeto" Type="Int32" />
                                <asp:Parameter Name="operadora" Type="String" />
                                <asp:Parameter Name="numero" Type="String" />
                                <asp:Parameter Name="serial" Type="String" />
                                <asp:Parameter Name="custo" Type="Decimal" />
                                <asp:Parameter Name="observacao" Type="String" />
                                <asp:Parameter Name="ativo" Type="Boolean" />
                                <asp:Parameter Name="dt_criacao" Type="DateTime" />
                                <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                                <asp:Parameter Name="id" Type="Int32" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="dsProjeto0" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [nome] FROM [projeto] WHERE ([ativo] = @ativo) ORDER BY [nome]">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <dx:ASPxGridViewExporter ID="gvExporterSimcard" runat="server" ExportedRowType="All" FileName="simcard" GridViewID="gvSimcard" PaperKind="A4">
                        </dx:ASPxGridViewExporter>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="Software" Visible="False">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <dx:ASPxGridView ID="gvCelular0" runat="server" AutoGenerateColumns="False" DataSourceID="dsSoftware" KeyFieldName="id" Theme="Metropolis">
                            <SettingsEditing Mode="PopupEditForm">
                            </SettingsEditing>
                            <Settings ShowFilterRow="True" />
                            <SettingsPopup>
                                <EditForm HorizontalAlign="Center" />
                            </SettingsPopup>
                            <SettingsSearchPanel Delay="2000" />
                            <Columns>
                                <dx:GridViewCommandColumn ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="8">
                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="nome" ShowInCustomizationForm="True" VisibleIndex="1">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="versao" ShowInCustomizationForm="True" VisibleIndex="2">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="sistema" ShowInCustomizationForm="True" VisibleIndex="3">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="requer" ShowInCustomizationForm="True" VisibleIndex="4">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="fabricante" ShowInCustomizationForm="True" VisibleIndex="5">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataMemoColumn FieldName="changelog" ShowInCustomizationForm="True" VisibleIndex="6">
                                </dx:GridViewDataMemoColumn>
                                <dx:GridViewDataCheckColumn FieldName="ativo" ShowInCustomizationForm="True" VisibleIndex="7">
                                </dx:GridViewDataCheckColumn>
                            </Columns>
                        </dx:ASPxGridView>
                        <asp:SqlDataSource ID="dsSoftware" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>"
                            DeleteCommand="DELETE FROM software WHERE id = @id"
                            InsertCommand="INSERT INTO software (id, nome, versao, sistema, requer, fabricante, changelog, ativo) VALUES (@id, @nome, @versao, @sistema, @requer, @fabricante, @changelog, @ativo)"
                            ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>"
                            SelectCommand="SELECT id, nome, versao, sistema, requer, fabricante, changelog, ativo FROM software ORDER BY nome, versao"
                            UpdateCommand="UPDATE software SET nome = @nome, versao = @versao, sistema = @sistema, requer = @requer, fabricante = @fabricante, changelog = @changelog, ativo = @ativo WHERE id = @id">
                            <DeleteParameters>
                                <asp:Parameter Name="id" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="id" Type="Int32" />
                                <asp:Parameter Name="nome" Type="String" />
                                <asp:Parameter Name="versao" Type="String" />
                                <asp:Parameter Name="sistema" Type="String" />
                                <asp:Parameter Name="requer" Type="String" />
                                <asp:Parameter Name="fabricante" Type="String" />
                                <asp:Parameter Name="changelog" Type="String" />
                                <asp:Parameter Name="ativo" Type="Object" />
                            </InsertParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="nome" Type="String" />
                                <asp:Parameter Name="versao" Type="String" />
                                <asp:Parameter Name="sistema" Type="String" />
                                <asp:Parameter Name="requer" Type="String" />
                                <asp:Parameter Name="fabricante" Type="String" />
                                <asp:Parameter Name="changelog" Type="String" />
                                <asp:Parameter Name="ativo" Type="Object" />
                                <asp:Parameter Name="id" Type="Int32" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="Modelo e função">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table class="dxflInternalEditorTable_Moderno">
                            <tr>
                                <td>
                                    <dx:ASPxGridView ID="gvModeloCelular" runat="server" AutoGenerateColumns="False" DataSourceID="dsTipoCelular" KeyFieldName="nome" OnRowInserted="gvModeloCelular_RowInserted" OnRowInserting="gvModeloCelular_RowInserting" OnRowUpdated="gvModeloCelular_RowUpdated" OnRowUpdating="gvModeloCelular_RowUpdating" Theme="Metropolis">
                                        <SettingsDetail AllowOnlyOneMasterRowExpanded="True" />
                                        <Templates>
                                            <PreviewRow>
                                            </PreviewRow>
                                        </Templates>
                                        <SettingsEditing Mode="PopupEditForm">
                                        </SettingsEditing>
                                        <Settings ShowFilterRow="True" ShowFilterRowMenu="True" />
                                        <SettingsBehavior ConfirmDelete="True" />
                                        <SettingsPopup>
                                            <EditForm HorizontalAlign="Center" />
                                        </SettingsPopup>
                                        <SettingsSearchPanel Delay="2000" Visible="True" />
                                        <Columns>
                                            <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                            </dx:GridViewCommandColumn>
                                            <dx:GridViewDataTextColumn Caption="Modelo" FieldName="nome" ShowInCustomizationForm="True" VisibleIndex="1">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="tipo" ReadOnly="True" ShowInCustomizationForm="True" Visible="False" VisibleIndex="2">
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:ASPxGridView>
                                </td>
                                <td>
                                    <dx:ASPxGridView ID="gvFuncaoCelular" runat="server" AutoGenerateColumns="False" DataSourceID="dsFuncaoCelular" KeyFieldName="nome" OnRowInserted="gvFuncaoCelular_RowInserted" OnRowInserting="gvFuncaoCelular_RowInserting" OnRowUpdated="gvFuncaoCelular_RowUpdated" OnRowUpdating="gvFuncaoCelular_RowUpdating" Theme="Metropolis">
                                        <SettingsDetail AllowOnlyOneMasterRowExpanded="True" />
                                        <Templates>
                                            <PreviewRow>
                                            </PreviewRow>
                                        </Templates>
                                        <SettingsEditing Mode="PopupEditForm">
                                        </SettingsEditing>
                                        <Settings ShowFilterRow="True" ShowFilterRowMenu="True" />
                                        <SettingsBehavior ConfirmDelete="True" />
                                        <SettingsPopup>
                                            <EditForm HorizontalAlign="Center" />
                                        </SettingsPopup>
                                        <SettingsSearchPanel Delay="2000" Visible="True" />
                                        <Columns>
                                            <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                            </dx:GridViewCommandColumn>
                                            <dx:GridViewDataTextColumn Caption="Função" FieldName="nome" ShowInCustomizationForm="True" VisibleIndex="1">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="tipo" ReadOnly="True" ShowInCustomizationForm="True" Visible="False" VisibleIndex="2">
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                        </table>
                        <asp:SqlDataSource ID="dsTipoCelular" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                            DeleteCommand="DELETE FROM tipo_equipamento WHERE (nome = @nome) AND (tipo = 'celular')" 
                            InsertCommand="INSERT INTO [tipo_equipamento] ([nome], [tipo]) VALUES (@nome, @tipo)" 
                            OnUpdating="dsTipoCelular_Updating" SelectCommand="SELECT [nome], [tipo] FROM [tipo_equipamento] WHERE ([tipo] = @tipo) ORDER BY [nome]" 
                            UpdateCommand="UPDATE tipo_equipamento SET nome = @nome, tipo = @tipo WHERE (nome = @nome) AND (tipo = 'celular')">
                            <DeleteParameters>
                                <asp:Parameter Name="nome" Type="String" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="nome" Type="String" />
                                <asp:Parameter Name="tipo" Type="String" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:Parameter DefaultValue="celular" Name="tipo" Type="String" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="nome" />
                                <asp:Parameter DefaultValue="celular" Name="tipo" Type="String" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="dsFuncaoCelular" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                            DeleteCommand="DELETE FROM tipo_equipamento WHERE (nome = @nome) AND (tipo = 'funcao_celular')" 
                            InsertCommand="INSERT INTO [tipo_equipamento] ([nome], [tipo]) VALUES (@nome, @tipo)" 
                            OnUpdating="dsFuncaoCelular_Updating" SelectCommand="SELECT [nome], [tipo] FROM [tipo_equipamento] WHERE ([tipo] = @tipo) ORDER BY [nome]"
                             UpdateCommand="UPDATE tipo_equipamento SET nome = @nome, tipo = @tipo WHERE (nome = @nome) AND (tipo = 'funcao_celular')">
                            <DeleteParameters>
                                <asp:Parameter Name="nome" Type="String" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="nome" Type="String" />
                                <asp:Parameter Name="tipo" Type="String" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:Parameter DefaultValue="funcao_celular" Name="tipo" Type="String" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="nome" />
                                <asp:Parameter DefaultValue="funcao_celular" Name="tipo" Type="String" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
    </dx:ASPxPageControl>
</asp:Content>
