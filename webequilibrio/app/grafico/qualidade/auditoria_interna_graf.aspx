﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Grafico.master" AutoEventWireup="true" CodeBehind="auditoria_interna_graf.aspx.cs" Inherits="webequilibrio.app.grafico.qualidade.auditoria_interna_graf" %>
<%@ Register assembly="DevExpress.Web.ASPxPivotGrid.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPivotGrid" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="GraficoContent" runat="server">
    <table style="width: 95%">
    <tr>
        <td>
            <dx:ASPxPivotGrid ID="gvAuditoria" runat="server" ClientIDMode="AutoID" DataSourceID="dtAuditoria" EnableTheming="True" Theme="PlasticBlue" OnCustomFieldSort="gvAuditoria_CustomFieldSort">
                <Fields>
                    <dx:PivotGridField ID="fieldnome" AreaIndex="0" Caption="Nome" FieldName="nome">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldregional" AreaIndex="1" Caption="Regional" FieldName="regional">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldnomefiltro" Area="RowArea" AreaIndex="0" Caption="Processo" FieldName="nome_filtro">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldqtdtalhao" AreaIndex="2" Caption="Qtd. Talhão" FieldName="qtd_talhao">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldqtdnc" Area="DataArea" AreaIndex="0" Caption="Qtd. NC" FieldName="qtd_nc">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldverificado" AreaIndex="3" Caption="Verificado" FieldName="verificado">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldemaberto" AreaIndex="4" Caption="Em aberto" FieldName="em_aberto">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldAno" Area="ColumnArea" AreaIndex="0" FieldName="Ano">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldMs" Area="ColumnArea" AreaIndex="1" FieldName="Mês" SortMode="Custom">
                    </dx:PivotGridField>
                </Fields>
            </dx:ASPxPivotGrid>
            <asp:SqlDataSource ID="dtAuditoria" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT projeto.nome, regional.regional, DATEPART(YEAR, dt_amostra) AS Ano, DATENAME(MONTH, dt_amostra) AS Mês, DATEPART(MONTH, dt_amostra) AS numMes, filtro_regra.nome_filtro, qtd_talhao, qtd_nc, verificado, em_aberto
FROM qual_verif_monitora,  projeto, regional, filtro_regra
WHERE 
qual_verif_monitora.id_projeto = projeto.id AND
qual_verif_monitora.id_regional = regional.id AND
qual_verif_monitora.id_filtro = filtro_regra.id AND
qual_verif_monitora	.ativo = 1"></asp:SqlDataSource>
            <dx:ASPxPivotGridExporter ID="ASPxPivotGridExporter1" runat="server" ASPxPivotGridID="gvAuditoria">
                <OptionsPrint>
                    <PageSettings PaperKind="A4" />
                </OptionsPrint>
            </dx:ASPxPivotGridExporter>
        </td>
    </tr>
    <tr>
        <td>
            <dx:ASPxButton ID="btnExportar" runat="server" OnClick="btnExportar_Click" Text="Exportar para Excel" Theme="PlasticBlue">
            </dx:ASPxButton>
        </td>
    </tr>
</table>
</asp:Content>
