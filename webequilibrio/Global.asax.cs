using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Security;
    using System.Web.SessionState;
    using DevExpress.Web;

    namespace webequilibrio {
        public class Global_asax : System.Web.HttpApplication {
            void Application_Start(object sender, EventArgs e)
            {
                DevExpress.Web.ASPxWebControl.CallbackError += new EventHandler(Application_Error);

                System.Web.UI.ScriptResourceDefinition myScriptResDef = new System.Web.UI.ScriptResourceDefinition();
                myScriptResDef.Path = "~/Scripts/jquery-3.1.1.min.js";
                myScriptResDef.DebugPath = "~/Scripts/jquery-3.1.1.js";
                myScriptResDef.CdnPath = "http://ajax.microsoft.com/ajax/jQuery/jquery-3.1.1.min.js";
                myScriptResDef.CdnDebugPath = "http://ajax.microsoft.com/ajax/jQuery/jquery-3.1.1.js";
                System.Web.UI.ScriptManager.ScriptResourceMapping.AddDefinition("jquery", null, myScriptResDef);
            }

            void Application_End(object sender, EventArgs e) {
                // Code that runs on application shutdown
            }

            void Application_Error(object sender, EventArgs e) {
                // Code that runs when an unhandled error occurs
            }

            void Session_Start(object sender, EventArgs e) {
            // Code that runs when a new session is started
            Session["_ip"] = Request.ServerVariables["REMOTE_ADDR"].ToString();
            }

            void Session_End(object sender, EventArgs e) {
                // Code that runs when a session ends. 
                // Note: The Session_End event is raised only when the sessionstate mode
                // is set to InProc in the Web.config file. If session mode is set to StateServer 
                // or SQLServer, the event is not raised.
            }
        }
    }