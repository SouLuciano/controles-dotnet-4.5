﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="simcard.aspx.cs" Inherits="webequilibrio.app.cadastro.simcard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" HeaderText="Cadastro de Chip de celular" Theme="Metropolis" Width="100%">
        <PanelCollection>
            <dx:PanelContent runat="server">
                <dx:ASPxGridView ID="gvSimcard" runat="server" AutoGenerateColumns="False" DataSourceID="dsSimcard" KeyFieldName="id" Theme="Metropolis">
                    <Settings ShowFilterRow="True" />
                    <Columns>
                        <dx:GridViewCommandColumn ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="id" ShowInCustomizationForm="True" VisibleIndex="1" ReadOnly="True">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="numero" ShowInCustomizationForm="True" VisibleIndex="4" Caption="Número">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="serial" ShowInCustomizationForm="True" VisibleIndex="5" Caption="Serial">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="observacao" ShowInCustomizationForm="True" VisibleIndex="7" Caption="Observação">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataComboBoxColumn Caption="Projeto" FieldName="id_projeto" ShowInCustomizationForm="True" VisibleIndex="2">
                            <PropertiesComboBox DataSourceID="dsProjeto" TextField="nome" ValueField="id">
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>
                        <dx:GridViewDataComboBoxColumn Caption="Operadora" FieldName="operadora" ShowInCustomizationForm="True" VisibleIndex="3">
                            <PropertiesComboBox>
                                <Items>
                                    <dx:ListEditItem Text="Vivo" Value="vivo" />
                                    <dx:ListEditItem Text="Tim" Value="tim" />
                                    <dx:ListEditItem Text="Claro" Value="claro" />
                                    <dx:ListEditItem Text="Oi" Value="oi" />
                                    <dx:ListEditItem Text="Nextel" Value="nextel" />
                                </Items>
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>
                        <dx:GridViewDataCheckColumn Caption="Ativo" FieldName="ativo" ShowInCustomizationForm="True" VisibleIndex="8">
                            <PropertiesCheckEdit ValueChecked="1" ValueType="System.Int32" ValueUnchecked="0">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataSpinEditColumn Caption="Custo mensal" FieldName="custo" ShowInCustomizationForm="True" VisibleIndex="6">
                            <PropertiesSpinEdit DisplayFormatString="c" NumberFormat="Currency">
                            </PropertiesSpinEdit>
                        </dx:GridViewDataSpinEditColumn>
                    </Columns>
                </dx:ASPxGridView>
                <asp:SqlDataSource ID="dsSimcard" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                    DeleteCommand="DELETE FROM simcard WHERE id = @id" 
                    InsertCommand="INSERT INTO simcard (id, id_projeto, operadora, numero, serial, custo, observacao, ativo) VALUES (@id, @id_projeto, @operadora, @numero, @serial, @custo, @observacao, @ativo)" 
                    ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" 
                    SelectCommand="SELECT id, id_projeto, operadora, numero, serial, custo, observacao, ativo FROM simcard" 
                    UpdateCommand="UPDATE simcard SET id_projeto = @id_projeto, operadora = @operadora, numero = @numero, serial = @serial, custo = @custo, observacao = @observacao, ativo = @ativo WHERE id = @id">
                    <DeleteParameters>
                        <asp:Parameter Name="id" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="id" Type="Int32" />
                        <asp:Parameter Name="id_projeto" Type="Int32" />
                        <asp:Parameter Name="operadora" Type="String" />
                        <asp:Parameter Name="numero" Type="String" />
                        <asp:Parameter Name="serial" Type="String" />
                        <asp:Parameter Name="custo" Type="Decimal" />
                        <asp:Parameter Name="observacao" Type="String" />
                        <asp:Parameter Name="ativo" Type="Int16" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="id_projeto" Type="Int32" />
                        <asp:Parameter Name="operadora" Type="String" />
                        <asp:Parameter Name="numero" Type="String" />
                        <asp:Parameter Name="serial" Type="String" />
                        <asp:Parameter Name="custo" Type="Decimal" />
                        <asp:Parameter Name="observacao" Type="String" />
                        <asp:Parameter Name="ativo" Type="Int16" />
                        <asp:Parameter Name="id" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsProjeto" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" SelectCommand="SELECT id, nome FROM projeto ORDER BY nome"></asp:SqlDataSource>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>
</asp:Content>
