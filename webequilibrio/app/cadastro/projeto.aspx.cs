﻿using System;
using System.Collections.Generic;
using System.Web;
using DevExpress.Web;
using System.Data;

namespace webequilibrio.app.cadastro
{
    public partial class projeto : System.Web.UI.Page
    {
        app.classe.bdados dados = new app.classe.bdados();
        string url;
        string userId;
        string email;

        protected void Page_Load(object sender, EventArgs e)
        {

            userId = Session["_userId"].ToString();
            url = HttpContext.Current.Request.Url.AbsolutePath;
            bool acesso = dados.temAcesso(Convert.ToInt32(userId), "~" + url);

            if (!acesso)
            {
                Response.Redirect("~/default.aspx", true);
            }

            using (DataTable dtUser = (DataTable)Session["_user"])
            {
                email = dtUser.Rows[0]["email"].ToString();
            }
        }

        protected void gvProjeto_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "criou", "projeto", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }

        protected void gvProjeto_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            e.NewValues["user_email"] = email;
            e.NewValues["dt_criacao"] = DateTime.Now;
            e.NewValues["dt_atualizacao"] = DateTime.Now;
        }

        protected void gvProjeto_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "atualizou", "projeto", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }

        protected void gvProjeto_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            e.NewValues["user_email"] = email;
            e.NewValues["dt_atualizacao"] = DateTime.Now;
        }

        protected void gvProjeto_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            foreach (GridViewColumn column in gvProjeto.Columns)
            {
                GridViewDataColumn dataColumn = column as GridViewDataColumn;
                if (dataColumn == null) continue;

                if (e.NewValues["nome"] == null)
                {
                    AddError(e.Errors, gvProjeto.Columns["nome"], "Não pode ser em branco");
                }


                if (e.NewValues["ativo"] == null)
                {
                    AddError(e.Errors, gvProjeto.Columns["ativo"], "Ativo não pode ser em branco");
                }

                if (dataColumn.FieldName == "nome")
                    if (e.NewValues["nome"] != null)
                    {
                        if (e.Keys["id"] != null)
                        {
                            if (dados.checkDadoRepetido("nome", e.NewValues["nome"].ToString(), "projeto", userId, e.Keys["id"].ToString(),true,true))
                                AddError(e.Errors, gvProjeto.Columns["nome"], "Este projeto já está cadastrado");
                        }
                        else
                        {
                            if (dados.checkDadoRepetido("nome", e.NewValues["nome"].ToString(), "projeto", userId, "",true,true))
                                AddError(e.Errors, gvProjeto.Columns["nome"], "Este projeto já está cadastrado");
                        }


                    }
            }
        }

        void AddError(Dictionary<GridViewColumn, string> errors, GridViewColumn column, string errorText)
        {
            if (errors.ContainsKey(column)) return;
            errors[column] = errorText;
        }

        protected void gvProjeto_CustomErrorText(object sender, ASPxGridViewCustomErrorTextEventArgs e)
        {
            if (e.ErrorText.Contains("duplicate"))
            {
                if (e.ErrorText.Contains("IX_nome") || e.ErrorText.Contains("UNIQUE KEY"))
                    e.ErrorText = "Esta projeto já está cadastrada";

            }
        }
    }
}