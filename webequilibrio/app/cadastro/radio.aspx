﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="radio.aspx.cs" Inherits="webequilibrio.app.cadastro.radio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" HeaderText="Cadastro de rádios" Theme="Metropolis" Width="100%">
        <PanelCollection>
            <dx:PanelContent runat="server">
                <table class="dxflInternalEditorTable_Moderno">
                    <tr>
                        <td>
                            <table style="width: 300px">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text="Mostrar itens"></asp:Label>
                                    </td>
                                    <td>
                                        <dx:ASPxComboBox ID="cbxAtivo" runat="server" AutoPostBack="True" SelectedIndex="0" Theme="Metropolis">
                                            <Items>
                                                <dx:ListEditItem Selected="True" Text="Ativos" Value="True" />
                                                <dx:ListEditItem Text="Inativos" Value="False" />
                                            </Items>
                                        </dx:ASPxComboBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvRadio" runat="server" AutoGenerateColumns="False" DataSourceID="dsRadio" KeyFieldName="id" OnRowInserting="gvRadio_RowInserting" OnRowUpdated="gvRadio_RowUpdated" OnRowUpdating="gvRadio_RowUpdating" OnRowValidating="gvRadio_RowValidating" Theme="Metropolis" OnRowInserted="gvRadio_RowInserted" OnCustomErrorText="gvRadio_CustomErrorText">
                                <SettingsDetail AllowOnlyOneMasterRowExpanded="True" />
                                <Templates>
                                    <PreviewRow>
                                    </PreviewRow>
                                </Templates>
                                <SettingsEditing Mode="PopupEditForm">
                                </SettingsEditing>
                                <Settings ShowFilterRow="True" ShowFilterRowMenu="True" />
                                <SettingsPopup>
                                    <EditForm HorizontalAlign="Center" />
                                </SettingsPopup>
                                <SettingsSearchPanel Delay="2000" Visible="True" />
                                <Columns>
                                    <dx:GridViewCommandColumn ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn FieldName="id" ShowInCustomizationForm="True" VisibleIndex="16" ReadOnly="True">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="num_serie" ShowInCustomizationForm="True" VisibleIndex="4" Caption="Núm. serie">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Modelo" FieldName="modelo" ShowInCustomizationForm="True" VisibleIndex="5">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataCheckColumn FieldName="ativo" ShowInCustomizationForm="True" VisibleIndex="12" Visible="False">
                                        <EditFormSettings Visible="True" />
                                    </dx:GridViewDataCheckColumn>
                                    <dx:GridViewDataTextColumn FieldName="user_email" ReadOnly="True" ShowInCustomizationForm="True" Visible="False" VisibleIndex="13">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn FieldName="dt_criacao" ShowInCustomizationForm="True" VisibleIndex="14" Visible="False">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataDateColumn Caption="Data hora atualização" FieldName="dt_atualizacao" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="15">
                                        <PropertiesDateEdit DisplayFormatString="g">
                                        </PropertiesDateEdit>
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Projeto" FieldName="id_projeto" ShowInCustomizationForm="True" VisibleIndex="1">
                                        <PropertiesComboBox DataSourceID="dsProjeto" TextField="nome" ValueField="id">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Regional" FieldName="id_regional" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="2">
                                        <PropertiesComboBox DataSourceID="dsRegional" TextField="regional" ValueField="id">
                                        </PropertiesComboBox>
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Equipe" FieldName="id_equipe" ShowInCustomizationForm="True" VisibleIndex="3">
                                        <PropertiesComboBox DataSourceID="dsEquipe" TextField="codigo" ValueField="id" AllowNull="True">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Sinal" FieldName="sinal" ShowInCustomizationForm="True" VisibleIndex="6">
                                        <PropertiesComboBox TextField="sinal" ValueField="sinal">
                                            <Items>
                                                <dx:ListEditItem Text="Analógico" Value="analogico" />
                                                <dx:ListEditItem Text="Digital" Value="digital" />
                                            </Items>
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Condição" FieldName="condicao" ShowInCustomizationForm="True" VisibleIndex="7">
                                        <PropertiesComboBox TextField="condicao" ValueField="condicao">
                                            <Items>
                                                <dx:ListEditItem Text="Locado" Value="locado" />
                                                <dx:ListEditItem Text="Próprio" Value="proprio" />
                                            </Items>
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Tipo" FieldName="tipo" ShowInCustomizationForm="True" VisibleIndex="9">
                                        <PropertiesComboBox TextField="tipo" ValueField="tipo">
                                            <Items>
                                                <dx:ListEditItem Text="Fixo (veicular)" Value="fixo" />
                                                <dx:ListEditItem Text="Portátil (móvel)" Value="portatil" />
                                            </Items>
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataMemoColumn Caption="Observação" FieldName="observacao" ShowInCustomizationForm="True" VisibleIndex="11">
                                    </dx:GridViewDataMemoColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="Status" FieldName="status" ShowInCustomizationForm="True" VisibleIndex="10">
                                                <PropertiesComboBox DataSourceID="dsStatusEquipamento" TextField="condicao" ValueField="condicao">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataDateColumn Caption="Data Nota F" FieldName="dt_nf" ShowInCustomizationForm="True" VisibleIndex="8">
                                    </dx:GridViewDataDateColumn>
                                </Columns>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" Text="Exportar para excel" Theme="Metropolis">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="dsRadio" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" DeleteCommand="DELETE FROM [radio] WHERE [id] = @id" InsertCommand="INSERT INTO [radio] ([id_projeto], [id_regional], [id_equipe], [num_serie], [modelo], [sinal], [condicao], [tipo], [observacao], [ativo], [user_email], [dt_criacao], [dt_atualizacao], [status], [dt_nf]) VALUES (@id_projeto, @id_regional, @id_equipe, @num_serie, @modelo, @sinal, @condicao, @tipo, @observacao, @ativo, @user_email, @dt_criacao, @dt_atualizacao, @status, @dt_nf)" SelectCommand="SELECT [id], [id_projeto], [id_regional], [id_equipe], [num_serie], [modelo], [sinal], [condicao], [tipo], [observacao], [ativo], [user_email], [dt_criacao], [dt_atualizacao], [status], [dt_nf] FROM [radio] WHERE ([ativo] = @ativo)" UpdateCommand="UPDATE [radio] SET [id_projeto] = @id_projeto, [id_regional] = @id_regional, [id_equipe] = @id_equipe, [num_serie] = @num_serie, [modelo] = @modelo, [sinal] = @sinal, [condicao] = @condicao, [tipo] = @tipo, [observacao] = @observacao, [ativo] = @ativo, [user_email] = @user_email, [dt_criacao] = @dt_criacao, [dt_atualizacao] = @dt_atualizacao, [status] = @status, [dt_nf] = @dt_nf WHERE [id] = @id">
                    <DeleteParameters>
                        <asp:Parameter Name="id" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="id_projeto" Type="Int32" />
                        <asp:Parameter Name="id_regional" Type="Int32" />
                        <asp:Parameter Name="id_equipe" Type="Int32" />
                        <asp:Parameter Name="num_serie" Type="String" />
                        <asp:Parameter Name="modelo" Type="String" />
                        <asp:Parameter Name="sinal" Type="String" />
                        <asp:Parameter Name="condicao" Type="String" />
                        <asp:Parameter Name="tipo" Type="String" />
                        <asp:Parameter Name="observacao" Type="String" />
                        <asp:Parameter Name="ativo" Type="Boolean" />
                        <asp:Parameter Name="user_email" Type="String" />
                        <asp:Parameter Name="dt_criacao" Type="DateTime" />
                        <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                        <asp:Parameter Name="status" Type="String" />
                        <asp:Parameter Name="dt_nf" Type="DateTime" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="cbxAtivo" DefaultValue="True" Name="ativo" PropertyName="Value" Type="Boolean" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="id_projeto" Type="Int32" />
                        <asp:Parameter Name="id_regional" Type="Int32" />
                        <asp:Parameter Name="id_equipe" Type="Int32" />
                        <asp:Parameter Name="num_serie" Type="String" />
                        <asp:Parameter Name="modelo" Type="String" />
                        <asp:Parameter Name="sinal" Type="String" />
                        <asp:Parameter Name="condicao" Type="String" />
                        <asp:Parameter Name="tipo" Type="String" />
                        <asp:Parameter Name="observacao" Type="String" />
                        <asp:Parameter Name="ativo" Type="Boolean" />
                        <asp:Parameter Name="user_email" Type="String" />
                        <asp:Parameter Name="dt_criacao" Type="DateTime" />
                        <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                        <asp:Parameter Name="status" Type="String" />
                        <asp:Parameter Name="dt_nf" Type="DateTime" />
                        <asp:Parameter Name="id" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsProjeto" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                    SelectCommand="SELECT [id], [nome], [descricao] FROM [projeto] WHERE ([ativo] = @ativo) ORDER BY [nome]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsEquipe" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                    SelectCommand="SELECT [id], [id_projeto], [codigo] FROM [equipe] WHERE ([ativo] = @ativo)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsRegional" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                    SelectCommand="SELECT [id], [regional] FROM [regional] WHERE ([ativo] = @ativo) ORDER BY [regional]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <dx:ASPxGridViewExporter ID="gvExporter" runat="server" ExportedRowType="All" FileName="Radios" GridViewID="gvRadio" PaperKind="A4">
                </dx:ASPxGridViewExporter>
                <asp:SqlDataSource ID="dsStatusEquipamento" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [condicao], [tipo_equipamento] FROM [status_equipamento] WHERE ([tipo_equipamento] = @tipo_equipamento)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="radio" Name="tipo_equipamento" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>
</asp:Content>
