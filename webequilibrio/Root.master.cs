using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;


namespace webequilibrio {
    public partial class RootMaster : System.Web.UI.MasterPage {

        app.classe.bdados dados = new app.classe.bdados();

        protected override void OnInit(EventArgs e)
        {
            if (Session["_user"] == null)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                Session.Abandon();
                System.Web.Security.FormsAuthentication.RedirectToLoginPage();
                Response.End();
            }
        }

        protected void Page_Load(object sender, EventArgs e) {
            //if (!IsPostBack)
                //lblNomeStudio.Text = dados.getNomeStudio((int)Session["_studio_id"], (long)Session["_user_id"]);
        }

        protected void LoginStatus_LoggedOut(Object sender, System.EventArgs e)
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Session.Abandon();
            System.Web.Security.FormsAuthentication.RedirectToLoginPage();
            Response.End();
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Session.Abandon();
            System.Web.Security.FormsAuthentication.RedirectToLoginPage();
            Response.End();
        }



    }
}