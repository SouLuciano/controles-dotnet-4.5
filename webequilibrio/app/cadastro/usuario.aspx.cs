﻿using System;
using System.Web;
using DevExpress.Web;

namespace webequilibrio.app.cadastro
{
    public partial class usuario : System.Web.UI.Page
    {
        app.classe.bdados dados = new app.classe.bdados();
        string url;
        ASPxListBox listaGrupo = new ASPxListBox();
        protected void Page_Load(object sender, EventArgs e)
        {
            url = HttpContext.Current.Request.Url.AbsolutePath;
            bool acesso = dados.temAcesso(Convert.ToInt32(Session["_userId"]), "~" + url);

            if (!acesso)
            {
                Response.Redirect("~/default.aspx", true);
            }

            if (!IsPostBack)
            {
                ASPxPageControl1.ActiveTabIndex = 0;
            }
            
        }

    }
}