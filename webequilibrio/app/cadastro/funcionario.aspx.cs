﻿using System;
using System.Data;
using System.Web;
using DevExpress.XtraPrinting;
using DevExpress.Export;
using DevExpress.Web;
using System.Collections.Generic;

namespace webequilibrio.app.cadastro
{
    public partial class funcionario : System.Web.UI.Page
    {
        string url;
        string userId;
        classe.bdados dados = new classe.bdados();
        string email;

        protected void Page_Load(object sender, EventArgs e)
        {
            userId = Session["_userId"].ToString();
            url = HttpContext.Current.Request.Url.AbsolutePath;
            bool acesso = dados.temAcesso(Convert.ToInt32(userId), "~" + url);

            if (!acesso)
            {
                Response.Redirect("~/default.aspx", true);
            }

            using (DataTable dtUser = (DataTable)Session["_user"])
            {
                email = dtUser.Rows[0]["email"].ToString();
            }
        }

        protected void gvFuncionario_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            e.NewValues["user_email"] = email;

            if (e.NewValues["id_equipe"] != null)
            {
                int id_equipe = Convert.ToInt32(e.NewValues["id_equipe"]);

                using (DataTable regional = dados.getRegional(id_equipe, userId))
                {
                    e.NewValues["id_regional"] = regional.Rows[0]["id"];
                }
            }

            e.NewValues["dt_atualizacao"] = DateTime.Now;
        }

        protected void gvFuncionario_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "atualizou", "funcionario", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }

        protected void gvFuncionario_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            try
            {
                #region ATUALIZAÇÃO DA REGIONAL

                if (e.NewValues["id_equipe"] != null)
                {
                    int id_equipe = Convert.ToInt32(e.NewValues["id_equipe"]);

                    using (DataTable regional = dados.getRegional(id_equipe, userId))
                    {
                        e.NewValues["id_regional"] = regional.Rows[0]["id"].ToString();
                    }
                }

                #endregion

                e.NewValues["dt_atualizacao"] = DateTime.Now;
            }
            catch (Exception ex)
            {
                dados.setLog(Convert.ToInt32(userId), "err", "gvFuncionario_RowUpdating()", ex.Message, url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
               
            }
            
        }

        protected void gvFuncionario_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            foreach (GridViewColumn column in gvFuncionario.Columns)
            {
                GridViewDataColumn dataColumn = column as GridViewDataColumn;
                if (dataColumn == null) continue;

                if (e.NewValues["id_projeto"] == null)
                {
                    AddError(e.Errors, gvFuncionario.Columns["id_projeto"], "Não pode ser em branco");
                }

                if (e.NewValues["id_equipe"] == null)
                {
                    AddError(e.Errors, gvFuncionario.Columns["id_equipe"], "Não pode ser em branco");
                }

                if (e.NewValues["id_cargo"] == null)
                {
                    AddError(e.Errors, gvFuncionario.Columns["id_cargo"], "Não pode ser em branco");
                }

                if (e.NewValues["nome"] == null)
                {
                    AddError(e.Errors, gvFuncionario.Columns["nome"], "Não pode ser em branco");
                }

                if (e.NewValues["ativo"] == null)
                {
                    AddError(e.Errors, gvFuncionario.Columns["ativo"], "Ativo não pode ser em branco");
                }
            }

        }

        void AddError(Dictionary<GridViewColumn, string> errors, GridViewColumn column, string errorText)
        {
            if (errors.ContainsKey(column)) return;
            errors[column] = errorText;
        }

        protected void gvFuncionario_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
        {
            dados.setLog(Convert.ToInt32(userId), "inf", "criou", "funcionario", url, Session["_ip"].ToString(), Request.Browser.Platform, Request.Browser.Browser, Request.Browser.Version);
        }
    }
}