﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="veiculo_km_rodado.aspx.cs" Inherits="webequilibrio.app.controle.veiculo_km_rodado" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" HeaderText="Km rodado por equipe" Theme="Metropolis" Width="100%">
        <PanelCollection>
            <dx:PanelContent runat="server">
                <table class="dxflInternalEditorTable_Moderno">
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvVeiculoKmRodado" runat="server" AutoGenerateColumns="False" DataSourceID="dsVeiculoKmRodado" KeyFieldName="id" Theme="Metropolis" OnInit="gvVeiculoKmRodado_Init" OnInitNewRow="gvVeiculoKmRodado_InitNewRow" OnRowDeleting="gvVeiculoKmRodado_RowDeleting" OnRowInserting="gvVeiculoKmRodado_RowInserting" OnRowUpdating="gvVeiculoKmRodado_RowUpdating">
                                <SettingsDetail AllowOnlyOneMasterRowExpanded="True" />
                                <Templates>
                                    <PreviewRow>
                                    </PreviewRow>
                                </Templates>
                                <SettingsEditing Mode="PopupEditForm">
                                </SettingsEditing>
                                <Settings ShowFilterRow="True" ShowFilterRowMenu="True" ShowFooter="True" />
                                <SettingsBehavior ConfirmDelete="True" />
                                <SettingsPopup>
                                    <EditForm HorizontalAlign="Center" />
                                </SettingsPopup>
                                <SettingsSearchPanel Delay="2000" Visible="True" />
                                <Columns>
                                    <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn FieldName="id" ShowInCustomizationForm="True" VisibleIndex="13" ReadOnly="True">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Observação" FieldName="observacao" ShowInCustomizationForm="True" VisibleIndex="9">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="user_email" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="10" Caption="Cadastrado pelo(a)">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn FieldName="dt_criacao" ShowInCustomizationForm="True" VisibleIndex="11" ReadOnly="True" Visible="False">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataDateColumn Caption="Data hora atualização" FieldName="dt_atualizacao" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="12" Visible="False">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Projeto" FieldName="id_projeto" ShowInCustomizationForm="True" VisibleIndex="1">
                                        <PropertiesComboBox DataSourceID="dsProjeto" TextField="nome" ValueField="id">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Regional" FieldName="id_regional" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="2">
                                        <PropertiesComboBox DataSourceID="dsRegional" TextField="regional" ValueField="id">
                                        </PropertiesComboBox>
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Equipe" FieldName="id_equipe" ShowInCustomizationForm="True" VisibleIndex="3">
                                        <PropertiesComboBox DataSourceID="dsEquipe" TextField="codigo" ValueField="id">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Mês" FieldName="mes" ShowInCustomizationForm="True" VisibleIndex="5">
                                        <PropertiesComboBox>
                                            <Items>
                                                <dx:ListEditItem Text="Jan" Value="jan" />
                                                <dx:ListEditItem Text="Fev" Value="fev" />
                                                <dx:ListEditItem Text="Mar" Value="mar" />
                                                <dx:ListEditItem Text="Abr" Value="abr" />
                                                <dx:ListEditItem Text="Mai" Value="mai" />
                                                <dx:ListEditItem Text="Jun" Value="jun" />
                                                <dx:ListEditItem Text="Jul" Value="jul" />
                                                <dx:ListEditItem Text="Ago" Value="ago" />
                                                <dx:ListEditItem Text="Set" Value="set" />
                                                <dx:ListEditItem Text="Out" Value="out" />
                                                <dx:ListEditItem Text="Nov" Value="nov" />
                                                <dx:ListEditItem Text="Dez" Value="dez" />
                                            </Items>
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataSpinEditColumn Caption="Ano" FieldName="ano" ShowInCustomizationForm="True" VisibleIndex="4">
                                        <PropertiesSpinEdit AllowNull="False" DisplayFormatString="g" NumberType="Integer">
                                        </PropertiesSpinEdit>
                                    </dx:GridViewDataSpinEditColumn>
                                    <dx:GridViewDataSpinEditColumn Caption="Km/Hrs rodado" FieldName="km_rodado" ShowInCustomizationForm="True" VisibleIndex="8">
                                        <PropertiesSpinEdit AllowNull="False" DisplayFormatString="g">
                                        </PropertiesSpinEdit>
                                    </dx:GridViewDataSpinEditColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Placa do carro" FieldName="id_veiculo" ShowInCustomizationForm="True" VisibleIndex="6">
                                        <PropertiesComboBox DataSourceID="dsVeiculo" TextField="placa" ValueField="id">
                                            <Columns>
                                                <dx:ListBoxColumn Caption="Placa" FieldName="placa" />
                                                <dx:ListBoxColumn Caption="Modelo" FieldName="modelo" />
                                            </Columns>
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                </Columns>
                                <TotalSummary>
                                    <dx:ASPxSummaryItem DisplayFormat="Soma={0:0.00}" FieldName="km_rodado" ShowInColumn="Observação" SummaryType="Sum" />
                                </TotalSummary>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" Text="Exportar para excel" Theme="Metropolis">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="dsVeiculoKmRodado" runat="server" 
                ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                DeleteCommand="DELETE FROM [veiculo_km_rodado] WHERE [id] = @id" 
                InsertCommand="INSERT INTO [veiculo_km_rodado] ([id_projeto], [id_regional], [id_equipe], [ano], [mes], [id_veiculo], [km_rodado], [observacao], [user_email], [dt_criacao], [dt_atualizacao]) VALUES (@id_projeto, @id_regional, @id_equipe, @ano, @mes, @id_veiculo, @km_rodado, @observacao, @user_email, @dt_criacao, @dt_atualizacao)" 
                SelectCommand="SELECT [id], [id_projeto], [id_regional], [id_equipe], [ano], [mes], [id_veiculo], [km_rodado], [observacao], [user_email], [dt_criacao], [dt_atualizacao] FROM [veiculo_km_rodado] ORDER BY [id] DESC" 
                UpdateCommand="UPDATE [veiculo_km_rodado] SET [id_projeto] = @id_projeto, [id_regional] = @id_regional, [id_equipe] = @id_equipe, [ano] = @ano, [mes] = @mes, [id_veiculo] = @id_veiculo, [km_rodado] = @km_rodado, [observacao] = @observacao, [user_email] = @user_email, [dt_criacao] = @dt_criacao, [dt_atualizacao] = @dt_atualizacao WHERE [id] = @id">
                    <DeleteParameters>
                        <asp:Parameter Name="id" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="id_projeto" Type="Int32" />
                        <asp:Parameter Name="id_regional" Type="Int32" />
                        <asp:Parameter Name="id_equipe" Type="Int32" />
                        <asp:Parameter Name="ano" Type="Int16" />
                        <asp:Parameter Name="mes" Type="String" />
                        <asp:Parameter Name="id_veiculo" Type="Int32" />
                        <asp:Parameter Name="km_rodado" Type="Decimal" />
                        <asp:Parameter Name="observacao" Type="String" />
                        <asp:Parameter Name="user_email" Type="String" />
                        <asp:Parameter Name="dt_criacao" Type="DateTime" />
                        <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="id_projeto" Type="Int32" />
                        <asp:Parameter Name="id_regional" Type="Int32" />
                        <asp:Parameter Name="id_equipe" Type="Int32" />
                        <asp:Parameter Name="ano" Type="Int16" />
                        <asp:Parameter Name="mes" Type="String" />
                        <asp:Parameter Name="id_veiculo" Type="Int32" />
                        <asp:Parameter Name="km_rodado" Type="Decimal" />
                        <asp:Parameter Name="observacao" Type="String" />
                        <asp:Parameter Name="user_email" Type="String" />
                        <asp:Parameter Name="dt_criacao" Type="DateTime" />
                        <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                        <asp:Parameter Name="id" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsProjeto" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                    SelectCommand="SELECT [id], [nome], [descricao] FROM [projeto] WHERE ([ativo] = @ativo) ORDER BY [nome]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsEquipe" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                    SelectCommand="SELECT [id], [id_projeto], [codigo] FROM [equipe] WHERE ([ativo] = @ativo)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsVeiculo" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [placa], [modelo] FROM [veiculo] ORDER BY [placa]"></asp:SqlDataSource>
                <asp:SqlDataSource ID="dsRegional" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                    SelectCommand="SELECT [id], [regional] FROM [regional] WHERE ([ativo] = @ativo) ORDER BY [regional]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <dx:ASPxGridViewExporter ID="gvExporter" runat="server" ExportedRowType="All" FileName="Veículo Km Rodado" GridViewID="gvVeiculoKmRodado" PaperKind="A4">
                </dx:ASPxGridViewExporter>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>
</asp:Content>
