﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="regional.aspx.cs" Inherits="webequilibrio.app.cadastro.regional" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" HeaderText="Cadastro de regionais" Theme="Metropolis" Width="100%">
        <PanelCollection>
            <dx:PanelContent runat="server">
                <table style="width: 100%">
                    <tr>
                        <td>
                            <table style="width: 400px">
                                <tr>
                                    <td><span style="color: rgb(64, 64, 64); font-family: Roboto, 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: center; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none; background-color: rgb(255, 255, 255)">&nbsp;<asp:Label ID="Label1" runat="server" Text="Mostrar itens"></asp:Label>
                                        </span></td>
                                    <td>
                                        <dx:ASPxComboBox ID="cbxAtivo" runat="server" AutoPostBack="True" SelectedIndex="0" Theme="Metropolis">
                                            <Items>
                                                <dx:ListEditItem Selected="True" Text="Ativos" Value="True" />
                                                <dx:ListEditItem Text="Inativos" Value="False" />
                                            </Items>
                                        </dx:ASPxComboBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvRegional" runat="server" AutoGenerateColumns="False" DataSourceID="dsRegional" KeyFieldName="id" OnRowInserted="gvRegional_RowInserted" OnRowInserting="gvRegional_RowInserting" OnRowUpdated="gvRegional_RowUpdated" OnRowUpdating="gvRegional_RowUpdating" OnRowValidating="gvRegional_RowValidating" Theme="Metropolis" OnCustomErrorText="gvRegional_CustomErrorText">
                                <SettingsEditing Mode="PopupEditForm">
                                </SettingsEditing>
                                <Settings ShowFilterRow="True" ShowFilterRowMenu="True" />
                                <SettingsSearchPanel Visible="True" />
                                <Columns>
                                    <dx:GridViewCommandColumn ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn FieldName="id" ShowInCustomizationForm="True" VisibleIndex="8" ReadOnly="True">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="regional" ShowInCustomizationForm="True" VisibleIndex="2" Caption="Regional">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Descrição" FieldName="descricao" ShowInCustomizationForm="True" VisibleIndex="3">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataCheckColumn Caption="Ativo" FieldName="ativo" ShowInCustomizationForm="True" Visible="False" VisibleIndex="4">
                                        <EditFormSettings Visible="True" />
                                    </dx:GridViewDataCheckColumn>
                                    <dx:GridViewDataDateColumn FieldName="dt_criacao" ReadOnly="True" ShowInCustomizationForm="True" Visible="False" VisibleIndex="5">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataDateColumn Caption="Data hora atualização" FieldName="dt_atualizacao" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="6">
                                        <PropertiesDateEdit DisplayFormatString="g">
                                        </PropertiesDateEdit>
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn FieldName="user_email" ShowInCustomizationForm="True" VisibleIndex="7" ReadOnly="True" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Projeto" FieldName="id_projeto" ShowInCustomizationForm="True" VisibleIndex="1">
                                        <PropertiesComboBox DataSourceID="dsProjeto" TextField="nome" ValueField="id" AllowNull="True">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                </Columns>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="dsProjeto" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [nome] FROM [projeto] WHERE ([ativo] = @ativo) ORDER BY [nome]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsRegional" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [id], [regional], [descricao], [ativo], [dt_criacao], [dt_atualizacao], [user_email], [id_projeto] FROM [regional] WHERE ([ativo] = @ativo) ORDER BY [id_projeto], [regional]" DeleteCommand="DELETE FROM [regional] WHERE [id] = @id" InsertCommand="INSERT INTO [regional] ([regional], [descricao], [ativo], [dt_criacao], [dt_atualizacao], [user_email], [id_projeto]) VALUES (@regional, @descricao, @ativo, @dt_criacao, @dt_atualizacao, @user_email, @id_projeto)" UpdateCommand="UPDATE [regional] SET [regional] = @regional, [descricao] = @descricao, [ativo] = @ativo, [dt_criacao] = @dt_criacao, [dt_atualizacao] = @dt_atualizacao, [user_email] = @user_email, [id_projeto] = @id_projeto WHERE [id] = @id">
                    <DeleteParameters>
                        <asp:Parameter Name="id" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="regional" Type="String" />
                        <asp:Parameter Name="descricao" Type="String" />
                        <asp:Parameter Name="ativo" Type="Boolean" />
                        <asp:Parameter Name="dt_criacao" Type="DateTime" />
                        <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                        <asp:Parameter Name="user_email" Type="String" />
                        <asp:Parameter Name="id_projeto" Type="Int32" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="cbxAtivo" DefaultValue="True" Name="ativo" PropertyName="Value" Type="Boolean" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="regional" Type="String" />
                        <asp:Parameter Name="descricao" Type="String" />
                        <asp:Parameter Name="ativo" Type="Boolean" />
                        <asp:Parameter Name="dt_criacao" Type="DateTime" />
                        <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                        <asp:Parameter Name="user_email" Type="String" />
                        <asp:Parameter Name="id_projeto" Type="Int32" />
                        <asp:Parameter Name="id" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>
</asp:Content>
