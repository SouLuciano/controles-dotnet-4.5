﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="defasagem.aspx.cs" Inherits="webequilibrio.app.residuo.defasagem" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" HeaderText="Defasagem SMPD" ShowCollapseButton="True" Theme="Office2010Blue" Width="200px">
        <PanelCollection>
            <dx:PanelContent runat="server">
                <table class="nav-justified">
                    <tr>
                        <td>
                            <table class="nav-justified">
                                <tr>
                                    <td>
                                        <dx:ASPxRadioButtonList ID="ASPxRadioButtonList1" runat="server" AutoPostBack="True" RepeatDirection="Horizontal" SelectedIndex="0" ValueType="System.Boolean">
                                            <Items>
                                                <dx:ListEditItem Selected="True" Text="Manter" Value="True" />
                                                <dx:ListEditItem Text="Retirar" Value="False" />
                                            </Items>
                                        </dx:ASPxRadioButtonList>
                                    </td>
                                    <td>
                                        <dx:ASPxGridViewExporter ID="gvExporter" runat="server" ExportedRowType="All" GridViewID="ASPxGridView1">
                                        </dx:ASPxGridViewExporter>
                                        <asp:Button ID="btnExportar" runat="server" OnClick="btnExportar_Click" Text="Exportar excel" CssClass="btn btn-info" />
&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:SqlDataSource ID="dsDefasagem" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" DeleteCommand="DELETE FROM defasagem WHERE id = @id" InsertCommand="INSERT INTO [defasagem] ([id], [cla_idade], [cla_defasagem], [mes], [ano], [filial], [projeto], [talhao], [manejo], [vl_defasagem], [dt_plantio], [idade], [area], [op1], [op2a], [op2b], [status]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" ProviderName="<%$ ConnectionStrings:equilibrioConnectionString.ProviderName %>" SelectCommand="SELECT id, cla_idade, cla_defasagem, mes, ano, filial, projeto, talhao, manejo, vl_defasagem, dt_plantio, idade, area, op1, op2a, op2b, status FROM defasagem WHERE (status = @status)" UpdateCommand="UPDATE defasagem SET cla_idade = @cla_idade, cla_defasagem = @cla_defasagem, mes = @mes, ano = @ano, filial = @filial, projeto = @projeto, talhao = @talhao, manejo = @manejo, vl_defasagem = @vl_defasagem, dt_plantio = @dt_plantio, idade = @idade, area = @area, op1 = @op1, op2a = @op2a, op2b = @op2b, status = @status WHERE id = @id">
                                <DeleteParameters>
                                    <asp:Parameter Name="id" Type="Int32" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="id" Type="Int32" />
                                    <asp:Parameter Name="cla_idade" Type="String" />
                                    <asp:Parameter Name="cla_defasagem" Type="String" />
                                    <asp:Parameter Name="mes" Type="Int32" />
                                    <asp:Parameter Name="ano" Type="Int32" />
                                    <asp:Parameter Name="filial" Type="String" />
                                    <asp:Parameter Name="projeto" Type="String" />
                                    <asp:Parameter Name="talhao" Type="String" />
                                    <asp:Parameter Name="manejo" Type="Int32" />
                                    <asp:Parameter Name="vl_defasagem" Type="Single" />
                                    <asp:Parameter Name="dt_plantio" Type="DateTime" />
                                    <asp:Parameter Name="idade" Type="Int32" />
                                    <asp:Parameter Name="area" Type="Single" />
                                    <asp:Parameter Name="op1" Type="Int32" />
                                    <asp:Parameter Name="op2a" Type="Int32" />
                                    <asp:Parameter Name="op2b" Type="Int32" />
                                    <asp:Parameter Name="status" Type="Boolean" />
                                </InsertParameters>
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="ASPxRadioButtonList1" DefaultValue="true" Name="status" PropertyName="Value" Type="Boolean" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="cla_idade" Type="String" />
                                    <asp:Parameter Name="cla_defasagem" Type="String" />
                                    <asp:Parameter Name="mes" Type="Int32" />
                                    <asp:Parameter Name="ano" Type="Int32" />
                                    <asp:Parameter Name="filial" Type="String" />
                                    <asp:Parameter Name="projeto" Type="String" />
                                    <asp:Parameter Name="talhao" Type="String" />
                                    <asp:Parameter Name="manejo" Type="Int32" />
                                    <asp:Parameter Name="vl_defasagem" Type="Single" />
                                    <asp:Parameter Name="dt_plantio" Type="DateTime" />
                                    <asp:Parameter Name="idade" Type="Int32" />
                                    <asp:Parameter Name="area" Type="Single" />
                                    <asp:Parameter Name="op1" Type="Int32" />
                                    <asp:Parameter Name="op2a" Type="Int32" />
                                    <asp:Parameter Name="op2b" Type="Int32" />
                                    <asp:Parameter Name="status" Type="Boolean" />
                                    <asp:Parameter Name="id" Type="Int32" />
                                </UpdateParameters>
                            </asp:SqlDataSource>
                            <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="dsDefasagem" KeyFieldName="id" Theme="PlasticBlue">
                                <SettingsEditing Mode="Batch">
                                </SettingsEditing>
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" Visible="False" VisibleIndex="0">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Classe de idade" FieldName="cla_idade" ShowInCustomizationForm="True" VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Classe de defasagem" FieldName="cla_defasagem" ShowInCustomizationForm="True" VisibleIndex="3">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Mês" FieldName="mes" ShowInCustomizationForm="True" VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Ano" FieldName="ano" ShowInCustomizationForm="True" VisibleIndex="5">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Filial" FieldName="filial" ShowInCustomizationForm="True" VisibleIndex="6">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Projeto" FieldName="projeto" ShowInCustomizationForm="True" VisibleIndex="7">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Talhão" FieldName="talhao" ShowInCustomizationForm="True" VisibleIndex="8">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Manejo" FieldName="manejo" ShowInCustomizationForm="True" VisibleIndex="9">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Valor defasagem" FieldName="vl_defasagem" ShowInCustomizationForm="True" VisibleIndex="10">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn Caption="Data de plantio" FieldName="dt_plantio" ShowInCustomizationForm="True" VisibleIndex="11">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn Caption="Idade" FieldName="idade" ShowInCustomizationForm="True" VisibleIndex="12">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Área" FieldName="area" ShowInCustomizationForm="True" VisibleIndex="13">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Op 1" FieldName="op1" ShowInCustomizationForm="True" VisibleIndex="14">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Op 2a" FieldName="op2a" ShowInCustomizationForm="True" VisibleIndex="15">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Op2 b" FieldName="op2b" ShowInCustomizationForm="True" VisibleIndex="16">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataCheckColumn Caption="Positiva" FieldName="status" ShowInCustomizationForm="True" VisibleIndex="1">
                                    </dx:GridViewDataCheckColumn>
                                </Columns>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>


    </asp:Content>
