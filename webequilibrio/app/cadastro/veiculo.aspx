﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="veiculo.aspx.cs" Inherits="webequilibrio.app.cadastro.veiculo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" HeaderText="Cadastro de veículos" Theme="Metropolis" Width="1500px">
        <PanelCollection>
            <dx:PanelContent runat="server">
                <table class="dxflInternalEditorTable_Moderno">
                    <tr>
                        <td>
                            <table style="width: 300px">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text="Mostrar itens"></asp:Label>
                                    </td>
                                    <td>
                                        <dx:ASPxComboBox ID="cbxAtivo" runat="server" AutoPostBack="True" SelectedIndex="0" Theme="Metropolis">
                                            <Items>
                                                <dx:ListEditItem Selected="True" Text="Ativos" Value="True" />
                                                <dx:ListEditItem Text="Inativos" Value="False" />
                                            </Items>
                                        </dx:ASPxComboBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvVeiculo" runat="server" AutoGenerateColumns="False" DataSourceID="dsVeiculo" KeyFieldName="id" OnCustomErrorText="gvVeiculo_CustomErrorText" OnRowInserting="gvVeiculo_RowInserting" OnRowUpdated="gvVeiculo_RowUpdated" OnRowUpdating="gvVeiculo_RowUpdating" OnRowValidating="gvVeiculo_RowValidating" Theme="Metropolis">
                                <SettingsDetail AllowOnlyOneMasterRowExpanded="True" ShowDetailRow="True" />
                                <Templates>
                                    <DetailRow>
                                        <dx:ASPxGridView ID="gvVeiculoHist" runat="server" AutoGenerateColumns="False" DataSourceID="dsVeiculoHist" EnableTheming="True" KeyFieldName="id" OnBeforePerformDataSelect="gvVeiculoHist_BeforePerformDataSelect" Theme="Metropolis">
                                            <Settings ShowTitlePanel="True" />
                                            <SettingsText Title="Histórico de alterações" />
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" Visible="False" VisibleIndex="0">
                                                    <EditFormSettings Visible="False" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="id_veiculo" Visible="False" VisibleIndex="1">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Placa" FieldName="placa" VisibleIndex="4">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Modelo" FieldName="modelo" VisibleIndex="5">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="cartao" VisibleIndex="6" Visible="False">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataCheckColumn FieldName="ativo" VisibleIndex="14">
                                                </dx:GridViewDataCheckColumn>
                                                <dx:GridViewDataTextColumn Caption="Cadastrado pelo(a)" FieldName="user_email" VisibleIndex="15">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataDateColumn Caption="Inicio contrato" FieldName="dt_contrato_ini" VisibleIndex="10">
                                                    <PropertiesDateEdit EditFormatString="d">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="Final contrato" FieldName="dt_contrato_fim" VisibleIndex="11">
                                                    <PropertiesDateEdit EditFormatString="d">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="Combustível" FieldName="combustivel" VisibleIndex="8">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Alugado" FieldName="alugado" VisibleIndex="9">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Observação" FieldName="observacao" VisibleIndex="13">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataComboBoxColumn Caption="Projeto" FieldName="id_projeto" Visible="False" VisibleIndex="2">
                                                    <PropertiesComboBox DataSourceID="dsProjeto" TextField="nome" ValueField="id">
                                                    </PropertiesComboBox>
                                                </dx:GridViewDataComboBoxColumn>
                                                <dx:GridViewDataComboBoxColumn Caption="Equipe" FieldName="id_equipe" VisibleIndex="3">
                                                    <PropertiesComboBox DataSourceID="dsEquipe" TextField="codigo" ValueField="id">
                                                    </PropertiesComboBox>
                                                </dx:GridViewDataComboBoxColumn>
                                                <dx:GridViewDataComboBoxColumn Caption="Rastreador" FieldName="id_celular" VisibleIndex="7">
                                                    <PropertiesComboBox DataSourceID="dsCelular" ValueField="id">
                                                        <Columns>
                                                            <dx:ListBoxColumn Caption="Imei" FieldName="imei1" />
                                                            <dx:ListBoxColumn Caption="Núm" FieldName="numero" />
                                                        </Columns>
                                                    </PropertiesComboBox>
                                                </dx:GridViewDataComboBoxColumn>
                                                <dx:GridViewDataTextColumn Caption="Status" FieldName="status" VisibleIndex="12">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                        <asp:SqlDataSource ID="dsVeiculoHist" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                                            SelectCommand="SELECT [id], [id_veiculo], [id_projeto], [placa], [modelo], [cartao], [ativo], [id_equipe], [id_celular], [user_email], [dt_contrato_ini], [dt_contrato_fim], [combustivel], [alugado], [observacao], [status] FROM [veiculo_hist] WHERE ([id_veiculo] = @id_veiculo) ORDER BY [id] DESC">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="id_veiculo" SessionField="id_veiculo" Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </DetailRow>
                                    <PreviewRow>
                                    </PreviewRow>
                                </Templates>
                                <SettingsEditing Mode="PopupEditForm">
                                </SettingsEditing>
                                <Settings ShowFilterRow="True" ShowFilterRowMenu="True" />
                                <SettingsPopup>
                                    <EditForm HorizontalAlign="Center" />
                                </SettingsPopup>
                                <SettingsSearchPanel Delay="2000" Visible="True" />
                                <Columns>
                                    <dx:GridViewCommandColumn ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="17">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Placa" FieldName="placa" ShowInCustomizationForm="True" VisibleIndex="4">
                                        <PropertiesTextEdit>
                                            <MaskSettings Mask="AAA-0000" />
                                        </PropertiesTextEdit>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Cartão Good Card" FieldName="cartao" ShowInCustomizationForm="True" VisibleIndex="6" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataCheckColumn FieldName="ativo" ShowInCustomizationForm="True" VisibleIndex="14" Visible="False">
                                        <EditFormSettings Visible="True" />
                                    </dx:GridViewDataCheckColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Equipe" FieldName="id_equipe" ShowInCustomizationForm="True" VisibleIndex="3">
                                        <PropertiesComboBox AllowNull="True" DataSourceID="dsEquipe" TextField="codigo" ValueField="id">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Rastreador Imei 1" FieldName="id_celular" ShowInCustomizationForm="True" VisibleIndex="7">
                                        <PropertiesComboBox AllowNull="True" DataSourceID="dsCelular" TextField="imei1" ValueField="id">
                                            <Columns>
                                                <dx:ListBoxColumn Caption="Imei 1" FieldName="imei1" />
                                                <dx:ListBoxColumn Caption="Numero 1" FieldName="numero" />
                                            </Columns>
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Projeto" FieldName="id_projeto" ShowInCustomizationForm="True" VisibleIndex="1">
                                        <PropertiesComboBox DataSourceID="dsProjeto" TextField="nome" ValueField="id">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataDateColumn Caption="Data hora atualização" FieldName="dt_atualizacao" ShowInCustomizationForm="True" VisibleIndex="16">
                                        <PropertiesDateEdit DisplayFormatString="g">
                                        </PropertiesDateEdit>
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Modelo" FieldName="modelo" ShowInCustomizationForm="True" VisibleIndex="5">
                                        <PropertiesComboBox DataSourceID="dsTipoEquipamento" TextField="nome" ValueField="nome">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataTextColumn Caption="Observação" FieldName="observacao" ShowInCustomizationForm="True" VisibleIndex="13">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Cadastrado pelo(a)" FieldName="user_email" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="15">
                                        <PropertiesTextEdit>
                                            <MaskSettings Mask="\@.*$" />
                                        </PropertiesTextEdit>
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Regional" FieldName="id_regional" ShowInCustomizationForm="True" ToolTip="Regional" VisibleIndex="2">
                                        <PropertiesComboBox DataSourceID="dsRegional" TextField="regional" ValueField="id">
                                        </PropertiesComboBox>
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Alugado" FieldName="alugado" ShowInCustomizationForm="True" VisibleIndex="9">
                                        <PropertiesComboBox>
                                            <Items>
                                                <dx:ListEditItem Selected="True" Text="Sim" Value="sim" />
                                                <dx:ListEditItem Text="Não" Value="nao" />
                                            </Items>
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Combustível" FieldName="combustivel" ShowInCustomizationForm="True" VisibleIndex="8">
                                        <PropertiesComboBox DisplayFormatString="G">
                                            <Items>
                                                <dx:ListEditItem Selected="True" Text="Própio" Value="propio" />
                                                <dx:ListEditItem Text="Cliente" Value="cliente" />
                                            </Items>
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataDateColumn Caption="Inicio contrato" FieldName="dt_contrato_ini" ShowInCustomizationForm="True" VisibleIndex="10">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataDateColumn Caption="Fim contrato" FieldName="dt_contrato_fim" ShowInCustomizationForm="True" VisibleIndex="11">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Status" FieldName="status" ShowInCustomizationForm="True" VisibleIndex="12">
                                        <PropertiesComboBox DataSourceID="dsStatusEquipamento" TextField="condicao" ValueField="condicao">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                </Columns>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" Text="Exportar para excel" Theme="Metropolis">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="dsVeiculo" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>"
                    DeleteCommand="DELETE FROM [veiculo] WHERE [id] = @id"
                    InsertCommand="INSERT INTO [veiculo] ([id_projeto], [placa], [modelo], [cartao], [ativo], [id_equipe], [id_celular], [dt_criacao], [dt_atualizacao], [observacao], [user_email], [id_regional], [dt_contrato_ini], [dt_contrato_fim], [combustivel], [alugado], [status]) VALUES (@id_projeto, @placa, @modelo, @cartao, @ativo, @id_equipe, @id_celular, @dt_criacao, @dt_atualizacao, @observacao, @user_email, @id_regional, @dt_contrato_ini, @dt_contrato_fim, @combustivel, @alugado, @status)"

                    SelectCommand="SELECT [id], [id_projeto], [placa], [modelo], [cartao], [ativo], [id_equipe], [id_celular], [dt_criacao], [dt_atualizacao], [observacao], [user_email], [id_regional], [dt_contrato_ini], [dt_contrato_fim], [combustivel], [alugado], [status] FROM [veiculo] WHERE ([ativo] = @ativo) ORDER BY [id_regional], [id_equipe]"
                    UpdateCommand="UPDATE [veiculo] SET [id_projeto] = @id_projeto, [placa] = @placa, [modelo] = @modelo, [cartao] = @cartao, [ativo] = @ativo, [id_equipe] = @id_equipe, [id_celular] = @id_celular, [dt_criacao] = @dt_criacao, [dt_atualizacao] = @dt_atualizacao, [observacao] = @observacao, [user_email] = @user_email, [id_regional] = @id_regional, [dt_contrato_ini] = @dt_contrato_ini, [dt_contrato_fim] = @dt_contrato_fim, [combustivel] = @combustivel, [alugado] = @alugado, [status] = @status WHERE [id] = @id">
                    <DeleteParameters>
                        <asp:Parameter Name="id" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="id_projeto" Type="Int32" />
                        <asp:Parameter Name="placa" Type="String" />
                        <asp:Parameter Name="modelo" Type="String" />
                        <asp:Parameter Name="cartao" Type="String" />
                        <asp:Parameter Name="ativo" Type="Boolean" />
                        <asp:Parameter Name="id_equipe" Type="Int32" />
                        <asp:Parameter Name="id_celular" Type="Int32" />
                        <asp:Parameter Name="dt_criacao" Type="DateTime" />
                        <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                        <asp:Parameter Name="observacao" Type="String" />
                        <asp:Parameter Name="user_email" Type="String" />
                        <asp:Parameter Name="id_regional" Type="Int32" />
                        <asp:Parameter Name="dt_contrato_ini" Type="DateTime" />
                        <asp:Parameter Name="dt_contrato_fim" Type="DateTime" />
                        <asp:Parameter Name="combustivel" Type="String" />
                        <asp:Parameter Name="alugado" Type="String" />
                        <asp:Parameter Name="status" Type="String" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="cbxAtivo" DefaultValue="True" Name="ativo" PropertyName="Value" Type="Boolean" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="id_projeto" Type="Int32" />
                        <asp:Parameter Name="placa" Type="String" />
                        <asp:Parameter Name="modelo" Type="String" />
                        <asp:Parameter Name="cartao" Type="String" />
                        <asp:Parameter Name="ativo" Type="Boolean" />
                        <asp:Parameter Name="id_equipe" Type="Int32" />
                        <asp:Parameter Name="id_celular" Type="Int32" />
                        <asp:Parameter Name="dt_criacao" Type="DateTime" />
                        <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
                        <asp:Parameter Name="observacao" Type="String" />
                        <asp:Parameter Name="user_email" Type="String" />
                        <asp:Parameter Name="id_regional" Type="Int32" />
                        <asp:Parameter Name="dt_contrato_ini" Type="DateTime" />
                        <asp:Parameter Name="dt_contrato_fim" Type="DateTime" />
                        <asp:Parameter Name="combustivel" Type="String" />
                        <asp:Parameter Name="alugado" Type="String" />
                        <asp:Parameter Name="status" Type="String" />
                        <asp:Parameter Name="id" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsProjeto" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                    SelectCommand="SELECT [id], [nome], [descricao] FROM [projeto] WHERE ([ativo] = @ativo) ORDER BY [nome]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsEquipe" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                    OnSelecting="dsEquipe_Selecting" 
                    SelectCommand="SELECT [id], [codigo] FROM [equipe] WHERE ([ativo] = @ativo) ORDER BY [codigo]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsTipoEquipamento" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                  
                    SelectCommand="SELECT [nome], [tipo] FROM [tipo_equipamento] WHERE ([tipo] = @tipo) ORDER BY [nome]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="veiculo" Name="tipo" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsCelular" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>"
                    SelectCommand="SELECT celular.id, celular.imei1, simcard.numero FROM celular LEFT OUTER JOIN simcard ON celular.id_simcard1 = simcard.id WHERE (celular.ativo = 1) AND (celular.funcao = 'rastreador')" 
                    OnSelecting="dsCelular_Selecting"></asp:SqlDataSource>
                <asp:SqlDataSource ID="dsRegional" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
                    SelectCommand="SELECT [id], [regional] FROM [regional] WHERE ([ativo] = @ativo) ORDER BY [regional]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="True" Name="ativo" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <dx:ASPxGridViewExporter ID="gvExporter" runat="server" ExportedRowType="All" FileName="Veiculos" GridViewID="gvVeiculo" PaperKind="A4">
                </dx:ASPxGridViewExporter>
                <asp:SqlDataSource ID="dsStatusEquipamento" runat="server" ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" SelectCommand="SELECT [condicao], [tipo_equipamento] FROM [status_equipamento] WHERE ([tipo_equipamento] = @tipo_equipamento)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="radio" Name="tipo_equipamento" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>
</asp:Content>
