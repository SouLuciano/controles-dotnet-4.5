﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="projeto.aspx.cs" Inherits="webequilibrio.app.cadastro.projeto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" HeaderText="Cadastro de projetos" Theme="Metropolis" Width="100%">
        <PanelCollection>
<dx:PanelContent runat="server">
    <table class="dxflInternalEditorTable_Moderno">
        <tr>
            <td>
                <table style="width: 450px">
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text="Mostrar itens"></asp:Label>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cbxAtivo" runat="server" AutoPostBack="True" SelectedIndex="0" Theme="Metropolis">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="Ativos" Value="True" />
                                    <dx:ListEditItem Text="Inativos" Value="False" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxGridView ID="gvProjeto" runat="server" AutoGenerateColumns="False" DataSourceID="ds_projeto" KeyFieldName="id" OnCustomErrorText="gvProjeto_CustomErrorText" OnRowInserted="gvProjeto_RowInserted" OnRowInserting="gvProjeto_RowInserting" OnRowUpdated="gvProjeto_RowUpdated" OnRowUpdating="gvProjeto_RowUpdating" OnRowValidating="gvProjeto_RowValidating" Theme="Metropolis">
                    <SettingsEditing Mode="PopupEditForm">
                    </SettingsEditing>
                    <Settings ShowFilterRow="True" />
                    <SettingsSearchPanel Visible="True" />
                    <Columns>
                        <dx:GridViewCommandColumn ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="7">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Nome" FieldName="nome" ShowInCustomizationForm="True" VisibleIndex="1">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Descrição" FieldName="descricao" ShowInCustomizationForm="True" VisibleIndex="2">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="ativo" ShowInCustomizationForm="True" Visible="False" VisibleIndex="3">
                            <EditFormSettings Visible="True" />
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn FieldName="user_email" ReadOnly="True" ShowInCustomizationForm="True" Visible="False" VisibleIndex="4">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn FieldName="dt_criacao" ReadOnly="True" ShowInCustomizationForm="True" Visible="False" VisibleIndex="5">
                            <PropertiesDateEdit DisplayFormatString="g">
                            </PropertiesDateEdit>
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataDateColumn Caption="Data hora atualização" FieldName="dt_atualizacao" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="6">
                            <PropertiesDateEdit DisplayFormatString="g">
                            </PropertiesDateEdit>
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataDateColumn>
                    </Columns>
                </dx:ASPxGridView>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    <asp:SqlDataSource ID="ds_projeto" runat="server" 
        ConnectionString="<%$ ConnectionStrings:equilibrioConnectionString %>" 
        DeleteCommand="DELETE FROM [projeto] WHERE [id] = @id" 
        InsertCommand="INSERT INTO [projeto] ([nome], [descricao], [ativo], [user_email], [dt_criacao], [dt_atualizacao]) VALUES (@nome, @descricao, @ativo, @user_email, @dt_criacao, @dt_atualizacao)" 
        SelectCommand="SELECT [id], [nome], [descricao], [ativo], [user_email], [dt_criacao], [dt_atualizacao] FROM [projeto] WHERE ([ativo] = @ativo) ORDER BY [nome]" 
        UpdateCommand="UPDATE [projeto] SET [nome] = @nome, [descricao] = @descricao, [ativo] = @ativo, [user_email] = @user_email, [dt_criacao] = @dt_criacao, [dt_atualizacao] = @dt_atualizacao WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="nome" Type="String" />
            <asp:Parameter Name="descricao" Type="String" />
            <asp:Parameter Name="ativo" Type="Boolean" />
            <asp:Parameter Name="user_email" Type="String" />
            <asp:Parameter Name="dt_criacao" Type="DateTime" />
            <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="cbxAtivo" DefaultValue="True" Name="ativo" PropertyName="Value" Type="Boolean" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="nome" Type="String" />
            <asp:Parameter Name="descricao" Type="String" />
            <asp:Parameter Name="ativo" Type="Boolean" />
            <asp:Parameter Name="user_email" Type="String" />
            <asp:Parameter Name="dt_criacao" Type="DateTime" />
            <asp:Parameter Name="dt_atualizacao" Type="DateTime" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
            </dx:PanelContent>
</PanelCollection>
    </dx:ASPxRoundPanel>
</asp:Content>
